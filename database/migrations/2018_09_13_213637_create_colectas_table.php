<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColectasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql')->create('colectas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_participante')->unsigned();
            $table->integer('id_recolector')->unsigned();
            $table->datetime('desde');
            $table->datetime('hasta');
            $table->double('monto_recibido',0.0);
            $table->text('observacion')->nullable();
            $table->tinyInteger('estado');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('colectas');
    }
}
