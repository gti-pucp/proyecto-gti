<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql')->create('permisos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('titulo');
            $table->text('descripcion');
            $table->timestamps();
        });

        Schema::connection('mysql')->create('permisos_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_permiso');
            $table->string('id_usuario');
            $table->text('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('mysql')->dropIfExists('permisos_usuarios');
        Schema::connection('mysql')->dropIfExists('permisos');
    }
}
