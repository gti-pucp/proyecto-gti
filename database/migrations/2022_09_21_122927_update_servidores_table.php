<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServidoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('servidores', function ($table) {
            $table->integer('limite_espacio')->after('comprobar')
                ->comment("Limite porcentual inferior de espacio usado")
                ->default(90);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('servidores', function ($table) {
            $table->dropColumn("limite_espacio");
        });
    }
}
