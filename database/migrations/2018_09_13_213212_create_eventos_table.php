<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql')->create('eventos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_creador')->unsigned();
            $table->integer('id_responsable')->unsigned();
            $table->string('titulo');
            $table->text('descripcion')->nullable();
            $table->tinyInteger('tipo')->unsigned();
            $table->dateTime('fecha_hora');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('mysql')->dropIfExists('eventos');
    }
}
