<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableServidores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('servidores', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('url');
            $table->boolean('comprobar')->default(false);
            $table->dateTime("fecha_online")->nullable();
            $table->dateTime("fecha_notificacion")->nullable();
            $table->string('correo_responsable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('servidores');
    }
}
