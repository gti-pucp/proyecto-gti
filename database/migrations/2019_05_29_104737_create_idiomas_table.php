<?php

use App\Modulos\Dataverse\Idioma;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdiomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::connection('mysql')->create('idiomas', function (Blueprint $table) {
            $table->increments('id');
            $table->string("codigo")->unique();
            $table->timestamps();
        });

        Idioma::create(['codigo'=>'en']);
        Idioma::create(['codigo'=>'es']);


        Schema::connection('mysql')->create('archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string("nombre")->unique();
            $table->timestamps();
        });
        Schema::connection('mysql')->create('identificadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_archivo');
            $table->text("identificador");
            $table->boolean("es_texto");
            $table->timestamps();
        });


        Schema::connection('mysql')->create('traducciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_identificador');
            $table->string('id_idioma');
            $table->text("valor");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('idiomas');
        Schema::connection('mysql')->dropIfExists('archivos');
        Schema::connection('mysql')->dropIfExists('identificadores');
        Schema::connection('mysql')->dropIfExists('traducciones');
    }
}
