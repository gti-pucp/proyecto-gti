<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql')->create('invitados', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_evento')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('monto_ofrecido');
            $table->integer('estado')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->dropIfExists('invitados');
    }
}
