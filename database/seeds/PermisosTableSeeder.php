<?php

use App\Modulos\OJS\v2\Reporte;
use App\Modulos\Usuarios\Permiso;
use App\Modulos\Usuarios\Usuario;
use App\Policies\EventosPolicy;
use App\Policies\OAIPolicy;
use App\Policies\PermisosPolicy;
use App\Policies\ReporteDspaceRIPolicy;
use App\Policies\ReporteDspaceTesisPolicy;
use App\Policies\ReporteEZProxyPolicy;
use App\Policies\ReporteOJSPolicy;
use App\Policies\ReporteOSTicketsPolicy;
use App\Policies\ServidorPolicy;
use App\Policies\UsuariosPolicy;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permisos')->truncate();
        Permiso::create([
            'codigo' => UsuariosPolicy::class,
            'titulo'=>'Gestion de Usuarios',
            'descripcion' => "Acceso al modulo de usuarios",
        ]);

        Permiso::create([
            'codigo' => PermisosPolicy::class,
            'titulo' => 'Gestion de permisos',
            'descripcion' => "Acceso al modulo de permisos de usuario",
        ]);

        Permiso::create([
            'codigo' => ServidorPolicy::class,
            'titulo' => 'Modulo Monitoreo',
            'descripcion' => "Acceso al modulo de Monitoreo de servidores",
        ]);

        Permiso::create([
            'codigo' => OAIPolicy::class,
            'titulo' => 'Modulo OAI depurador',
            'descripcion' => "Acceso al modulo de depuracion OAI",
        ]);


        Permiso::create([
            'codigo' => ReporteOJSPolicy::class,
            'titulo' => 'Modulo OJS - reportes',
            'descripcion' => "Acceso al modulo de reportes OJS",
        ]);

        Permiso::create([
            'codigo' => ReporteEZProxyPolicy::class,
            'titulo' => 'Modulo EZProxy - resumenes',
            'descripcion' => "Acceso al modulo de resumenes logs EZProxy",
        ]);


        Permiso::create([
            'codigo' => ReporteOSTicketsPolicy::class,
            'titulo'=>'Modulo Mesa de ayuda',
            'descripcion' => "Acceso al modulo de mesa de ayuda OSTickets",
        ]);

        Permiso::create([
            'codigo' => ReporteDspaceRIPolicy::class,
            'titulo'=>'Modulo Repositorio Institucional',
            'descripcion' => "Acceso al modulo de repositorio institucional",
        ]);

        Permiso::create([
            'codigo' => ReporteDspaceTesisPolicy::class,
            'titulo'=>'Modulo Repositorio de tesis',
            'descripcion' => "Acceso al modulo de repositorio de tesis",
        ]);

//        Permiso::create([
//            'codigo' => EventosPolicy::class,
//            'titulo' => 'Modulo Eventos',
//            'descripcion' => "Acceso al modulo de eventos",
//        ]);

//        $usuario=Usuario::find(1);
//        DB::table('permisos_usuarios')->truncate();
//        Permiso::all()->each(function ($permiso) use($usuario){
//            $usuario->permisos()->attach($permiso->id,['estado'=>1]);
//        });


    }
}
