<?php

use App\Modulos\Tesis\Agente;
use App\Modulos\Tesis\EPerson;
use Illuminate\Database\Seeder;

class AgenteTesisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $lista=collect([
            'dalvarado@pucp.edu.pe',
            'vbranes@pucp.pe',
            'miriam.cayllahua@pucp.pe',
            'mgaldos@pucp.pe',
            'cgiraldoa@pucp.pe',
            'shuaraz@pucp.pe',
            'leon.jd@pucp.pe',
            'jsorianof@pucp.edu.pe',
            'ana.tarazona@pucp.pe',
            'rvaleriom@pucp.pe',
            'lmanchego@pucp.pe',
            'nbasilio@pucp.edu.pe'
        ]);

        $lista->each(function($email){
            $eperson=EPerson::where('email',$email)->first();

            if($eperson instanceof EPerson ){
                $agente=Agente::where('uuid',$eperson->uuid)->first();
                if($agente instanceof Agente){
                    echo "Ya existe el usuario con email : ".$email."\n";
                }else{
                    $agente=new Agente();
                    $agente->uuid=$eperson->uuid;
                    $agente->save();
                    echo "Asignado para reportes: ".$email."\n";
                }
            }else{
                echo "no existe usuario con email ".$email."\n";
            }
        });
    }
}
