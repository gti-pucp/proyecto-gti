<?php

use App\Modulos\Chat\Canal;
use App\Modulos\Chat\Mensaje;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CanalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('canales')->truncate();

        $canal=Canal::create([
            'id_usuario'=>1,
            'nombre'=>'general',
        ]);

        DB::table('canales_usuarios')->truncate();
        User::all()->random(5)->each(function($usuario) use($canal){
            $canal->miembros()->attach($usuario->id);
        });

        DB::table('mensajes')->truncate();

        $canal->miembros->each(function($usuario) use($canal){
           Mensaje::create([
               'id_canal'=>$canal->id,
               'id_usuario'=>$usuario->id,
               'mensaje'=>Str::random(5),
           ]);
        });

    }
}
