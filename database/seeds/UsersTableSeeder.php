<?php

use App\Modulos\Usuarios\Usuario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->truncate();
        Usuario::create([
            'name' => 'Alan Gastelo',
            'email' => 'agastelo@pucp.edu.pe',
            'password' => bcrypt('030496e'),
            'remember_token' => Str::random(10),
            'api_token' => Str::random(60),

        ]);

        Usuario::create([
            'name' => 'Alan Charly Gastelo Benavides',
            'email' => 'alangb01@gmail.com',
            'password' => bcrypt('030496e'),
            'remember_token' => Str::random(10),
            'api_token' => Str::random(60),
        ]);

        factory(Usuario::class,50)->create();

        //seteando permisos
        DB::table('permisos_usuarios')->truncate();
//        Usuario::whereIn('id',[1,2])->get()->each(function($user){
//            $user->permisos()->attach(Permiso::MODULO_USUARIOS,['estado'=>true]);
//            $user->permisos()->attach(Permiso::MODULO_PERMISOS,['estado'=>true]);
//            $user->permisos()->attach(Permiso::MODULO_EZPROXY,['estado'=>true]);
//            $user->permisos()->attach(Permiso::MODULO_OJS,['estado'=>true]);
//            $user->permisos()->attach(Permiso::MODULO_EVENTOS,['estado'=>true]);
//        });
    }
}
