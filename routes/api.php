<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//
////REPOSITORIO TESIS
//Route::middleware('auth:api')->get('/dspace-tesis/thumbnails', function (Request $request) {
//    ItemTesis::prepararParaThumbnailGenerator()->get()->each(function($item) {
//        echo $item->handle."\n";
//    });
//});
//
////REPOSITORIO INSTITUCIONAL
//Route::middleware('auth:api')->get('/dspace-ri/cosechas', function (Request $request) {
//    $fecha_get=$request->get('fecha_limite');
//    if(isset($fecha_get)){
//        $fecha_limite=Carbon::createFromFormat("Y-m-d",$fecha_get)->startOfDay();
//    }else{
//        $fecha_limite=Carbon::now()->startOfDay();
//
//        if(!$fecha_limite->isSunday()){
//            $fecha_limite->startOfWeek()->addDays(-1);
//        }
//    }
//
//    if($fecha_limite instanceof Carbon){
//        HarvestedCollectionRI::prepararCosechas($fecha_limite)->get()->each(function($harvested)  {
//            echo $harvested->handle_coleccion."\n";
//        });
//    }else{
//        echo "error";
//    }
//
//
//
//});
//
//Route::middleware('auth:api')->get('/dspace-ri/thumbnails', function (Request $request) {
//    $position=0;
//    ItemRI::prepararParaThumbnailGenerator()->get()->each(function($item) use(&$position) {
//        echo $item->handle."\n";
//    });
//});

Route::middleware('auth:api')->get('/dspace-tesis/thumbnails', 'Api\TesisController@thumbnailsPendientes');
Route::middleware('auth:api')->get('/dspace-ri/cosechas', 'Api\RIController@cosechasPendientes');
Route::middleware('auth:api')->get('/dspace-ri/thumbnails', 'Api\RIController@thumbnailsPendientes');
Route::middleware('auth:api')->post('/notificar/espacio-disco', 'Api\ServidorController@espacioDisco');
