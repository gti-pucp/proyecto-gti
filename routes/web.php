<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/pruebas/chat",function(){
    return view('pruebas.chat.index');
})->name('pruebas.chat.index');
Route::get("/pruebas/chat/popup",function(){
    return view('pruebas.chat.popup');
})->name('pruebas.chat.popup');
Route::get("/pruebas/chat/embebed",function(){
    return view('pruebas.chat.embebed');
})->name('pruebas.chat.embebed');
Route::get("/pruebas/chat/slide",function(){
    return view('pruebas.chat.slide');
})->name('pruebas.chat.slide');


Route::get('/', function () {
    return redirect()->route('home');
});

// LOGIN CON GOOGLE
Route::get('login-social/google', 'Auth\LoginSocialiteController@redirectToProvider')->name('login.google');
Route::get('login-social/google/callback', 'Auth\LoginSocialiteController@handleProviderCallback');
Route::get('login-social/google/registrar', 'Auth\RegisterSocialiteController@registrar')->name('registrar');
Route::get('generar-token','Auth\ApiTokenController@update')->name('perfil.token.generar');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix("gestor")->namespace("Gestor")->group(function(){
    //PERFIL DE GESTOR
    Route::get('perfil','PerfilController@show')->name('perfil.show');
    Route::get('perfil/editar','PerfilController@editar')->name('perfil.edit');
    Route::put('perfil/editar','PerfilController@update');
    Route::get('perfil/clave','PerfilController@clave')->name('perfil.clave');

    Route::put('perfil/clave','PerfilController@store');
    Route::get('perfil/imagen','PerfilController@cargarImagen')->name('perfil.imagen');
    Route::get('perfil/editar/imagen','PerfilController@imagen')->name('perfil.imagen.edit');
    Route::put('perfil/editar/imagen','PerfilController@almacenarImagen');
    Route::delete('perfil/editar/imagen','PerfilController@eliminarImagen');

    //USUARIOS
    Route::prefix("usuarios")->namespace('Usuarios')->group(function(){
        Route::get('/',"UsuariosController@index")->name('gestor.usuarios.listado');
        Route::get('/crear',"UsuariosController@create")->name('gestor.usuarios.crear');
        Route::post('/crear',"UsuariosController@store");
        Route::get('/{id_usuario}/editar',"UsuariosController@edit")->name('gestor.usuarios.editar');
        Route::post('/{id_usuario}/editar',"UsuariosController@update");
        Route::delete('/{id_usuario}/eliminar',"UsuariosController@destroy")->name('gestor.usuarios.eliminar');

         Route::put('/{id_usuario}','UsuariosController@generarClave')->name('gestor.usuarios.reset');

        Route::get('/permisos/{id_usuario}',"PermisosController@index")->name('gestor.permisos.listado');
        Route::post('/permisos/{id_usuario}',"PermisosController@store")->name('gestor.permisos.agregar');
        Route::delete('/permisos/{id_usuario}',"PermisosController@destroy");
    });

    //DSPACE TESIS
    Route::prefix("dspace/tesis")->namespace('Dspace\Tesis')->group(function(){
        Route::get('configurar/agentes','AgentesController@index')->name('dspace.tesis.agentes.listado');
        Route::post('configurar/agentes','AgentesController@store');
        Route::delete('configurar/agentes/{uuid}','AgentesController@destroy')->name('dspace.tesis.agentes.delete');
        Route::get('reporte','ReportesController@index')->name('dspace.tesis.reporte');
        Route::get('exportar','ReportesController@exportar')->name('dspace.tesis.reporte.exportar');
        Route::get('pesados','ReportesController@archivosPesados')->name('dspace.tesis.pesados');
        Route::get('pendientes','ReportesController@itemsPendientes')->name('dspace.tesis.pendientes');
    });
    //DSPACE REPOSITORIO
    Route::prefix("dspace/ri")->namespace('Dspace\RI')->group(function(){
        Route::get('configurar/agentes','AgentesController@index')->name('dspace.ri.agentes.listado');
        Route::post('configurar/agentes','AgentesController@store');
        Route::delete('configurar/agentes/{uuid}','AgentesController@destroy')->name('dspace.ri.agentes.delete');
        Route::get('reporte','ReportesController@index')->name('dspace.ri.reporte');
        Route::get('pesados','ReportesController@archivosPesados')->name('dspace.ri.pesados');
        Route::get('pendientes','ReportesController@itemsPendientes')->name('dspace.ri.pendientes');
        Route::get('ri','ReportesController@cosechas')->name('dspace.ri.cosechas');

        Route::get('duplicados','ReportesController@duplicados')->name('dspace.ri.duplicados');

        Route::get('items-nulos-indexacion','ReportesController@itemsIndexacionNullPointer')->name('dspace.ri.oai-null');
    });

    //OS TICKETS Mesa de ayuda
    Route::prefix("ostickets")->namespace('OSTickets')->group(function(){
        Route::get('reporte/agentes','ReportesController@participacionAgentes')->name('ostickets.reporte.agentes');
        Route::get('reporte/usuarios','ReportesController@participacionUsuarios')->name('ostickets.reporte.usuarios');

        Route::get('reporte/general','ReportesOSTicketsController@index')->name('ostickets.reporte.general');
        Route::get('reporte/general/equipos','ReportesOSTicketsController@index')->name('ostickets.reporte.equipos_departamento');
        Route::get('reporte/general/agentes','ReportesOSTicketsController@index')->name('ostickets.reporte.agentes_equipo');

    });

    //REVISTAS
    Route::prefix("revistas")->namespace('OJS')->group(function(){
        Route::get('v2/reporte/visitas','ReportesController@visitasAnualOJS2')->name('revistas2.reporte.visitas.anual');
        Route::get('v2/reporte/descargas','ReportesController@descargasAnualOJS2')->name('revistas2.reporte.descargas.anual');

        Route::get('v3/reporte/visitas','ReportesController@visitasAnualOJS3')->name('revistas3.reporte.visitas.anual');
        Route::get('v3/reporte/descargas','ReportesController@descargasAnualOJS3')->name('revistas3.reporte.descargas.anual');
        // REGISTRAR DOI LOTE
        Route::get('v3/doi/form','AsignarDOIController@form')->name('revistas3.doi.registrar');
        Route::post('v3/doi/form','AsignarDOIController@register');
        //LICENCIAS
        Route::get('v3/reporte/licencias','LicenciasController@index')->name('revistas3.licencias');
//        Route::post('v3/doi/{submission_id}','AsignarDOIController@save');// corrige submission
//        Route::get('v3/doi/{journal_path}/{issue_id}','AsignarDOIController@issues');
//        Route::get('v3/doi','AsignarDOIController@fixDoi');
    });

    //SERVIDORES
    Route::prefix("monitoreo")->namespace('Monitoreo')->group(function(){
        Route::get('/',"ServidoresController@index")->name('gestor.servidores.listado');
        Route::get('/crear',"ServidoresController@create")->name('gestor.servidores.crear');
        Route::post('/crear',"ServidoresController@store");
        Route::get('/{id_servidor}/editar',"ServidoresController@edit")->name('gestor.servidores.editar');
        Route::post('/{id_servidor}/editar',"ServidoresController@update");
        Route::delete('/{id_servidor}/eliminar',"ServidoresController@destroy")->name('gestor.servidores.eliminar');
    });


    //OAI PROCESADOR
    Route::prefix("oai")->namespace('OAI')->group(function(){
        Route::get('depurador','DepuradorOAIController@create')->name('gestor.oai.depurador');
        Route::post('depurador','DepuradorOAIController@store');

        Route::get('extractor/listado','ExtractorController@index')->name('gestor.oai.listado');
        Route::get('extractor/formulario','ExtractorController@create')->name('gestor.oai.crear');
        Route::post('extractor/formulario','ExtractorController@store');
        Route::get('extractor/{id_fuente}/procesado','ExtractorController@procesar')->name('gestor.oai.extractor');
        Route::get('extractor/descarga','ExtractorController@descarga')->name('gestor.oai.descarga');
    });

// EZPROXY
    Route::prefix("ezproxy")->namespace('EZPROXY')->group(function(){
        Route::get('listado','ReportesMensualesController@index')->name('gestor.ezproxy.listado');
        Route::get('descarga/{file}','ReportesMensualesController@descarga')->name('gestor.ezproxy.descarga');
    });

    Route::prefix("crossref")->namespace('CrossRef')->group(function(){
        //EXPORTAR XML
        Route::get('generador','GeneradorXmlController@formulario')->name('crossref.formulario');
        Route::post('generador','GeneradorXmlController@generar');
    });

    // EVENTOS
//    Route::prefix("colabora")->namespace('Colabora')->group(function(){
//        //eventos
////        Route::resource('eventos', 'EventosController');
//
//        //INVITADOS
//        Route::get('eventos/{id_evento}/invitados','InvitadosController@index')->name('invitados.index');
//        Route::post('eventos/{id_evento}/invitados','InvitadosController@store')->name('invitados.create');
//        Route::delete('eventos/{id_evento}/invitados','InvitadosController@destroy')->name('invitados.destroy');
//
//        //COLABORACION
////        Route::get('colaboraciones/listado','ColaboracionesController@index')->name('colaboraciones.listado');
//        Route::resource('colaboraciones', 'ColaboracionesController');
//
//        //SORTEO
////        Route::get('sorteos/listado','SorteosController@index')->name('sorteos.listado');
//        Route::resource('sorteos', 'SorteosController');
//    });

    //COMUNICACION
//    Route::prefix("comunicacion")->namespace("Comunicacion")->group(function(){
//        Route::get('general/{id_canal}','CanalesController@index');
//        Route::post('general/{id_canal}','GeneralController@store');
//        Route::get('general/{id_canal}/send','GeneralController@send');
//    });

});
Route::get('test-ri',function(){
//
    $rs=App\Modulos\RI\Item::prepararParaThumbnailGenerator()->limit(10);
    $rs->get()->each(function($item){

        echo $item->uuid." ".$item->handle. " ".$item->bundles()->count()."<br>";

        $item->bundles()->get()->each(function($bundle){
           echo "bundle ".$bundle->uuid."<br>";
           $bundle->getValores()->get()->each(function($value){
//               echo $value->metadataFieldRegistry."<br>";
               echo "    bundle value => ".$value->metadataFieldRegistry->element." :".$value->text_value."<br>";
           });

            $bundle->bitstreams()->get()->each(function($bitstream){
               echo "    bitstream ".$bitstream->uuid." sequence ".$bitstream->sequence_id."<br>";
               $bitstream->getValores()->get()->each(function($value){
//                  echo $value->metadataFieldRegistry."<br>";
                  echo "     bitstream value => e(".$value->metadataFieldRegistry->element.") :".$value->text_value."<br>";
               });
            });
        });
        echo "<hr>";
    });
});

