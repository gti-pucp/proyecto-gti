<form id="delete-form" action="" method="POST" style="display: none;">
    @csrf
    @method('delete')
</form>
<form id="confirm-form" action="" method="POST" style="display: none;">
    @csrf
    @method('put')
</form>

{{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">--}}
    {{--Launch demo modal--}}
{{--</button>--}}

<!-- Modal -->
<div class="modal fade" id="modalConfirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-confirmar"><i class="fas fa-trash-alt"></i> Confirmar </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(document).on('click','.btn-delete',function(e){
            e.preventDefault();
            href=$(this).attr('href');
            oModal=$('#modalConfirmacion').on('click', '.btn-confirmar', function(){
                $("#delete-form").attr('action',href).submit();
            });

            boton_confirmar=oModal.find(".btn.btn-confirmar");
            boton_confirmar.removeClass("btn-primary").addClass('btn-danger');

            oModal.modal('show');
            titulo=oModal.find('.modal-title').text('Confirmación');
            contenido=oModal.find('.modal-body').html($(this).data('modal'));
        });

        $(document).on('click','.btn-confirm',function(e){
            e.preventDefault();
            href=$(this).attr('href');
            oModal=$('#modalConfirmacion').on('click', '.btn-confirmar', function(){
                $("#confirm-form").attr('action',href).submit();
            });
            boton_confirmar=oModal.find(".btn.btn-confirmar");
            boton_confirmar.removeClass("btn-danger").addClass('btn-primary');

            oModal.modal('show');
            titulo=oModal.find('.modal-title').text('Confirmación');
            contenido=oModal.find('.modal-body').html($(this).data('modal'));
        });
    </script>
@endpush
