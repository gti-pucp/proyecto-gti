@if(isset($breadcrumbs))
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            {{--            <li class="breadcrumb-item"><a href="#">Home</a></li>--}}
            {{--            <li class="breadcrumb-item"><a href="#">Library</a></li>--}}
            {{--            <li class="breadcrumb-item active" aria-current="page">Data</li>--}}
            @foreach($breadcrumbs as $item)
                <li class="breadcrumb-item {{ $item->active?'active':'' }}">
                    @if(isset($item->href))
                        <a href="#">{{ $item->label }}</a>
                    @else
                        {{ $item->label }}
                    @endif
                </li>
            @endforeach
        </ol>
    </nav>
@endif
