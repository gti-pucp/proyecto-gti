@if(session('tipo'))
    <div class="alert alert-{{session('tipo')}} alert-dismissible fade show" role="alert">
        <strong>{{ session('notificacion') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif