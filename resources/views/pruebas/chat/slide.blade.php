@extends('pruebas.chat.layout')
@section('content')
    <a href="{{ route('pruebas.chat.index') }}"></a>
    <h4>SLIDE</h4>
    <!-- Place this div in your web page where you want your chat widget to appear. -->
    <div class="needs-js" style="margin:auto">chat loading...</div>

    <!-- Place this script as near to the end of your BODY as possible. -->
    <script type="text/javascript">
        (function() {
            var x = document.createElement("script"); x.type = "text/javascript"; x.async = true;
            x.src = (document.location.protocol === "https:" ? "https://" : "http://") + "refchatter.net/js/libraryh3lp.js?884";
            var y = document.getElementsByTagName("script")[0]; y.parentNode.insertBefore(x, y);
        })();
    </script>
@endsection
