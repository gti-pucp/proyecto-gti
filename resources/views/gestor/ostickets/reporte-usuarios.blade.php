@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <form class= "form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold d-inline col-md-4 ">{{ $titulo }}</label>
                    <label for="desde">Desde</label>
                    <input type="date" max="#hasta" name="desde" id="desde" class="form-control " placeholder="desde" value="{{ $dateStart->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                    <label for="hasta">Hasta</label>
                    <input type="date" min="#desde" name="hasta" id="hasta" class="form-control " placeholder="hasta" value="{{ $dateEnd->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                    <input type="submit" name="filtro" value="Consultar" class="btn btn-success ">
                </form>
            </div>
            <div class="card-body">
                <table class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark">
                    <tr>
                        {{--<th>id</th>--}}
                        <th>Usuario</th>
                        <th>total tickets</th>
                        <th>tickets del usuario</th>
                        <th>tickets de otro usuario</th>
                        <th>total mensajes</th>
                        <th>mensajes del usuario</th>
                        <th >mensajes de otro usuario</th>
                    </tr>
                    </thead>
                    @forelse($usuarios as $usuario)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">{{ $usuario->name }}</td>
                            <td class="text-sm-right bg-light">{{ $usuario->total_tickets }}</td>
                            <td class="text-sm-right">{{ $usuario->total_tickets_propios }}</td>
                            <td class="text-sm-right">{{ $usuario->total_tickets_ajenos }}</td>
                            <td class="text-sm-right bg-light">{{ $usuario->total_mensajes }}</td>
                            <td class="text-sm-right">{{ $usuario->total_mensajes_tickets_propios }}</td>
                            <td class="text-sm-right">{{ $usuario->total_mensajes_tickets_ajenos }}</td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($usuarios->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="14">{{ $usuarios->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
