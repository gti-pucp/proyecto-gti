@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <label class="card-title text-uppercase font-weight-bold d-inline col-md-4">{{ $titulo }}</label>
                <hr>
                <form class= "form form-inline" action="" method="get">

{{--                                <label for="temporalidad">Filtro </label>--}}
{{--                                <select name="temporalidad" id="temporalidad" class="form-control">--}}
{{--                                    @forelse($temporalidades as $item)--}}
{{--                                        <option value="{{$item->value}}" {{ $item->selected?'selected':'' }}>{{$item->label}}</option>--}}
{{--                                    @empty--}}
{{--                                        <option>Sin opciones</option>--}}
{{--                                    @endforelse--}}
{{--                                </select>--}}

{{--                                @if($temporalidad_seleccionado!="rango")--}}
{{--                                    <div class="btn-group" role="group" aria-label="Basic example">--}}
{{--                                        <button type="submit" name="btnOpcionTemporal" value="anterior" class="btn btn-secondary">Anterior</button>--}}
{{--                                        <button type="submit" name="btnOpcionTemporal" value="actual" class="btn btn-secondary">Actual</button>--}}
{{--                                        <button type="submit" name="btnOpcionTemporal" value="siguiente" class="btn btn-secondary">Siguiente</button>--}}
{{--                                    </div>--}}
{{--                                @endif--}}

{{--                                --}}
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Filtro rápido
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    @forelse($selectTemporalidades as $item)
                                        <a class="dropdown-item" href="{{$item->url}}">{{ $item->label }}</a>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                            <label for="desde">Desde</label>
                            <input type="date" max="#hasta" name="fecha_inicio" id="fecha_inicio" class="form-control" placeholder="desde" value="{{ $fecha_inicio->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" {{$temporalidad_seleccionado!="rango"?"readonly":""}}>
                            <label for="hasta">Hasta</label>
                            <input type="date" min="#desde" name="fecha_final" id="fecha_final" class="form-control" placeholder="hasta" value="{{ $fecha_final->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" {{$temporalidad_seleccionado!="rango"?"readonly":""}}>

                            <label for="desde">Por</label>
                            <select name="tipo" id="" class="form-control">
                                @forelse($selectTipos as $tipo)
                                    <option value="{{$tipo->value}}" {{ $tipo->selected?'selected':'' }}>{{$tipo->label}}</option>
                                @empty
                                    <option>departamento</option>
                                @endforelse
                            </select>
                    <select name="mostrar" id="" class="form-control">
                        @forelse($selectMostrar as $item)
                            <option value="{{$item->value}}" {{ $item->selected?'selected':'' }}>{{$item->label}}</option>
                        @empty
                            <option value="todos">Todos los tickets</option>
                        @endforelse
                    </select>

                            <input type="submit" name="filtro" value="Consultar" class="btn btn-success">


                </form>

            </div>
            <div class="card-body">

                <table class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th rowspan="2"></th>
                        <th>Creados</th>
                        <th>Asignados</th>
                        <th>Vencidos</th>
                        <th>Cerrados</th>
                        <th>Re-abierto</th>
                        <th>Tiempo de servicio</th>
                        <th>Tiempo de respuesta</th>
                    </tr>
                    </thead>
                    @forelse($listado as $item)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-left">





                                @if(isset($item->departamento))
                                    <a href="{{ $item->url_equipos_departamento }}">{{ $item->departamento }}</a>
                                @elseif(isset($item->equipo))
                                    <a href="{{ $item->url_agentes_equipo }}">{{ $item->equipo }}</a>
                                @elseif(isset($item->agente))
                                    {{ $item->agente }}
                                @elseif(isset($item->tipo))
                                    {{ $item->tipo }}
                                @else
                                    Sin asignar
                                @endif

                            </td>
                            <td class="text-right">{{ $item->opened }}</td>
                            <td class="text-right">{{ $item->assigned }}</td>
                            <td class="text-right">{{ $item->overdue }}</td>
                            <td class="text-right">{{ $item->closed }}</td>
                            <td class="text-right">{{ $item->reopened }}</td>
                            <td class="text-right">{{ $item->tiempo_servicio }}</td>
                            <td class="text-right">{{ $item->tiempo_respuesta }}</td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="19"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="19">{{ $listado->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
