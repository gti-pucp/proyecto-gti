@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <form class= "form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold d-inline col-md-4">{{ $titulo }}</label>
                    <label for="desde">Desde</label>
                    <input type="date" max="#hasta" name="desde" id="desde" class="form-control" placeholder="desde" value="{{ $dateStart->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                    <label for="hasta">Hasta</label>
                    <input type="date" min="#desde" name="hasta" id="hasta" class="form-control" placeholder="hasta" value="{{ $dateEnd->format('Y-m-d') }}"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}">
                    <input type="submit" name="filtro" value="Consultar" class="btn btn-success">
                </form>
            </div>
            <div class="card-body">

                <table class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th rowspan="2">Agente</th>
                        <th rowspan="2">% tickets asignados</th>
                        <th rowspan="2">% tickets ajenos</th>
                        <th rowspan="2">mensajes por ticket</th>
                        <th colspan="3">Tickets</th>
                        <th colspan="4">Mensajes</th>
                        <th colspan="4">Mensajes en Tickets asignados</th>
                        <th colspan="4">Mensajes en Tickets ajenos</th>
                    </tr>
                    <tr>
                        {{--<th>id</th>--}}


                        <th class="bg-info">total </th>
                        <th>asignados</th>
                        <th>ajenos</th>

                        <th class="bg-info">total</th>
                        <th>mensajes iniciales</th>
                        <th>notas</th>
                        <th>mensajes</th>

                        <th class="bg-info">total</th>
                        <th>mensajes iniciales </th>
                        <th>notas</th>
                        <th>mensajes</th>

                        <th class="bg-info">total</th>
                        <th>mensajes iniciales </th>
                        <th>notas</th>
                        <th>mensajes</th>
                    </tr>
                    </thead>
                    @forelse($agentes as $agente)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">{{ $agente->firstname." ".$agente->lastname }}</td>
                            <td class="text-sm-right ">{{ $agente->tickets_asignados_entre_totales*100 }}</td>
                            <td class="text-sm-right ">{{ $agente->tickets_ajenos_entre_totales*100 }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_x_ticket }}</td>

                            <td class="text-sm-right bg-info">{{ $agente->total_tickets }}</td>
                            <td class="text-sm-right">{{ $agente->total_tickets_asignados }}</td>
                            <td class="text-sm-right">{{ $agente->total_tickets_ajenos }}</td>

                            <td class="text-sm-right bg-info">{{ $agente->total_mensajes }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_inicial }}</td>
                            <td class="text-sm-right">{{ $agente->notas }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes }}</td>


                            <td class="text-sm-right bg-info">{{ $agente->total_mensajes_tickets_asignados }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_inicial_tickets_asignados }}</td>
                            <td class="text-sm-right">{{ $agente->notas_tickets_asignados }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_tickets_asignados }}</td>


                            <td class="text-sm-right bg-info">{{ $agente->total_mensajes_tickets_ajenos }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_inicial_tickets_ajenos }}</td>
                            <td class="text-sm-right">{{ $agente->notas_tickets_ajenos }}</td>
                            <td class="text-sm-right">{{ $agente->mensajes_tickets_ajenos }}</td>

                            {{--                    <td>{{ $agente->suma_tickets }}</td>--}}

                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="19"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($agentes->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="19">{{ $agentes->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
