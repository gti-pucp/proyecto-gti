@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <canvas id="myChart" width="800" height="400" style="display: none;"></canvas>
        <div class="card">
            <div class="card-header">
                <h3>URL Permitidas por PLUGINS OJS</h3>
                @foreach($url_licencias_permitidas as $licencia)
                    {{ $licencia }}<br>
                @endforeach
            </div>
            <div class="card-body">
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>LICENCIA REGISTRADAS</th>
                        <th>TOTAL ENVIOS</th>
                        <th>URL</th>
                    </tr>
                    </thead>
                    @forelse($filtros as $item)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left {{ $item->permitido?'bg-success':'bg-danger' }}">{{ $item->setting_value }}</td>
                            <td class="text-sm-left">{{ $item->total }}</td>
                            <td class="text-sm-left">
                                <a href="{{ $item->url_filtrado }}" class="btn btn-primary btn-sm">Filtrar</a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay filtros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($filtros->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $filtros->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>REVISTA</th>
                        <th>ENVIO</th>
                        <th>URL ADMIN</th>
                        <th>URL PÚBLICO</th>
                    </tr>
                    </thead>
                    @forelse($listado as $submission)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">{{ $submission->journal->path }}</td>
                            <td class="text-sm-left">{{ $submission->submission_id }}</td>
                            <td class="text-sm-left">
                                <a href="{{ $submission->url_gestor }}" class="btn btn-warning btn-sm" target="_blank">Enlace Admin</a>
                            </td>
                            <td class="text-sm-left">
                                <a href="{{ $submission->url_revista }}" class="btn btn-primary btn-sm" target="_blank">Enlace publico</a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $listado->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>

@endsection
