@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                REGISTROS DE DOIS PROCESADOS
                <a href="{{ route('revistas3.doi.registrar') }}">Volver</a>
            </div>
            <div class="card-body">

                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>Url Revistas Ingresado</th>
                        <th>Url DOI ingresado</th>
                        <th>ESTADO</th>
                    </tr>
                    </thead>
                    @forelse($listado_resultados as $item)
                        <tr class="item">
                            <td class="text-center">
                                @isset($item->url)
                                    <a href="{{$item->url}}" target="_blank" class="btn btn-primary btn-sm">
                                        Click aquí
                                    </a>
                                @endif
                            </td>
                            <td class="text-center">
                                @isset($item->doi)
                                <a href="{{$item->doi}}" target="_blank" class="btn btn-secondary btn-sm">
                                    Click aquí
                                </a>
                                @endif
                            </td>
                            <td class="text-center {{ $item->status?'bg-success':'bg-danger' }}">
                                @if($item->status)
                                    Procesado con éxito
                                @else
                                    Error al procesar
                                    {{ $item->error }}
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="1"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
@endsection

