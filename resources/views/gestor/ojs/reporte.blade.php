@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <canvas id="myChart" width="800" height="400" style="display: none;"></canvas>
        <div class="card">
            <div class="card-header">
                <form class="form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold col-md-2"> Conteo {{ $titulo }}</label>
                    <div class="form-group col-md-5">
                        <div class="input-group">
                            <select name="year" id="year" class="form-control">
                                @foreach($filtro_year as $year)
                                    <option  {{ app('request')->get('year',$ultimo_anio)==$year?'selected':''  }}>{{ $year }}</option>
                                @endforeach
                            </select>
                            <input type="text" name="filtro" id="filtro" class="form-control" placeholder="revista">
                            <div class="input-group-append">
                                <button class="btn btn-success " type="submit">
                                    <i class="fas fa-search"></i> Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-5 text-right">
                        <label>Registros al {{ $reporte->created_at->format('Y-m-d h:i a') }} </label>
                        <button type="button" id="evento" class="ml-2 btn btn-danger ">  <i class="fas fa-server"></i> Procesar
                        </button>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        {{--<th>id</th>--}}
                        <th>revista (uri)</th>
                        <th>enero</th>
                        <th>febrero</th>
                        <th>marzo</th>
                        <th>abril</th>
                        <th>mayo</th>
                        <th>junio</th>
                        <th>julio</th>
                        <th>agosto</th>
                        <th>setiembre</th>
                        <th>octubre</th>
                        <th>noviembre</th>
                        <th>diciembre</th>
                        <th >Total</th>
                    </tr>
                    </thead>
                    @forelse($journals as $journal)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">{{ $journal->path }}</td>
                            <td class="text-sm-right">{{ $journal->metric_01 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_02 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_03 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_04 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_05 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_06 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_07 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_08 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_09 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_10 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_11 }}</td>
                            <td class="text-sm-right">{{ $journal->metric_12 }}</td>
                            <td class="text-sm-right bg-primary">{{ $journal->metric_total }}</td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($journals->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $journals->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).on('change','#year,#month',function(){
                $(this).parent('form').submit();
            });

            $(document).on('click','#evento',function(e){
                e.preventDefault();
                data={procesar:true};

                var icono=$(this).find('i');
                icono.addClass('gly-spin');
                $.get("",data,function(){

                }).done(function(rs){
                    if(rs.status){
                        window.location.reload();
                    }else{
                        alert(rs.error)
                    }
                    icono.removeClass('gly-spin');
                }).fail(function(xhr,status,error){
                    icono.removeClass('gly-spin');
                    alert(error);
                })
            });
        </script>
        <script src="{{ asset('js/Chart.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [
                        {!! $grafico->getLabels() !!}
                    ],
                    datasets:[
                    @foreach($grafico->getJournals() as $id=>$journal)
                                <?php
                            $total=$grafico->getJournals()->count();

                            $escala=255*255*255/$total;
                            $posicion=$total-($id+1);

                            $r=(($posicion*$escala/255)/255);
                            $g=(($posicion*$escala/255)%255);
                            $b=($posicion*$escala%255);

                            $color_hexadecimal=sprintf("#%02x%02x%02x", $r,$g,$b );

                            //echo "// $total - $r $g $b \n";
                            ?>
                        {{ $id>0?",":"" }} {
                            label: '{{ $journal->path }}',
                            data:[
                                {{ $journal->metric_01 }},{{ $journal->metric_02 }},{{ $journal->metric_03 }},
                                {{ $journal->metric_04 }},{{ $journal->metric_05 }},{{ $journal->metric_06 }},
                                {{ $journal->metric_07 }},{{ $journal->metric_08 }},{{ $journal->metric_09 }},
                                {{ $journal->metric_10 }},{{ $journal->metric_11 }},{{ $journal->metric_12 }}
                            ],
                            fill:false,
                            lineTension:0,
                            backgroundColor: '{{ $color_hexadecimal }}',
                            borderColor: '{{ $color_hexadecimal }}',
                            hidden: {{ $id>10?'true':'false' }},
                        }
                    @endforeach
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            stacked: false,
                            fill: false
                        }]
                    }
                }
            });


            // filtrar items de tabla
            $("#filtro").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("table tr.item").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        </script>
    @endpush
    @push('estilos')
        <style type="text/css">
            .none{
                display: none;
            }
            .gly-spin {
                display:inline-block;
                -webkit-animation: spin 2s infinite linear;
                -moz-animation: spin 2s infinite linear;
                -o-animation: spin 2s infinite linear;
                animation: spin 2s infinite linear;
            }
            @-moz-keyframes spin {
                0% {
                    -moz-transform: rotate(0deg);
                }
                100% {
                    -moz-transform: rotate(359deg);
                }
            }
            @-webkit-keyframes spin {
                0% {
                    -webkit-transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(359deg);
                }
            }
            @-o-keyframes spin {
                0% {
                    -o-transform: rotate(0deg);
                }
                100% {
                    -o-transform: rotate(359deg);
                }
            }
            @keyframes spin {
                0% {
                    -webkit-transform: rotate(0deg);
                    transform: rotate(0deg);
                }
                100% {
                    -webkit-transform: rotate(359deg);
                    transform: rotate(359deg);
                }
            }
            .gly-rotate-90 {
                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                transform: rotate(90deg);
            }
            .gly-rotate-180 {
                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2);
                -webkit-transform: rotate(180deg);
                -moz-transform: rotate(180deg);
                -ms-transform: rotate(180deg);
                -o-transform: rotate(180deg);
                transform: rotate(180deg);
            }
            .gly-rotate-270 {
                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
                -webkit-transform: rotate(270deg);
                -moz-transform: rotate(270deg);
                -ms-transform: rotate(270deg);
                -o-transform: rotate(270deg);
                transform: rotate(270deg);
            }
            .gly-flip-horizontal {
                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1);
                -webkit-transform: scale(-1, 1);
                -moz-transform: scale(-1, 1);
                -ms-transform: scale(-1, 1);
                -o-transform: scale(-1, 1);
                transform: scale(-1, 1);
            }
            .gly-flip-vertical {
                filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1);
                -webkit-transform: scale(1, -1);
                -moz-transform: scale(1, -1);
                -ms-transform: scale(1, -1);
                -o-transform: scale(1, -1);
                transform: scale(1, -1);
            }
        </style>
    @endpush
@endsection
