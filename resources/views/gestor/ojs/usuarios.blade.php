@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')

        <div class="card">
            <div class="card-header">
                Usuarios
                <form action="" method="get">
                    <select name="journal_id" id="journal_id">
                        <option value="">Todos</option>
                        @foreach($journals as $journal)
                            <option value="{{$journal->journal_id}}" {{$journal->selected?'selected':''}}>{{ $journal->path }}</option>
                        @endforeach
                    </select>
                    <select name="group_id" id="group_id">
                        <option value="">Todos</option>
                        @foreach($grupos as $grupo)
                            <option value="{{$grupo->user_group_id}}" {{$grupo->selected?'selected':''}}>
                                @if($grupo->getName() instanceof \App\Modulos\OJS\v3\UserGroupSetting)
                                    {{ $grupo->user_group_id }} - {{ $grupo->getName()->setting_value }}
                                @else
                                    No definido
                                @endif
                                </option>
                        @endforeach
                    </select>

                    <select name="conteo_grupos" id="conteo_grupos">
                        <option value="0">Todos</option>
                        <option value="1" {{ $conteo_grupos=="1"?'selected':'' }}>Solo pertenecen a un grupo</option>
                        <option value="2" {{ $conteo_grupos=="2"?'selected':'' }}>pertenecen a mas de un grupo</option>
                    </select>
                    <button type="submit">Enviar</button>
                </form>
            </div>
            <div class="card-body">
                <div class="">
                    @foreach($usuarios->all() as $user)
                        @if($user->username!="agastelo" && $user->username!="udea")
                            php5.6 tools/mergeUsers.php agastelo {{ $user->username }}  <br>
                        @endif
                    @endforeach
                </div>
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <th>Grupos</th>
                    </tr>
                    </thead>
                    @forelse($usuarios as $user)
                        <tr class="item">
                            <td class="text-left">{{ $user->first_name }}</td>
                            <td class="text-left">{{ $user->last_name }}</td>
                            <td class="text-left">{{ $user->username }}</td>
                            <td class="text-sm-left">
                                {{ $user->email }}
                            </td>
                            <td>
                                <b>{{ $user->total_grupos }}</b> <small>( {{ $user->grupos }} )</small>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="1"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse

                        <tfooter>
                            <tr>
                                <td colspan="5">
                                    <div class="float-left">
                                        @if($usuarios->hasPages()) {{ $usuarios->links() }}@endif
                                    </div>
                                    <div class="float-right">
                                        Total: {{ $usuarios->total() }} usuarios
                                    </div>
                                </td>
                            </tr>
                        </tfooter>

                </table>
            </div>
        </div>

    </div>

@endsection
