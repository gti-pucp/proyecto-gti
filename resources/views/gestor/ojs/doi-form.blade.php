@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="" method="post">
            @csrf
            <h4>Indicaciones:</h4>
            <ul>
                <li> Debe seleccionar el orden de los valores:</li>
                <li> Los valores deben ser url completas </li>
                <li> Las url deben estar separadas por un espacio </li>
            </ul>
            <code class="bg-dark text-white">
                http://revistas.pucp.edu.pe/index.php/sordaysonora/article/view/21815 https://doi.org/10.18800/sordaysonora.201901.001<br>
                http://revistas.pucp.edu.pe/index.php/sordaysonora/article/view/21811 https://doi.org/10.18800/sordaysonora.201901.002
            </code>
            <textarea name="doi_multiple" class="form-control mb-2"  cols="30" rows="15" placeholder=""></textarea>

            FORMATO DE INSERCION <select name="orden_parametros" id="" style="display: block">
                <option value="url_doi">URL REVISTAS - DOI</option>
                <option value="doi_url">DOI - URL REVISTAS</option>
            </select>
            <button type="submit" class="btn btn-success float-right">
                Guardar
            </button>
        </form>
    </div>
@endsection

