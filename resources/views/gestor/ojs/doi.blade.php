@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')

        <div class="card">
            <div class="card-header">
                Revistas
            </div>
            <div class="card-body">
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>revista (uri)</th>
                    </tr>
                    </thead>
                    @forelse($journals as $journal)
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">
                                <a href="{{ $journal->url }}">{{ $journal->path }}</a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="1"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
{{--                    @if($journals->hasPages())--}}
{{--                        <tfooter>--}}
{{--                            <tr>--}}
{{--                                <td colspan="3">{{ $journals->links() }}</td>--}}
{{--                            </tr>--}}
{{--                        </tfooter>--}}
{{--                    @endif--}}
                </table>
            </div>
        </div>

    </div>

@endsection
