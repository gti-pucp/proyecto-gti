@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')

        <div class="card">
            <div class="card-header">
                Revistas <a href="{{ $journal->url }}">{{ $journal->path }}</a> : <a href="{{ $issue->link }}" target="_blank">{{ $issue->number }} ({{ $issue->year }})</a>
            </div>
            <div class="card-body">
                <table class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr>
                        <th>submission</th>
                        <th>Titulo</th>
                        <th>Abstract</th>
                        <th>Author</th>
                        <th>Afiliacion</th>
                        <th>Resumen</th>
                        <th>Keywords</th>
{{--                        <th>doi_suffix</th>--}}
{{--                        <th>pub_id::doi</th>--}}
                    </tr>
                    </thead>
                    @forelse($submissions as $submission)
                        <tr class="item">
                            <td>
                                <a href="{{ $submission->link }}" target="_blank">
                                    {{ $submission->submission_id }}
                                </a>
                            </td>
                            <td>
                                {{ $submission->titulo }}
                            </td>
                            <td>
                                {{ $submission->autor }}
                            </td>
                            <td>
                                {{ $submission->afiliacion }}
                            </td>
                            <td>
                                {{ $submission->resumen }}
                            </td>
                            <td>
                                {{ $submission->keywords }}
                            </td>
                            <td class="text-sm-left">
                                {{ $submission->doi_suffix }}
                            </td>
                            <td>
                                {{ $submission->pub_id_doi}}
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="1"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
{{--                    @if($journals->hasPages())--}}
{{--                        <tfooter>--}}
{{--                            <tr>--}}
{{--                                <td colspan="3">{{ $journals->links() }}</td>--}}
{{--                            </tr>--}}
{{--                        </tfooter>--}}
{{--                    @endif--}}
                </table>
            </div>
        </div>

    </div>

@endsection
