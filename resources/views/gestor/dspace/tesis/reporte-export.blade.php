<table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm ">
    <thead class="thead-dark">
    <tr class="text-center">
        {{--<th>id</th>--}}
        <th rowspan="2">Agente</th>
        <th rowspan="2" >Email</th>
        <th colspan="12">Mes</th>
        <th rowspan="2">Total</th>
    </tr>
    <tr>
        <th alt="enero">01</th>
        <th>02</th>
        <th>03</th>
        <th>04</th>
        <th>05</th>
        <th>06</th>
        <th>07</th>
        <th>08</th>
        <th>09</th>
        <th>10</th>
        <th>11</th>
        <th>12</th>
    </tr>
    </thead>
    <tbody>
    @foreach($agentes as $agente)
        {{--                {{ dd($agente) }}--}}
        <tr class="item">
            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
            <td class="text-sm-left">
                {{ ucwords(mb_strtolower($agente->apellidos,'UTF-8')) }},{{ ucwords(mb_strtolower($agente->nombres),'UTF-8') }}
            </td>
            <td  class="text-sm-left">{{ $agente->email }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_01 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_02 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_03 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_04 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_05 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_06 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_07 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_08 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_09 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_10 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_11 }}</td>
            <td class="text-sm-right">{{ (int)$agente->metric_12 }}</td>
            <td class="text-sm-right bg-primary"><strong>{{ (int)$agente->metric_total }}</strong></td>
        </tr>


    @endforeach
    </tbody>
    <tr>
        <td colspan="2"></td>
        <td class="text-sm-right"><strong>{{ $total['metric_01'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_02'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_03'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_04'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_05'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_06'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_07'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_08'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_09'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_10'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_11'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_12'] }}</strong></td>
        <td class="text-sm-right"><strong>{{ $total['metric_total'] }}</strong></td>
    </tr>
</table>
