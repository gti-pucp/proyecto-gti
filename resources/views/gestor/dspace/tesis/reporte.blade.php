@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <canvas id="myChart" width="800" height="400" style="display: none;"></canvas>
        <div class="card">
            <div class="card-header">
                <form class="form form-inline " action="" method="get">
                    <label class="card-title text-capitalize d-inline col-md-4"> Conteo de {{ $titulo }}</label>
                    <div class="form-group col-md-7 ">
                        <label for="">
                            conteo de {{ $titulo }} del año:
                        </label>
                        <input type="number" class="form-control form-control-sm" name="year" value="{{ $year }}">
                        <button class="btn btn-success btn-sm ml-2" type="submit">
                            <i class="fas fa-search"></i> Filtrar
                        </button>
                        <a href="{{ route('dspace.tesis.reporte.exportar',['year'=>$year]) }}" class="btn btn-primary btn-sm">
                            <i class="fas fa-download"></i>
                            descargar
                        </a>
                    </div>
                    {{--<input type="submit" name="evento" value="Procesar registros" class="btn btn-danger">--}}

                </form>
            </div>
            <div class="card-body">


                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm ">
                    <thead class="thead-dark">
                    <tr class="text-center">
                        {{--<th>id</th>--}}
                        <th rowspan="2">Agente</th>
                        <th rowspan="2" >Email</th>
                        <th colspan="12">Mes</th>
                        <th rowspan="2">Total</th>
                    </tr>
                    <tr>
                        <th alt="enero">01</th>
                        <th>02</th>
                        <th>03</th>
                        <th>04</th>
                        <th>05</th>
                        <th>06</th>
                        <th>07</th>
                        <th>08</th>
                        <th>09</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                    </tr>
                    </thead>
                    @forelse($agentes as $agente)
                        {{--                {{ dd($agente) }}--}}
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">
                                {{ ucwords(mb_strtolower($agente->apellidos,'UTF-8')) }},{{ ucwords(mb_strtolower($agente->nombres),'UTF-8') }}
                            </td>
                            <td class="text-sm-left">{{ $agente->email }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_01 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_02 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_03 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_04 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_05 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_06 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_07 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_08 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_09 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_10 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_11 }}</td>
                            <td class="text-sm-right">{{ (int)$agente->metric_12 }}</td>
                            <td class="text-sm-right bg-primary"><strong>{{ (int)$agente->metric_total }}</strong></td>
                        </tr>
                        @if($loop->last)
                            <tr>
                                <td colspan="2"></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_01'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_02'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_03'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_04'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_05'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_06'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_07'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_08'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_09'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_10'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_11'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_12'] }}</strong></td>
                                <td class="text-sm-right"><strong>{{ $total['metric_total'] }}</strong></td>
                            </tr>
                        @endif
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($agentes->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="14">{{ $agentes->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>

    <div class="container ">
        {{--        <canvas id="myChart" width="800" height="400" style="display: none;"></canvas>--}}
        <form class="form form-inline m-2" action="">
            <div class="form-group col-md-7 ">

{{--                <select name="year" id="year" class="form-control">--}}
{{--                    @foreach($filtro_year as $year)--}}
{{--                        <option  {{ app('request')->get('year',2019)==$year?'selected':''  }}>{{ $year }}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--                <input type="text" name="filtro" id="filtro" class="form-control" placeholder="revista">--}}
{{--                <input type="submit" name="filtro" value="Consultar" class="btn btn-success ">--}}
            </div>
            {{--<input type="submit" name="evento" value="Procesar registros" class="btn btn-danger">--}}
            <div class="form-group col-md-5">
                {{--                <label>Registros exportados al {{ $reporte->created_at->format('Y-m-d h:i a') }} </label> <button type="button" id="evento" class="btn btn-danger float-right"> Procesar data <i class="glyphicon glyphicon-repeat none"></i></button>--}}
            </div>


        </form>

    </div>
@endsection
