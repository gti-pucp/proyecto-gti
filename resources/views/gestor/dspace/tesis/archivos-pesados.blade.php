@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container ">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <form class="form form-inline" action="">
                    <label class="card-title text-capitalize d-inline col-md-4">
                        {{ $titulo }}
                    </label>
                    <div class="form-group  col-md-8">
                        <select name="tipo" id="tipo" class="form-control form-control-sm">
                            @foreach($combo_tipos as $option_tipo)
                                <option value="{{ $option_tipo->id }}" {{ $option_tipo->id==$tipo?'selected':''  }}>{{ $option_tipo->valor }}</option>
                            @endforeach
                        </select>
                        Tamaño minimo
                        <input type="number" name="peso" id="peso" class="form-control form-control-sm" placeholder="2" min="1" value="{{ $peso }}">MB
                        <input type="submit" name="filtro" value="Consultar" class="btn btn-success btn-sm">
                    </div>
                    {{--<input type="submit" name="evento" value="Procesar registros" class="btn btn-danger">--}}
                    <div class="form-group col-md-5">
                        {{--                <label>Registros exportados al {{ $reporte->created_at->format('Y-m-d h:i a') }} </label> <button type="button" id="evento" class="btn btn-danger float-right"> Procesar data <i class="glyphicon glyphicon-repeat none"></i></button>--}}
                    </div>


                </form>
            </div>
            <div class="card-body">
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th>Archivo</th>
                        <th>Peso(MB) </th>
                    </tr>
                    </thead>
                    @forelse($bitstreams as $bitstream)
                        {{--                {{ dd($agente) }}--}}
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">
                                {{--                        {{ $bitstream->uuid }}--}}
                                {{--                        <br>--}}
                                Coleccion: <a target="_blank" href="{{ $url_base.$bitstream->handle_coleccion }}">{{ $bitstream->titulo_coleccion }}</a><br>

                                Titulo: <a target="_blank" href="{{ $url_base.$bitstream->handle_item }}">{{ $bitstream->titulo_item }}</a><br>

                                {{--                        {{ $bitstream->uuid }}--}}
                                Archivo: {{ $bitstream->titulo }}
                            </td>
                            <td class="text-sm-right ">
                                {{ round($bitstream->size_bytes/1024/1024,2) }}
                            </td>
                        </tr>

                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="2"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($bitstreams->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="2">{{ $bitstreams->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>


    </div>
@endsection
