@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container ">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <form class="form form-inline" action="">
                    <label class="card-title text-capitalize d-inline col-md-4">
                        {{ $titulo }}
                    </label>
                    <div class="form-group  col-md-8">
                        <input type="text" class="form-control" name="buscar" value="{{ $buscar }}">
                        <input type="submit" name="filtro" value="Consultar" class="btn btn-success" placeholder="Filtrar por ...">
                    </div>
                    {{--<input type="submit" name="evento" value="Procesar registros" class="btn btn-danger">--}}
                    <div class="form-group col-md-5">
                        {{--                <label>Registros exportados al {{ $reporte->created_at->format('Y-m-d h:i a') }} </label> <button type="button" id="evento" class="btn btn-danger float-right"> Procesar data <i class="glyphicon glyphicon-repeat none"></i></button>--}}
                    </div>


                </form>
            </div>
            <div class="card-body">
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th>Colección</th>
                        <th>Estado</th>
                        <th>Ultima cosecha</th>
                    </tr>
                    </thead>
                    @forelse($colecciones_cosechadas as $coleccion)

                        <tr class="item">
                            <td class="text-sm-left">
                                Coleccion: <a target="_blank" href="{{ $url_base.$coleccion->handle_coleccion }}">{{ $coleccion->titulo }}</a><br>
                            </td>
                            <td>
                                @if($coleccion->harvest_status==$coleccion::STATUS_COSECHADO)
                                    <div class="btn btn-success btn-sm">Cosechado</div>
                                @elseif($coleccion->harvest_status==$coleccion::STATUS_ERROR)
                                    <div class="btn btn-danger btn-sm">Error</div>
                                @endif
                            </td>
                            <td class="text-sm-right ">
                                @if(isset($coleccion->harvest_start_time))
                                  {{ $coleccion->harvest_start_time->format("Y-m-d h:i:s a") }}
                                @else
                                   No ha sido cosechado
                                @endif
                            </td>
                        </tr>

                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="2"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($colecciones_cosechadas->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="2">{{ $colecciones_cosechadas->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>


    </div>
@endsection
