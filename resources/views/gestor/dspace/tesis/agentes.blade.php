@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">
                    <form action="" class="form form-inline" method="POST">
                        {{ @csrf_field() }}
                        <label class="card-title text-capitalize font-weight-bold d-inline col-md-4">Asignar usuario al reporte</label>
                        <div class="input-group col-md-8">
{{--                            <select name="permiso" id="permiso" class="form-control">--}}
{{--                                @foreach($combo_permisos as $permiso)--}}
{{--                                    <option value="{{ $permiso->id }}">{{ $permiso->titulo }}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
                            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder=" Ingrese el email del usuario ">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-success">
                                    <i class="fas fa-plus-square"></i>
                                    Asignar
                                </button>
                            </div>

                        </div>

                    </form>
                </div>
                <div class="card-body">
                    @error('email')
                    <div class="alert alert-danger ">{{ $message }}</div>
                    @enderror
                    <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                        <thead class="thead-dark text-center">
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        @forelse($agentes as $agente)
                            <tr class="item">
                                <td class="">{{ $agente->eperson->apellidos}}, {{$agente->eperson->nombres }}</td>
                                <td class="">{{ $agente->eperson->email }}</td>
                                <td class="text-center">
                                    <a class="btn btn-outline-danger btn-delete" href="{{ $agente->url_eliminar}}" data-modal="Confirmar eliminación de <strong>{{ $agente->eperson->email }}</strong>">
                                        <i class="fas fa-user-times"></i> Remover
                                    </a>
                                </td>

                            </tr>
                        @empty
                            <tfooter>
                                <tr>
                                    <td colspan="4"> No hay registros</td>
                                </tr>
                            </tfooter>
                        @endforelse
                        @if($agentes->hasPages())
                            <tfooter>
                                <tr>
                                    <td colspan="4">{{ $agentes->links() }}</td>
                                </tr>
                            </tfooter>
                        @endif
                    </table>
                </div>
            </div>

        </div>
    @include('layouts.action.delete')
    @endsection

