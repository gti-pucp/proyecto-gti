@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                <form class="form form-inline " action="" method="get">
                    <label class="card-title text-capitalize d-inline col-md-4">
                        {{ $titulo }}
                    </label>
                </form>
            </div>
            <div class="card-body">
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th>Item</th>
                    </tr>
                    </thead>
                    @forelse($items as $item)
                        {{--                {{ dd($agente) }}--}}
                        <tr class="item">
                            {{--<td class="text-right">{{ $journal->journal_id }}</td>--}}
                            <td class="text-sm-left">
                                Colección: <a target="_blank" href="{{ $url_base.$item->handle_coleccion }}">{{ $item->titulo_coleccion }}</a>
                                <br>
                                Item: <a target="_blank" href="{{ $url_base.$item->handle }}">{{ $item->titulo }}</a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($items->hasPages())
                        <tfooter>
                            <tr>
                                <td >{{ $items->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>

@endsection
