@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container ">
        <div class="card">
            <div class="card-header">
                Modulo EZPROXY

            </div>
            <div class="card-body">
                <h5 class="card-title text-uppercase font-weight-bold">listado de reportes EZProxy</h5>

                <table id="tabla_revistas" class="table table-responsive-sm table-bordered table-hover">
                    <thead class="thead-dark">
                    <tr>
                        {{--<th>id</th>--}}
                        <th>nombre</th>
                        <th>fecha</th>
                        <th>opcion</th>
                    </tr>
                    </thead>
                    @forelse($files as $file)
                        <tr class="item">
                            <td class="text-left">{{ $file->nombre  }}</td>
                            <td class="text-left">{{ $file->fecha }}</td>
                            <td class="text-right">
                                <a href="{{ $file->url_descarga }}" class="btn btn-sm btn-outline-success">
                                    <i class="fas fa-download"></i>
                                    Descargar
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse

                </table>
            </div>
        </div>

    </div>

@endsection
