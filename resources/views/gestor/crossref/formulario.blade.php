@extends('layouts.app')
@section('content')
    <div class="container">
        <form class="form" action="" method="post">
            @csrf
            <div class="form-row">
                <div class="col">
                    <input type="text" class="form-control" name="url" placeholder="url de repositorio">
                </div>
                <div class="col">
                    <select name="type" class="form-control">
                        <option value="edited_book">Edited Book</option>
                        <option value="monograph">Monograph</option>
                    </select>
                </div>
                <div class="col">
                    <input type="text" class="form-control" name="doi" placeholder="doi">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-success">Enviar</button>
                </div>
            </div>
            @error('url')
            <div>
                <div class="alert alert-danger">{{ $message }}</div>
            </div>
            @enderror
        </form>
    </div>
@endsection
