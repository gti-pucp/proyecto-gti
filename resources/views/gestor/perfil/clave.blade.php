@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.notificacion')
        {{--<a href="{{ route('usuario.index') }}" class="btn btn-secondary btn-sm right">--}}
            {{--<i class="fas fa-chevron-left"></i>--}}
            {{--Volver--}}
        {{--</a>--}}
        <div class="card">
            <div class="card-header">
                Perfil de usuario
            </div>
            <div class="card-body">
                <h5 class="card-title">Cambiar contraseña</h5>

                <form class="form" method="POST" action="" autocomplete="off">
                    @csrf
                    @if($usuario->id>0)
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <label for="current_password">Current password</label>
                        <input type="password" name="current_password" class="form-control" id="current_password" aria-describedby="current_password" placeholder="Enter current password"  autocomplete="off" value="">
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('current_password'))
                            <small class="bg-danger text-white">{{ $errors->first('current_password') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="new_password">Nuevo password</label>
                        <input type="password" name="new_password" class="form-control" id="new_password" aria-describedby="new_password" placeholder="Enter new password"  autocomplete="off" value="">
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('password'))
                            <small class="bg-danger text-white">{{ $errors->first('password') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Confirm password</label>
                        <input type="password" name="confirm_password" class="form-control" id="confirm_password" aria-describedby="confirm_password" placeholder="Confirm new password"  autocomplete="off" value="">
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('confirm_password'))
                            <small class="bg-danger text-white">{{ $errors->first('confirm_password') }}</small>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-success btn-sm">
                        <i class="fas fa-save"></i>
                        Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection
