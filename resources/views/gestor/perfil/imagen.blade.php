@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.notificacion')
        {{--<a href="{{ route('usuario.index') }}" class="btn btn-secondary btn-sm right">--}}
            {{--<i class="fas fa-chevron-left"></i>--}}
            {{--Volver--}}
        {{--</a>--}}
        <div class="card" style="width: 32rem; margin:auto" >
            <div class="card-header">

                <form class="form form-inline " method="POST" action="" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @if($usuario->id>0)
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <input type="file" id="imagen" name="imagen"  class="form-control-file" lang="es">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">
                                <i class="fas fa-file-upload"></i> Subir
                            </button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="card-body">
                @error('imagen')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                @if($usuario->imagen)
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{ route('perfil.imagen') }}" alt="Card image cap">
                            <form class="form form-inline m-2" method="POST" action="" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm col-md-12">
                                    <i class="fas fa-trash"></i>
                                    Eliminar</button>
                            </form>
                    </div>
                    @else
                    No ha subido ninguna imagen
                @endif
            </div>
        </div>
    </div>
@endsection
