@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.notificacion')
        {{--<a href="{{ route('usuario.index') }}" class="btn btn-secondary btn-sm right">--}}
            {{--<i class="fas fa-chevron-left"></i>--}}
            {{--Volver--}}
        {{--</a>--}}
        <div class="card">
            <div class="card-header">
                Perfil de usuario
            </div>
            <div class="card-body">
                <h5 class="card-title">Editar informacion</h5>

                <form class="form" method="POST" action="" autocomplete="off">
                    @csrf
                    @if($usuario->id>0)
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="name" placeholder="Enter name"  autocomplete="off" value="{{ old('name',$usuario->name) }}">
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('name'))
                            <small class="bg-danger text-white">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email"  autocomplete="off" value="{{ old('email',$usuario->email) }}" readonly>
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('email'))
                            <small class="bg-danger text-white">{{ $errors->first('email') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="api_token">API token</label>
                        <input type="api_token" name="api_token" class="form-control" id="api_token" aria-describedby="emailHelp" placeholder="Enter email"  autocomplete="off" value="{{ old('email',$usuario->api_token) }}" readonly>
                        {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        @if($errors->has('email'))
                            <small class="bg-danger text-white">{{ $errors->first('email') }}</small>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-edit"></i> Guardar
                    </button>
                </form>

                {{--<a href="{{ route('perfil.edit') }}" class="btn btn-primary"> <i class="fas fa-edit"></i> Editar</a>--}}
            </div>
        </div>

    </div>
@endsection
