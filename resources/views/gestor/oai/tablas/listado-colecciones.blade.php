<div class="col-8 offset-2 ">
    <div class="card ">
        <div class="card-header">
            Listado de colecciones detectadas en registros: <strong>total {{ count(\App\Modulos\OAI\XmlEstadisticas::$COLECCIONES) }} colecciones</strong>
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>Coleccion</th>
                    <th>Nro registros</th>
                </tr>
                </thead>


                @forelse(\App\Modulos\OAI\XmlEstadisticas::$COLECCIONES as $coleccion)
                    <tr class="item">
                        <td class="">
                            <a href="{{ $base_url_web.$coleccion->handle }}/recent-submissions">{{ $coleccion->handle }}</a>
                        </td>
                        <td class="text-right">
                            {{ $coleccion->total_registros }}
                        </td>
                    </tr>
                @empty
                    No hay archivos
                @endforelse

            </table>
        </div>
    </div>
</div>
