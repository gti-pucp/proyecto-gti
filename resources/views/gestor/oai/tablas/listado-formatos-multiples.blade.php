<div class="col-8 offset-2 ">
    <div class="card ">
        <div class="card-header">
            Listado de registros con formato multiple <strong>Total: {{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_FORMATO_MULTIPLE) }} registros</strong>
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th class="w-25">Acceso web</th>
                    <th class="w-25">Acceso OAI</th>
                    <th class="w-25">Archivo</th>
                    <th class="w-25">Colecciones asociadas</th>
                </tr>
                </thead>
                @forelse(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_FORMATO_MULTIPLE as $registro)
                    <tr class="item">
                        <td>
                            <a href="{{ $base_url_web.$registro->handle }}">{{ $registro->handle }} </a>
                        </td>
                        <td>
                            <a href="{{ $base_url_oai.$registro->identifier }}">URL OAI</a>
                        </td>
                        <td>
                            {{ basename($registro->archivo) }}
                        </td>
                        <td>
                            @foreach($registro->formatos as $formato)
                                {{ $formato }}<br>
                            @endforeach
                        </td>
                    </tr>
                @empty
                    No hay archivos
                @endforelse
            </table>
        </div>
    </div>
</div>
