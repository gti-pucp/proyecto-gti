<div class="col-6">
    <div class="card ">
        <div class="card-header">
            Resumen de segun estado de registro
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>Descripcion</th>
                    <th>Nro registros</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Total de registros</th>
                    <th class="text-right">{{ \App\Modulos\OAI\XmlEstadisticas::$CONTEO_REGISTROS }}</th>
                </tr>
                <tr>
                    <td> - Registros eliminados</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_ELIMINADOS) }}</td>
                </tr>
                <tr>
                    <td> - Registros habilitados</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_HABILITADOS) }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
