<div class="col-6 mt-4  ">
    <div class="card ">
        <div class="card-header">
            Resumen de tipo de acceso
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>Tipo de acceso</th>
                    <th>Nro de registros</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Acceso abierto</td>
                    <td class="text-right">{{ \App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_ABIERTO }}</td>
                </tr>
                <tr>
                    <td>Acceso restringido</td>
                    <td class="text-right">{{ \App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_RESTRINGIDO }}</td>
                </tr>
                <tr>
                    <td>Acceso Embargado</td>
                    <td class="text-right">{{ \App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_EMBARGADO }}</td>
                </tr>
                <tr class="">
                    <td>
                        <strong>
                            Total
                        </strong>
                    </td>
                    <td class="text-right">
                        <strong>
                            {{ (\App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_ABIERTO+\App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_RESTRINGIDO+\App\Modulos\OAI\XmlEstadisticas::$CONTEO_ACCESO_EMBARGADO) }}
                        </strong>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
