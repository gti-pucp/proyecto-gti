<div class="col-6">
    <div class="card ">
        <div class="card-header">
            Resumen de la extracción OAI
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>Descripcion</th>
                    <th>Nro registros</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Total de archivos XML generados</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_XML) }}</td>
                </tr>
                <tr>
                    <td>Total de colecciones detectadas</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$COLECCIONES) }}</td>
                </tr>
                <tr>
                    <td>Total de registros con coleccion multiple</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_COLECCION_MULTIPLE) }}</td>
                </tr>
                <tr>
                    <td>Total de registros con formato multiple</td>
                    <td class="text-right">{{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_FORMATO_MULTIPLE) }}</td>
                </tr>
                <tr>
                    <td class="text-center" colspan="2">
                        <a href="{{ route('gestor.oai.descarga') }}" class="btn btn-success">Descargar ZIP</a>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
