<div class="col-8 offset-2 ">
    <div class="card ">
        <div class="card-header">
            Listado de registros con coleccion multiple <strong>Total: {{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_COLECCION_MULTIPLE) }} registros</strong>
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>URL WEB</th>
                    <th>URL OAI</th>
                    <th>Archivo</th>
                    <th>Colecciones asociadas</th>
                </tr>
                </thead>
                @forelse(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_COLECCION_MULTIPLE as $registro)
                    <tr class="">
                        <td class="col-4">
                            <a href="{{ $base_url_web.$registro->handle }}">{{ $registro->handle }}</a>

                        </td>
                        <td class="col-4">
                            <a href="{{ $base_url_oai.$registro->identifier }}">{{ $registro->identifier }}</a>
                        </td>
                        <td>
                            {{ basename($registro->archivo) }}
                        </td>
                        <td class="col-4">
                            @foreach($registro->colecciones as $coleccion)
                                <a href="{{ $base_url_web.$coleccion }}" target="_blank">{{ $coleccion }}</a><br>
                            @endforeach
                        </td>
                    </tr>
                @empty
                    No hay archivos
                @endforelse
            </table>
        </div>
    </div>
</div>
