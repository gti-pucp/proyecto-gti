<div class="col-8 offset-2 ">
    <div class="card ">
        <div class="card-header">
            Listado de registros eliminados <strong>Total: {{ count(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_ELIMINADOS) }} registros</strong>
        </div>
        <div class="card-body">
            <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                <thead class="thead-dark text-center">
                <tr>
                    <th>Handle</th>
                    <th>OAI</th>
                    <th>Archivo XML</th>
                </tr>
                </thead>
                @forelse(\App\Modulos\OAI\XmlEstadisticas::$REGISTROS_ELIMINADOS as $registro)
                    <tr class="item">
                        <td >
                            {{ $registro->handle }}
                        </td>
                        <td>
                            <a href="{{ $base_url_oai.$registro->identifier }}">{{ $registro->identifier }}</a>
                        </td>
                        <td>
                            {{ $registro->archivo }}
                        </td>
                    </tr>
                @empty
                    No hay archivos
                @endforelse
            </table>
        </div>
    </div>
</div>
