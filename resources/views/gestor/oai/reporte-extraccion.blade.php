@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    @include('layouts.fragment.breadcrumbs')
    @include('layouts.fragment.notificacion')
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#resumen" role="tab" aria-controls="home" aria-selected="true">Resumen</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#colecciones" role="tab" aria-controls="contact" aria-selected="false">Colecciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#colecciones-multiples" role="tab" aria-controls="col-mul" aria-selected="false">Colecciones multiples</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#formatos_multiples" role="tab" aria-controls="contact" aria-selected="false">Formatos multiples</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#registros_eliminados" role="tab" aria-controls="contact" aria-selected="false">Registro eliminados</a>
            </li>

        </ul>
        <div class="tab-content " id="myTabContent">
                <div class="tab-pane border p-4 fade show active " id="resumen" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row ">
                        @include('gestor.oai.tablas.resumen-extraccion')
                        @include('gestor.oai.tablas.resumen-estados')
                        @include('gestor.oai.tablas.resumen-accesos')
                    </div>
                </div>
                <div class="tab-pane border p-4 fade " id="colecciones" role="tabpanel" aria-labelledby="contact-tab">
                    @include('gestor.oai.tablas.listado-colecciones')
                </div>
                <div class="tab-pane border p-4 fade " id="colecciones-multiples" role="tabpanel" aria-labelledby="profile-tab">
                    @include('gestor.oai.tablas.listado-colecciones-multiples')
                </div>
                <div class="tab-pane border p-4 fade " id="registros_eliminados" role="tabpanel" aria-labelledby="profile-tab">
                    @include('gestor.oai.tablas.listado-registros-eliminados')
                </div>
                <div class="tab-pane border p-4 fade " id="formatos_multiples" role="tabpanel" aria-labelledby="profile-tab">
                    @include('gestor.oai.tablas.listado-formatos-multiples')
                </div>
        </div>
    </div>
@endsection
