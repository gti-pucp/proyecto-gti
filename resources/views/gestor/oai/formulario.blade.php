@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">
                {{ $titulo }}
            </div>
            <div class="card-body">
                <form method="POST" action="">
                    @csrf
                    <div class="form-group">
                        <label for="paquete">URL</label>
                        <input type="text" class="form-control" name="url" placeholder="Por ejemplo: http://tesis.pucp.edu.pe/oai/request" >
                        @error('url')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

{{--                    <div class="form-group">--}}
{{--                        <label for="paquete">TIPO</label>--}}

{{--                        <select name="tipo" id="" class="form-control">--}}
{{--                            <option value="0">XML ORIGINAL</option>--}}
{{--                            <option value="1">XML DEPURADO</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}

                    <button type="submit" class="btn btn-success float-right">
                        <i class="fas fa-download"></i> registrar
                    </button>
                </form>
            </div>
        </div>

    </div>

@endsection
