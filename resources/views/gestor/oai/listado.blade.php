@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">

                <form class="form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold d-inline col-md-4">{{ $titulo }}</label>
                    <div class="input-group col-md-7">
                        <input type="text" name="buscar" class="form-control" placeholder="Campo de busqueda" aria-label="Ingresar usuario o correo" aria-describedby="basic-addon2" value="{{ old('buscar',app('request')->get('buscar')) }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i> Filtrar</button>
                        </div>
                    </div>
{{--                    <a href="{{ route('gestor.oai.crear')}}" class="btn btn-outline-success  col-md-1">--}}
{{--                        <i class="fas fa-user-plus"></i>--}}
{{--                        Crear--}}
{{--                    </a>--}}
                </form>
            </div>
            <div class="card-body">
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark text-center">
                        <tr>
                            <th>Fuente</th>
                            <th>Extraccion</th>
                        </tr>
                    </thead>
                    @forelse($fuentes as $fuente)
                        <tr class="item">
                            <td class="">
                                <a href="{{ $fuente->getUrlIdentifier() }}" target="_blank">{{ $fuente->url }}</a>
                            </td>
                            <td class="text-center">
                                <a href="{{ route('gestor.oai.extractor',[$fuente->id,'depurado'=>0]) }}" class="btn btn-success btn-sm mr-3" target="_blank">XML ORIGINAL OAI</a>
                                <a href="{{ route('gestor.oai.extractor',[$fuente->id,'depurado'=>1]) }}" class="btn btn-primary btn-sm ml-3" target="_blank">XML DEPURADO OAI</a>
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="2"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($fuentes->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="4">{{ $fuentes->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
    @include('layouts.action.delete')
@endsection
