@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
{{--        <a href="{{ route('gestor.servidores.listado') }}" class="btn btn-secondary mb-2">Volver</a>--}}
        <form method="POST" action="" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="paquete">Paquete</label>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Archivo ZIP</span>
                    </div>
                    <div class="custom-file">
                        <input id="paquete" name="paquete" type="file" class="custom-file-input @error('paquete') is-invalid @enderror">
                        <label class="custom-file-label" for="paquete">Seleccionar</label>
                    </div>
                </div>
                @error('paquete')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

{{--            <div class="form-group">--}}
{{--                <label for="correo_responsable">Correo responsable</label>--}}
{{--                <input id="correo_responsable" name="correo_responsable" type="text" class="form-control @error('correo_responsable') is-invalid @enderror" value="{{ old('correo_responsable',$servidor->correo_responsable) }}">--}}
{{--                @error('correo_responsable')--}}
{{--                <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                @enderror--}}
{{--            </div>--}}

{{--            <div class="form-group">--}}
{{--                <label for="comprobar">Comprobar</label>--}}
{{--                <input id="comprobar" name="comprobar" type="text" class="form-control @error('comprobar') is-invalid @enderror" value="{{ old('comprobar',$servidor->comprobar) }}">--}}
{{--                @error('comprobar')--}}
{{--                <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                @enderror--}}
{{--            </div>--}}

            <button type="submit" class="btn btn-success float-right">
                <i class="fas fa-save"></i> Guardar
            </button>
        </form>
    </div>
@endsection
