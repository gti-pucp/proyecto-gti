@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">
                <a href="{{ route('gestor.usuarios.listado') }}" class="btn btn-secondary mb-2">
                    <i class="fas fa-chevron-left"></i> Volver
                </a>
                {{ $titulo }}
            </div>
            <div class="card-body">
                <form method="POST" action="">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input id="nombre" name="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" value="{{ old('nombre',$usuario->name) }}">
                        @error('nombre')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Correo </label>

                        <input id="correo" name="correo" type="text" class="form-control @error('correo') is-invalid @enderror" value="{{ old('correo',$usuario->email) }}">
                        @error('correo')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success float-right">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                </form>
            </div>
        </div>

    </div>


@endsection
