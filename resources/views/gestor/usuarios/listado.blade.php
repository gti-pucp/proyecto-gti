@extends('layouts.app')
@section('menu')
    @include('gestor.menu')

@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">

                <form class="form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold d-inline col-md-4">{{ $titulo }}</label>
                    <div class="input-group col-md-7">
                        <input type="text" name="buscar" class="form-control" placeholder="Campo de busqueda" aria-label="Ingresar usuario o correo" aria-describedby="basic-addon2" value="{{ old('buscar',app('request')->get('buscar')) }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i> Filtrar</button>
                        </div>
                    </div>
                    <a href="{{ route('gestor.usuarios.crear')}}" class="btn btn-outline-success  col-md-1">
                        <i class="fas fa-user-plus"></i>
                        Crear
                    </a>
                </form>
            </div>
            <div class="card-body">


                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark text-center">
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Permisos</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    @forelse($listado_usuarios as $usuario)
                        <tr class="item">
                            <td class="">{{ $usuario->name }}</td>
                            <td class="">{{ $usuario->email }}</td>
                            <td class="text-center">
                                @can('viewAny',\App\Modulos\Usuarios\Permiso::class)
                                    <a href="{{ $usuario->url_permisos }}" class="btn btn-outline-primary "><i class="fas fa-shield-alt"></i> Permisos</a>
                                @else
                                    No tiene permisos
                                @endcan
                                @can('changePassword',$usuario)
                                    <a href="{{ $usuario->url_reset }}" class="btn btn-outline-warning btn-confirm" data-modal="confirmar generacion de clave para <strong>{{ $usuario->email }}</strong>">
                                        <i class="fas fa-refresh"></i> Generar clave
                                    </a>
                                @endcan
                            </td>
                            <td class="text-center">
                                @can('update',$usuario)
                                <a href="{{ $usuario->url_editar }}" class="btn btn-outline-warning ">
                                    <i class="fas fa-user-edit"></i> Editar
                                </a>
                                @endcan
                                @can('delete',$usuario)
                                <a class="btn btn-outline-danger btn-delete" href="{{ $usuario->url_eliminar}}" data-modal="Confirmar eliminación de <strong>{{ $usuario->email }}</strong>">
                                    <i class="fas fa-user-times"></i> Eliminar
                                </a>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="4"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado_usuarios->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="4">{{ $listado_usuarios->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
    @include('layouts.action.delete')
@endsection
