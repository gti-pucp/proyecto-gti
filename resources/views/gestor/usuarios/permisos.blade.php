@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        <div class="card">
            <div class="card-header">
                    <form action="" class="form form-inline" method="POST">
                        {{ @csrf_field() }}
                        <label class="card-title text-capitalize font-weight-bold d-inline col-md-4">Permisos de {{ $usuario->name }}</label>
                        @if(Auth::user()->can('create',\App\Modulos\Usuarios\Permiso::class) && $combo_permisos->count()>0)
                        <div class="input-group col-md-8">
                            <select name="permiso" id="permiso" class="form-control">
                                @foreach($combo_permisos as $permiso)
                                    <option value="{{ $permiso->id }}">{{ $permiso->titulo }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-success">
                                    <i class="fas fa-plus-square"></i>
                                    Asignar
                                </button>
                            </div>
                        </div>
                        @endif
                    </form>


                </div>
                <div class="card-body">


                    <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                        <thead class="thead-dark text-center">
                        <tr>
                            <th>Modulo</th>
                            <th>Descripcion</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        @forelse($listado_permisos as $permiso)
                            <tr class="item">
                                <td class="">{{ $permiso->titulo }}</td>
                                <td class="">{{ $permiso->descripcion }}</td>
                                <td class="text-center">
                                    @can('delete',$permiso)
                                        <form action="" class="form " method="POST">
                                            {{ @method_field('delete') }}
                                            {{ @csrf_field() }}
                                            <input type="hidden" name="id_permiso" value="{{ $permiso->id }}">
                                            <button type="submit" class="btn btn-outline-danger" {{ Auth::user()->cannot('delete',$permiso)?'disabled':'' }}>
                                                <i class="fas fa-ban"></i>
                                                Remover
                                            </button>
                                        </form>
                                    @else
                                        No tiene permisos
                                    @endcan
                                </td>

                            </tr>
                        @empty
                            <tfooter>
                                <tr>
                                    <td colspan="4"> No hay registros</td>
                                </tr>
                            </tfooter>
                        @endforelse
                        @if($listado_permisos->hasPages())
                            <tfooter>
                                <tr>
                                    <td colspan="4">{{ $listado_permisos->links() }}</td>
                                </tr>
                            </tfooter>
                        @endif
                    </table>
                </div>
            </div>

        </div>

    @endsection
    @push('scripts')
        <script>
            $(document).on('click','a.btn_action_remover_permiso',function(e){
                e.preventDefault();
                action=$(this).attr('href');
                $("form#form_remover_permiso").attr("action",action).submit();
            })
        </script>
    @endpush
