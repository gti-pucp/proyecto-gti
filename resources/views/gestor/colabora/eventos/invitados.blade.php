@extends('layouts.app')
@section('menu')
    @include('admin.menu')
@endsection
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Modulo Eventos > {{ $evento->descripcion }} a las {{ $evento->fecha_hora }}
            </div>
            <div class="card-body">
                <h5 class="card-title">Listado de eventos</h5>
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead>
                    <tr>
                        {{--<th>Evento</th>--}}
                        <th>Fecha - Hora</th>
                        <th>Estado</th>
                        <th>Opcion</th>
                    </tr>
                    </thead>
                    @forelse($listado_invitados as $invitado)
                        <tr class="item">
                            {{--<td class="text-right">{{ $invitado->id }}</td>--}}
                            <td class="text-left"></td>

                            <td>

                            </td>
                            <td class="text-center">

                            </td>

                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado_invitados->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $listado_invitados->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
@endsection
