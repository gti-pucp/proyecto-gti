@extends('layouts.app')
@section('menu')
    @include('admin.menu')
@endsection
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Modulo Eventos
            </div>
            <div class="card-body">
                <h5 class="card-title text-uppercase font-weight-bold">{{ ($evento->id==0?'crear':'editar')." evento" }}</h5>
                @include('admin.usuarios.notificacion')
                <form action="" class="form " method="POST">
                    {{ @csrf_field() }}
                    @if($evento->id>0)
                        {{ method_field('PUT') }}
                    @endif
                    <div class="form-group ">
                        <label for="" class="text-capitalize">tipo</label>
                        {{--<input type="text" class="form-control" name="tipo" value="{{ old('tipo',$evento->tipo) }}">--}}
                        <select name="tipo" class="form-control {{ $errors->has('tipo')?'is-invalid':'' }}">
                            <option value="">Seleccionar tipo</option>
                            <option value="{{ \App\Modulos\SB\Evento::SORTEO }}">Sorteo</option>
                            <option value="{{ \App\Modulos\SB\Evento::COLECTA }}">Colecta</option>
                        </select>

                        <div class="invalid-feedback">Example invalid custom select feedback</div>
                    </div>
                    <div class="form-group ">
                        <label for="" class="text-capitalize">título</label>
                        <input type="text" class="form-control {{ $errors->has('titulo')?'is-invalid':'' }}" name="titulo" value="{{ old('titulo',$evento->titulo) }}">
                        <div class="invalid-feedback">{{ $errors->first('titulo') }}</div>
                    </div>
                    <div class="form-group ">
                        <label for="" class="text-capitalize">descripción</label>
                        {{--<input type="text" class="form-control {{ $errors->has('titulo')?'is-invalid':'' }}" name="titulo" value="{{ old('titulo',$evento->titulo) }}">--}}
                        <textarea name="descripcion" class="form-control {{ $errors->has('descripcion')?'is-invalid':'' }}" rows="5">{{ old('descripcion',$evento->descripcion) }}</textarea>
                        <div class="invalid-feedback">{{ $errors->first('titulo') }}</div>
                    </div>

                    <div class="form-group ">
                        <label for="" class="text-capitalize">fecha de evento</label>
                        <input type="text" class="form-control {{ $errors->has('fecha')?'is-invalid':'' }}" name="fecha" value="{{ old('fecha',$evento->fecha) }}">
                        <div class="invalid-feedback">{{ $errors->first('fecha') }}</div>
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-outline-success right">
                            <i class="fas fa-save"></i>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>

    @push('scripts')
        <script>
            $('#fecha').datetimepicker({

            });
        </script>
    @endpush
@endsection
