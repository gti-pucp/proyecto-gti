@extends('layouts.app')
@section('menu')
    @include('admin.menu')
@endsection
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Modulo Eventos
            </div>
            <div class="card-body">
                <h5 class="card-title">Listado de eventos</h5>
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead>
                    <tr>
                        <th>Evento</th>
                        <th>Fecha - Hora</th>
                        <th>Tipo</th>
                        <th>
                            Invitados
                        </th>
                        <th>Opcion</th>
                    </tr>
                    </thead>
                    @forelse($listado_eventos as $evento)
                        <tr class="item">
                            <td class="text-right">{{ $evento->id }}</td>
                            <td class="text-left">{{ $evento->fecha_hora->format('Y-m-d') }} a las  <strong>   {{ $evento->fecha_hora->format('h:i a') }}</strong></td>

                            <td class="text-left">
                                @if($evento->tipo==\App\Modulos\SB\Evento::SORTEO)
                                Sorteo
                                @elseif($evento->tipo==\App\Modulos\SB\Evento::COLECTA)
                                    Colaboración
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{ $evento->url_invitados }}" class="btn btn-primary"> Ver invitaciones</a>
                            </td>
                            <td class="text-center">
                                <a href="{{ $evento->url_editar }}" class="btn btn-outline-warning "><i class="fas fa-user-edit"></i> Editar</a>
                                <a href="{{ $evento->url_eliminar}}" class="btn btn-outline-danger "><i class="fas fa-user-times"></i> Eliminar</a>
                            </td>

                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado_eventos->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $listado_eventos->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
@endsection
