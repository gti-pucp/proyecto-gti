@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Modulo Eventos
            </div>
            <div class="card-body">
                <h5 class="card-title">Listado de eventos</h5>
                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead>
                    <tr>
                        {{--<th>Evento</th>--}}
                        <th>Fecha - Hora</th>
                        <th>Estado</th>
                        <th>Opcion</th>
                    </tr>
                    </thead>
                    @forelse($listado_sorteos as $sorteo)
                        <tr class="item">
                            {{--<td class="text-right">{{ $sorteo->id }}</td>--}}
                            <td class="text-left">{{ $sorteo->fecha_hora->format('Y-m-d') }} a las  <strong>   {{ $sorteo->fecha_hora->format('h:i a') }}</strong></td>

                            <td>
                                @if($sorteo->sorteados()->count()>0)
                                    <a href="{{ $sorteo->url_editar }}" class="btn btn-outline-warning "><i class="fas fa-user-edit"></i> Ver resultados</a>
                                @else
                                    Pendiente
                                @endif
                            </td>
                            <td class="text-center">
                                @if($sorteo->estaInscrito(Auth::user()))
                                    <a href="{{ $sorteo->url_editar }}" class="btn btn-outline-warning "><i class="fas fa-user-edit"></i> Inscribir</a>
                                @else
                                    Inscrito
                                    {{--<a href="{{ $sorteo->url_eliminar}}" class="btn btn-outline-danger "><i class="fas fa-user-times"></i> Eliminar</a>--}}
                                @endif
                            </td>

                        </tr>
                    @empty
                        <tfooter>
                            <tr>
                                <td colspan="14"> No hay registros</td>
                            </tr>
                        </tfooter>
                    @endforelse
                    @if($listado_sorteos->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="3">{{ $listado_sorteos->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
@endsection
