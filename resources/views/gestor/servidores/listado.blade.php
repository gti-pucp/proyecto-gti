@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">
                <form class="form form-inline" action="" method="get">
                    <label class="card-title text-uppercase font-weight-bold d-inline col-md-4">listado de servidores</label>
                    <div class="input-group col-md-6">
                        <input type="text" name="buscar" class="form-control" placeholder="Campo de busqueda" aria-label="Campo de busqueda" aria-describedby="basic-addon2" value="{{ old('buscar',app('request')->get('buscar')) }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"> <i class="fas fa-search"></i> Filtrar</button>
                        </div>

                    </div>
                    <a href="{{ route('gestor.servidores.crear') }}" class="btn btn-outline-success col-md-2">
                        <i class="fas fa-plus"></i> Crear
                    </a>
                </form>
            </div>
            <div class="card-body">


                <table id="tabla_revistas" class="table table-default table-bordered table-hover table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th class="text-center">URL</th>
                        <th class="text-center">Correo responsable</th>
                        <th class="text-center">Ultima vez en linea</th>
                        <th class="text-center">% limite espacio</th>
                        <th class="text-center">Caida notificada</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Opciones</th>
                    </tr>
                    </thead>
                    @forelse($listado_servidores as $servidor)
                        <tr>
                            <td>{{ $servidor->url }}</td>
                            <td>
                                <strong>{{ $servidor->correo_responsable }}</strong>
                                @if(isset($servidor->correos_adicionales))
                                    <br>
                                    <small>
                                        @foreach(explode(",",$servidor->correos_adicionales) as $pos=>$correo)
                                            {!! ($pos>0?',<br>cc: ':'cc: ').$correo !!}
                                        @endforeach
                                    </small>
                                @endif
                            </td>
                            <td>{{ $servidor->fecha_online }}</td>
                            <td>{{ $servidor->limite_espacio }}%</td>
                            <td>{{ $servidor->fecha_notificacion}}</td>
                            <td>
                                @if($servidor->comprobar)
                                    Monitoreando
                                @else
                                    Sin monitoreo
                                @endif
                            </td>
                            <td class="text-center">

                                @can('update',$servidor)
                                    <a href="{{ $servidor->url_editar }}" class="btn btn-outline-warning ">
                                        <i class="fas fa-edit"></i> Editar
                                    </a>
                                @endcan
                                @can('delete',$servidor)
                                    <a class="btn btn-outline-danger btn-delete" href="{{ $servidor->url_eliminar}}" data-modal="confirmar eliminarcion">
                                        <i class="fas fa-trash"></i> Eliminar
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">No hay servidores</td>
                        </tr>
                    @endforelse
                    @if($listado_servidores->hasPages())
                        <tfooter>
                            <tr>
                                <td colspan="4">{{ $listado_servidores->links() }}</td>
                            </tr>
                        </tfooter>
                    @endif
                </table>
            </div>
        </div>

    </div>
    @include('layouts.action.delete')
@endsection
