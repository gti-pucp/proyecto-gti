@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
    <div class="container">
        @include('layouts.fragment.breadcrumbs')
        @include('layouts.fragment.notificacion')
        <div class="card">
            <div class="card-header">
                <a href="{{ route('gestor.servidores.listado') }}" class="btn btn-secondary mb-2">
                    <i class="fas fa-chevron-left"></i> Volver
                </a>
                {{ $titulo }}
            </div>
            <div class="card-body">
                <form method="POST" action="">
                    @csrf
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input id="url" name="url" type="text" class="form-control @error('url') is-invalid @enderror" value="{{ old('url',$servidor->url) }}">
                        @error('url')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="limite_espacio">% Limite de espacio usado</label>
                        <input id="limite_espacio" name="limite_espacio" type="text" class="form-control @error('limite_espacio') is-invalid @enderror" value="{{ old('url',$servidor->limite_espacio) }}">
                        <span>Notificará cuando llegue o sobrepase el limite indicado</span>
                        @error('url')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="correo_responsable">Correo responsable</label>

                        <input id="correo_responsable" name="correo_responsable" type="text" class="form-control @error('correo_responsable') is-invalid @enderror" value="{{ old('correo_responsable',$servidor->correo_responsable) }}">
                        @error('correo_responsable')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="correos_adicionales">Correo adicional (separado por comas en caso sea mas de uno)</label>

                        <input id="correos_adicionales" name="correos_adicionales" type="text" class="form-control @error('correos_adicionales') is-invalid @enderror" value="{{ old('correos_adicionales',$servidor->correos_adicionales) }}"  autocomplete="false">
                        @error('correos_adicionales')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="comprobar">Comprobación de estado</label>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input @error('comprobar') is-invalid @enderror" id="comprobar" name="comprobar" {{ old('comprobar',$servidor->comprobar)!=""?'checked':'' }}>
                            <label class="custom-control-label" for="comprobar">Comprobar y notificar caidas de servidor</label>
                        </div>
                        @error('comprobar')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success float-right">
                        <i class="fas fa-save"></i> Guardar
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection
