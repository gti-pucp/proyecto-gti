@auth()
    @can("viewAny",\App\Modulos\Usuarios\Usuario::class)
        <li class="nav-item">
            <a href="{{ route('gestor.usuarios.listado') }}" class="nav-link">
                <i class="fas fa-users"></i>    Usuarios
            </a>
        </li>
    @endcan
    @if ( Auth::user()->can('viewAny', \App\Modulos\Monitoreo\Servidor::class)
        || Auth::user()->can('viewAny', \App\Modulos\EZPROXY\ReporteEZProxy::class)
        || Auth::user()->can('viewAny', \App\Modulos\OAI\Comprimido::class) )
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-newspaper"></i> Herramientas
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                @can("viewAny",\App\Modulos\Monitoreo\Servidor::class)
                    <a href="{{ route('gestor.servidores.listado') }}" class="nav-link">
                        <i class="fas fa-server"></i> Servidores
                    </a>
                @endcan
                @can('viewAny',\App\Modulos\EZPROXY\ReporteEZProxy::class)
                    <a href=" {{ route('gestor.ezproxy.listado') }}" class="nav-link">
                        <i class="fas fa-book"></i>  EZProxy
                    </a>
                @endcan
                @can("viewAny",\App\Modulos\OAI\Comprimido::class)
                    <a href="{{ route('gestor.oai.listado') }}" class="nav-link">
                        <i class="fas fa-code"></i> OAI extractor
                    </a>
                @endcan
            </div>
        </li>
    @endif

    @can('viewAny',\App\Modulos\OJS\v2\Reporte::class)
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-newspaper"></i> Revistas
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('revistas3.reporte.visitas.anual') }}">Resumen visitas OJS3</a>
                <a class="dropdown-item" href="{{ route('revistas3.reporte.descargas.anual') }}">Resumen descargas OJS3</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('revistas3.doi.registrar') }}">Registrar DOIS</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('revistas2.reporte.visitas.anual') }}">Resumen visitas OJS2</a>
                <a class="dropdown-item" href="{{ route('revistas2.reporte.descargas.anual') }}">Resumen descargas OJS2</a>

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('revistas3.licencias') }}">Licencias registradas</a>

            </div>
        </li>
    @endcan
    @can('viewAny',\App\Modulos\OSTickets\Reporte::class)
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="far fa-newspaper"></i> Mesa de Ayuda
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('ostickets.reporte.agentes') }}">Reporte Agentes</a>
                <a class="dropdown-item" href="{{ route('ostickets.reporte.usuarios') }}">Reporte Usuarios</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('ostickets.reporte.general') }}">Reporte General</a>

            </div>
        </li>
    @endcan
    @can('viewAny',\App\Modulos\Tesis\Reporte::class)
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="far fa-newspaper"></i> Repo Tesis
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('dspace.tesis.agentes.listado') }}">Configurar usuarios</a>
            <a class="dropdown-item" href="{{ route('dspace.tesis.reporte') }}">Reporte subidas</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('dspace.tesis.pesados') }}">Archivos Pesados</a>
            <a class="dropdown-item" href="{{ route('dspace.tesis.pendientes') }}">Sin Thumbnails</a>
{{--            <a class="dropdown-item" href="{{ route('dspace.tesis.cosechas') }}">Ultimas cosechas</a>--}}
        </div>
    </li>
    @endcan
    @can('viewAny',\App\Modulos\RI\Reporte::class)
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="far fa-newspaper"></i> Repo Institucional
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('dspace.ri.agentes.listado') }}">Configurar usuarios</a>
            <a class="dropdown-item" href="{{ route('dspace.ri.reporte') }}">Reporte subidas</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('dspace.ri.pesados') }}">Archivos Pesados</a>
            <a class="dropdown-item" href="{{ route('dspace.ri.pendientes') }}">Sin Thumbnails</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('dspace.ri.cosechas') }}">Cosechas</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('dspace.ri.duplicados') }}">Duplicados</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('dspace.ri.oai-null') }}">Nullpointer OAI</a>
        </div>
    </li>
    @endcan
{{--   --}}
    @can('viewAny',\App\Modulos\SB\Evento::class)
{{--        <li class="nav-item">--}}
{{--            <a href=" {{ route('eventos.index') }}" class="nav-link"><i class="fas fa-calendar-alt"></i> Eventos</a>--}}
{{--        </li>&nbsp;--}}
    @endcan


    @can('viewAny',\App\Modulos\SB\Evento::class)
{{--        <li class="nav-item">--}}
{{--            <a href=" {{ route('sorteos.index') }}" class="nav-link"><i class="fas fa-gift"></i>  Sorteos</a>--}}
{{--        </li>&nbsp;--}}
        {{--<li class="nav-item dropdown">--}}
            {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--Sorteos--}}
            {{--</a>--}}
            {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
                {{--<a class="dropdown-item" href="#"></a>--}}
                {{--<a class="dropdown-item" href="#">Sin participar</a>--}}
                {{--<a class="dropdown-item" href="#">Something else here</a>--}}
            {{--</div>--}}
        {{--</li>--}}
    @endcan
    @can('viewAny',\App\Modulos\SB\Evento::class)
        <li class="nav-item">
            <a href=" {{ route('colaboraciones.index') }}" class="nav-link"><i class="fas fa-donate"></i> Colaboraciones</a>
        </li>&nbsp;
        {{--<li class="nav-item dropdown">--}}
        {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
        {{--Sorteos--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">--}}
        {{--<a class="dropdown-item" href="#"></a>--}}
        {{--<a class="dropdown-item" href="#">Sin participar</a>--}}
        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
        {{--</div>--}}
        {{--</li>--}}
    @endcan
@endauth
