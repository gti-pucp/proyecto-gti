@component('mail::message')
# Introduction
Se ha generado una nueva cuenta contraseña, para ingresar por favor haz click en el siguiente enlace

@component('mail::button', ['url' => env('APP_URL') ])
    Iniciar sesión
@endcomponent
<br>
Su usuario: <strong> {{ $email }} </strong><br>
Su clave:  <strong> {{ $clave }} </strong>
<br>
Gracias,<br>
{{ config('app.name') }}
@endcomponent
