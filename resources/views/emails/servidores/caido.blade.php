@component('mail::message')
# Fuera de servicio
<br>
<a href="{{ $servidor->url }}">{{ $servidor->url }}</a>

@component('mail::button', ['url' => $servidor->url])
Visitar web
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
