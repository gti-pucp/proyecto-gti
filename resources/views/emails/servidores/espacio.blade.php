@component('mail::message')
# Limite espacio de disco
<br>
<a href="{{ $servidor->url }}">{{ $servidor->url }}</a>

Espacio Total : {{ $servidor->getEspacioTotal() }}<br>
Espacio Ocupado : {{ $servidor->getEspacioOcupado() }}<br>

@component('mail::button', ['url' => $servidor->url, 'color' => $servidor->porcentaje_usado>90?'danger':'warning'])
    Porcentaje de uso : {{ $servidor->porcentaje_usado }}%
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
