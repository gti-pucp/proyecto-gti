@extends('layouts.app')
@section('menu')
    @include('gestor.menu')
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Estas logueado.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scriptsx')
    <script>
        const socket=io("c055724.pucp.edu.pe:3000");

        socket.on('chat:message',(data)=>{

            textarea=$("textarea#mensaje");
            if(textarea.html()===""){
                contenido=data.mensaje;
            }else{
                contenido=textarea.val()+"\n"+data.mensaje;
            }
            textarea.html(contenido);

            console.log("mi usuario es "+data.usuario+" desde servidor");
        });

        $(document).on('click','a.btn_enviar',function(e){
           e.preventDefault();
           etiqueta_mensaje=$("form #mensaje");
            socket.emit('chat:message',{
                usuario:'charly',
                clave:'clave',
                mensaje:etiqueta_mensaje.val()
            });
            etiqueta_mensaje.val("");
        });


        $(document).on('click','a.btn_conectar_canal',function(e){
            e.preventDefault();
            identificador=$("#canal").val();

            console.log('conectando a '+identificador);
            socket.emit("subscribe",{"room":identificador});

            //canal.emit('prueba','aqui va');

        });


        $(document).on('click','a.btn_desconectar_canal',function(e){
            e.preventDefault();
            identificador=$("#canal").val();

            console.log('desconectando de '+identificador);
            socket.emit("unsubscribe",{"room":identificador});
        });

        $(document).on('click','a.btn_enviar_canal',function(e) {
            e.preventDefault();

            console.log(socket);
            //     socket.emit('chat:message_canal',{
            //         mensaje:$("form #mensaje").val()
            //     });
            // });

            identificador = $("#canal").val();

            socket.io.to(identificador).emit("prueba", {valor: "x"});
        });


        socket.on('event', function(data){
            console.log('event');
            console.log(data);
        });
    </script>
@endpush

@push('scriptsx')
    <script>
        Echo.join(`chat.1`)
            .here((users) => {
                //
            })
            .joining((user) => {
                console.log(user.name);
            })
            .leaving((user) => {
                console.log(user.name);
            });
    </script>
    @endpush
