@section('content')
    <div class="container">
        <div class="alert alert-danger text-center" role="alert">
            <h4 class="alert-heading"> @yield('code'):@yield('message')</h4>
            <hr>
            <p class="mb-0 " >Por favor contacta al administrador</p>
        </div>
    </div>
@endsection
