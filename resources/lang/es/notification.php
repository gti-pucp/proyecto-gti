<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'regards' => 'Saludos',
    'hello' => 'Hola!',
    'alternateLink'=>"Si tienes problemas para clickear el boton \":actionText\", copia y pega la url mostrada a continuación \n en tu navegador web: [:actionURL](:actionURL)",
];
