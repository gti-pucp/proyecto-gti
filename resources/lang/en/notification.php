<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'regards' => 'Regards trans',
    'hello' => 'Hello!',
    'alternateLink'=>"If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n into your web browser: [:actionURL](:actionURL)",
];
