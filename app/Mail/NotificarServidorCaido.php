<?php

namespace App\Mail;

use App\Modulos\Monitoreo\Servidor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarServidorCaido extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $servidor;
    public function __construct(Servidor $servidor)
    {
        //
        $this->servidor=$servidor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $this->subject("ALERTA: Caida de ".$this->servidor->url)
            ->to($this->servidor->correo_responsable,"responsable")
            ->markdown('emails.servidores.caido')
            ->onQueue('emails')
            ->with('servidor',$this->servidor);

        $correos_adicionales=$this->servidor->getCorreosAdicionalesCC();
        foreach($correos_adicionales as $correo) {
            $this->cc($correo);
        }

        return $this;
    }
}
