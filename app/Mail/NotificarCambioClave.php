<?php

namespace App\Mail;

use App\Modulos\Usuarios\Usuario;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificarCambioClave extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $usuario;
    private $clave;

    public function __construct(Usuario $usuario, $clave)
    {
        //
        $this->usuario=$usuario;
        $this->clave=$clave;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->subject("Notificación: Cambio de clave")
            ->to($this->usuario->email,$this->usuario->name)
            ->markdown('emails.usuarios.clave')
            ->onQueue('emails')
            ->with('email',(string)$this->usuario->email)
            ->with('clave',(string)$this->clave);


        return $this;
    }
}
