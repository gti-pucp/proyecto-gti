<?php

namespace App\Modulos\Chat;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    //
    protected $table="mensajes";

    protected $fillable=['id_canal','id_usuario','mensaje'];

    public function autor(){
        return $this->belongsTo(User::class,'id_usuario','id');
    }

    public function canal(){
        return $this->belongsTo(Canal::class,'id_canal','id');
    }
}
