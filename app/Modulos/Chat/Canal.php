<?php

namespace App\Modulos\Chat;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Canal extends Model
{
    //
    protected $table="canales";
    protected $fillable=['nombre'];

    public function miembros(){
        return $this->belongsToMany(User::class,"canales_usuarios","id_canal","id_usuario");
    }

    public function mensajes(){
        return $this->hasMany(Mensaje::class,'id_canal','id');
    }
}
