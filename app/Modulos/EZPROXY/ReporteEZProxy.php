<?php

namespace App\Modulos\EZPROXY;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ReporteEZProxy extends Model
{
    //
    protected $table="reportes_e_z_proxy";
    protected $dates=['fecha_reporte'];

    const DIRECTORIO_BASE = "D:/py/ezproxy-testeo/";

    public function scopeFiltrarReporte($query,$anio,$mes){
        $query->where(DB::raw("YEAR(fecha_reporte)"),$anio)
            ->where(DB::raw("MONTH(fecha_reporte)"),$mes);
        return $query;
    }

    public function scopeExiste($query,$archivo,$anio,$mes){
        $query->where('ruta',$archivo);
        $query->filtrarReporte($anio,$mes);

        return $query;
    }

    public function getComprimido(){
        $ruta_sistema=Storage::disk('archivos-ezproxy')->path($this->ruta);

        return $ruta_sistema;
    }

    public function scopeObtenerFechas($query){
        $query->select(DB::raw('distinct(fecha_reporte)'))->orderBy('fecha_reporte','desc');
        return $query->get();
    }
}
