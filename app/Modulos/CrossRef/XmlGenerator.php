<?php

namespace App\Modulos\CrossRef;

use App\Modulos\DSPACE\v62\MetadataValue;
use Carbon\Carbon;
use DOMDocument;
use DOMElement;
use Illuminate\Database\Eloquent\Model;

class XmlGenerator extends Model
{

    private $book_type;

    //DOM INSTANCE
    private $dom;

    public function cargarItemFromDSPACE($item,$book_type,$doi_number=""){
        //HEAD
        $timestamp=Carbon::now()->format("YmdHis");
        $microtime=Carbon::now()->format("YmdHis");

        $this->data_timestamp=$timestamp.substr($microtime,0,3);
        $this->data_registrant="WEB-FORM";
        $this->data_depositor_name="sdbi";
        $this->data_email_address="icanales@pucp.edu.pe";

        $this->data_doi_batch_id=$this->extraerDoiBatchIdFromDspace($item);

        //BODY
        $this->data_book_type=$book_type;

        $this->data_contributors=$this->extraerContributorsFromDspace($item);
        $this->data_title=$this->extraerTitleFromDspace($item);
        $this->data_abstract=$this->extraerAbstractFromDspace($item);
        $this->data_publication_date_print=$this->extraerPublicationDatePrintFromDspace($item);
        $this->data_publication_date_online=$this->extraerPublicationDateOnlineFromDspace($item);
        $this->data_isbn=$this->extraerISBNFromDspace($item);
        $this->data_publisher=$this->extraerPublisherFromDspace($item);
        $this->data_doi=$this->extraerDOIFromDspace($item,$doi_number);
        $this->data_resource=$this->extraerURIFromDspace($item);
    }

    private function extraerDoiBatchIdFromDspace($item){
        return $item->uuid;
    }

    public function generarXML(){
        $this->dom=new DOMDocument("1.0","UTF-8");

        $doi_batch_container=$this->dom->createElementNS("http://www.crossref.org/schema/4.4.2","doi_batch");
        $doi_batch_container->setAttribute("version","4.4.2");
        $doi_batch_container->setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        $doi_batch_container->setAttribute("xmlns:jats","http://www.ncbi.nlm.nih.gov/JATS1");
        $doi_batch_container->setAttribute("xsi:schemaLocation","http://www.crossref.org/schema/4.4.2 http://www.crossref.org/schema/deposit/crossref4.4.2.xsd");

        $this->crearHead($doi_batch_container);
        $this->crearBody($doi_batch_container);

        $this->dom->appendChild($doi_batch_container);

        return $this->dom;
    }

    private function crearHead(DOMElement $doi_batch_container){

        $doi_batch_id=$this->dom->createElement("doi_batch_id",$this->data_doi_batch_id);
        $timestamp=$this->dom->createElement("timestamp",$this->data_timestamp);
        $registrant=$this->dom->createElement("registrant",$this->data_registrant);

        $depositor_name=$this->dom->createElement("depositor_name",$this->data_depositor_name);
        $depositor_email=$this->dom->createElement("email_address",$this->data_email_address);
        $depositor=$this->dom->createElement("depositor");
        $depositor->appendChild($depositor_name);
        $depositor->appendChild($depositor_email);

        //agregando al head
        $head=$this->dom->createElement("head");
        $head->appendChild($doi_batch_id);
        $head->appendChild($timestamp);
        $head->appendChild($depositor);
        $head->appendChild($registrant);

        $doi_batch_container->appendChild($head);
    }

    private function crearBody(DOMElement $doi_batch_container){
        //METADATA
        $book_metadata=$this->dom->createElement("book_metadata");

        if(isset($this->data_contributors)){
            $book_metadata->appendChild($this->generarLabelContributors());
        }

        if(isset($this->data_title)){
            $book_metadata->appendChild($this->generarLabelTitles());
        }

        if(isset($this->data_abstract)){
            $book_metadata->appendChild($this->generarLabelAbstract());
        }

        if(isset($this->data_publication_date_print)){
            $book_metadata->appendChild($this->generarLabelPublicationDatePrint());
        }

        if(isset($this->data_publication_date_online)){
            $book_metadata->appendChild($this->generarLabelPublicationDateOnline());
        }

        if(isset($this->data_isbn)){
            $book_metadata->appendChild($this->generarLabelISBN());
        }

        if(isset($this->data_publisher)){
            $book_metadata->appendChild($this->generarLabelPublisher());
        }

        $book_metadata->appendChild($this->generarLabelDOIData());

        //book
        $book=$this->dom->createElement("book");
        $book->setAttribute("book_type",$this->data_book_type);
        $book->appendChild($book_metadata);

        //agregamdp body
        $body=$this->dom->createElement("body");
        $body->appendChild($book);

        $doi_batch_container->appendChild($body);
    }

    private function createPersonName( $sequence,$role,$given_name,$surname,$orcid_url){
        $given_name=$this->dom->createElement("given_name",$given_name);
        $surname=$this->dom->createElement("surname",$surname);


        //first person
        $person_name=$this->dom->createElement("person_name");
        $person_name->setAttribute("sequence",$sequence);
        $person_name->setAttribute("contributor_role",$role);
        $person_name->appendChild($given_name);
        $person_name->appendChild($surname);

        if(isset($orcid_url) && $orcid_url!=""){
            $orcid=$this->dom->createElement("ORCID",$orcid_url);
            $person_name->appendChild($orcid);
        }

        return $person_name;
    }

    private function generarLabelContributors()
    {
        $contributors=$this->dom->createElement("contributors");

        foreach($this->data_contributors as $person){
            $label_person=$this->createPersonName($person->sequence,
                $person->role,
                $person->nombre,
                $person->apellido,
                $person->orcid
            );
            $contributors->appendChild($label_person);
        }


        return $contributors;
    }

    private function generarLabelTitles()
    {

        //titles
        $title=$this->dom->createElement("title",$this->data_title);

        $titles=$this->dom->createElement("titles");
        $titles->appendChild($title);

        return $titles;
    }

    private function generarLabelAbstract()
    {

        $content=str_replace("\r","",$this->data_abstract);

        $abstract_p=$this->dom->createElement("jats:p",$content);

        $abstract=$this->dom->createElement("jats:abstract");
        $abstract->setAttribute("xml:lang","es");
        $abstract->appendChild($abstract_p);
        return $abstract;
    }

    private function generarLabelPublicationDatePrint()
    {
        if(isset($this->data_publication_date_print)){
            $publication_date_print_year=$this->dom->createElement("year",$this->data_publication_date_print);

            $publication_date_print=$this->dom->createElement("publication_date");
            $publication_date_print->setAttribute("media_type","print");
            $publication_date_print->appendChild($publication_date_print_year);

            return $publication_date_print;
        }
    }

    private function generarLabelPublicationDateOnline()
    {
        if(isset($this->data_publication_date_online)){
            $publication_date_online_year=$this->dom->createElement("year",$this->data_publication_date_online);

            $publication_date_online=$this->dom->createElement("publication_date");
            $publication_date_online->setAttribute("media_type","online");
            $publication_date_online->appendChild($publication_date_online_year);

            return $publication_date_online;
        }
    }

    private function generarLabelISBN()
    {
        if(isset($this->data_isbn)) {
            $isbn = $this->dom->createElement("isbn", $this->data_isbn);

            return $isbn;
        }
    }

    private function generarLabelPublisher()
    {
        if(isset($this->data_publisher)) {
            $publisher_name = $this->dom->createElement("publisher_name", $this->data_publisher);
            $publisher = $this->dom->createElement("publisher");
            $publisher->appendChild($publisher_name);

            return $publisher;
        }
    }

    private function generarLabelDOIData()
    {
        $doi_data=$this->dom->createElement("doi_data");

        if(isset($this->data_doi)){
            $doi=$this->dom->createElement("doi",$this->data_doi);
            $doi_data->appendChild($doi);
        }

        if(isset($this->data_resource)){
            $resource=$this->dom->createElement("resource",$this->data_resource);
            $doi_data->appendChild($resource);
        }

        return $doi_data;
    }

    private function extraerPublicationDateOnlineFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return  $mdv->metadataFieldRegistry->element=="date" && $mdv->metadataFieldRegistry->qualifier=="available";
        })->first();

        if($rs instanceof MetadataValue){

            if(strpos($rs->text_value,"Z")!==false && strpos($rs->text_value,"-")!==false){
                list($year)=explode("-",$rs->text_value);
                return $year;
            }
        }
    }

    private function extraerISBNFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return $mdv->metadataFieldRegistry->element=="identifier" && $mdv->metadataFieldRegistry->qualifier=="isbn";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }
    }

    private function extraerPublisherFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return $mdv->metadataFieldRegistry->element=="publisher";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }
    }

    private function extraerContributorsFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return $mdv->metadataFieldRegistry->element=="contributor" && $mdv->metadataFieldRegistry->qualifier=="author";
        });

        $collection=collect();

        $pos=0;
        foreach($rs as $mdv){
            $pos++;
            list($nombre,$apellido)=explode(",",$mdv->text_value);
            $sequence=$pos==1?'first':'additional';

            $collection->push( (object) [
                'sequence'=>$sequence,
                'role'=>'author',
                'nombre'=>trim($nombre),
                'apellido'=>trim($apellido),
                'orcid'=>"",
            ]);

        }
        return $collection;
    }

    private function extraerTitleFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return  $mdv->metadataFieldRegistry->element=="title";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }
    }

    private function extraerAbstractFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return  $mdv->metadataFieldRegistry->element=="description" && $mdv->metadataFieldRegistry->qualifier=="abstract";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }
    }

    private function extraerPublicationDatePrintFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return  $mdv->metadataFieldRegistry->element=="date" && $mdv->metadataFieldRegistry->qualifier=="issued";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }
    }

    private function extraerDOIFromDspace($item,$doi_number)
    {
        $uri=parse_url($doi_number,PHP_URL_PATH);
        $doi_number=substr($uri,1);

        return $doi_number;
    }

    private function extraerURIFromDspace($item)
    {
        $rs=$item->metadatavalue->filter(function($mdv,$key){
            return  $mdv->metadataFieldRegistry->element=="identifier" && $mdv->metadataFieldRegistry->qualifier=="uri";
        })->first();

        if($rs instanceof MetadataValue){
            return $rs->text_value;
        }

    }

}
