<?php


namespace App\Modulos\RI;


use App\Modulos\DSPACE\v62\Item as ItemBase;

class Item extends ItemBase
{
    protected  $connection='pgsql_ri';

    public function getDC(){
        $collection=$this->metadatavalue;

        $collection_clean=collect();
        foreach($collection as $mdv){
            $mdv->dc_label=$mdv->metadatafieldregistry->element;
            $mdv->dc_value=$mdv->text_value;
        }

        return $collection;
    }

    public function metadatavalue(){
        return $this->hasMany(MetadataValue::class,'dspace_object_id','uuid');
    }


    public function scopeFindHandle($query,$handle){
        $query->conHandle();
        $query->select('item.*');
        $query->where('handle',$handle);
    }

}
