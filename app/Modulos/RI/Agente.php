<?php

namespace App\Modulos\RI;


class Agente extends \App\Modulos\DSPACE\v62\Agente
{
    //
    protected $table="agentes_ri";

    public function eperson(){
        return $this->hasOne(EPerson::class,'uuid','uuid')->addSelect('email')->conNombres()->conApellidos();
    }
}
