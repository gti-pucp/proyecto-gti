<?php


namespace App\Modulos\RI;


use App\Modulos\DSPACE\v62\HarvestedCollection as HarvestedCollectionBase;

class HarvestedCollection extends HarvestedCollectionBase
{
    protected  $connection='pgsql_ri';
}
