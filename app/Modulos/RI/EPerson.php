<?php


namespace App\Modulos\RI;
use App\Modulos\DSPACE\v62\EPerson as EPersonBase;

class EPerson extends EPersonBase
{
    protected  $connection='pgsql_ri';

    public  function codigoNombres(){
        return 134;
    }

    public  function codigoApellidos(){
        return 135;
    }
}
