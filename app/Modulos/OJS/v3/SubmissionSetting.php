<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubmissionSetting extends Model
{
    //
    protected $connection="mysql_ojs_v3";

    protected $primary_key = null;
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable=[
        'submission_id',
        'locale',
        'setting_name',
        'setting_value',
        'setting_type'
    ];

    public  function scopeFiltrarLicenciasRegistradas($query)
    {
        $query->addSelect("setting_value");
        $query->addSelect(DB::raw("count(submission_id) as total"));
        $query->where('setting_name','licenseURL')->where("setting_value","<>","");
        $query->groupBy("setting_value");

        return $query;
    }

    public function scopePrepararQuery($query,$submission_id,$setting_name){
        $query->where('submission_id',$submission_id)
            ->where('locale','')
            ->where('setting_type','string')
            ->where('setting_name',$setting_name);
        return $query;
    }

    public function submission(){
        return $this->belongsTo(Submission::class,'submission_id','submission_id');
    }
}
