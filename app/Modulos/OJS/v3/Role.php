<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    public static $ROLE_ID_MANAGER=16;
    public static $ROLE_ID_SITE_ADMIN=1;
    public static $ROLE_ID_SUB_EDITOR=17;
    public static $ROLE_ID_AUTHOR=65536;
    public static $ROLE_ID_REVIEWER=4096;
    public static $ROLE_ID_ASSISTANT=4097;
    public static $ROLE_ID_READER=1048576;
    public static $ROLE_ID_SUBSCRIPTION_MANAGER=2097152;

    protected $connection="mysql_ojs_v3";
    protected $primaryKey="role_id";
}
