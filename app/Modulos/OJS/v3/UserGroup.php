<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    //
    protected $connection="mysql_ojs_v3";
    protected $primaryKey="user_group_id";

    public function scopeFromJournal($query,$journal_id){
        if($journal_id!=""){
            $query->where('context_id',$journal_id);
        }else{
            $query->where('context_id',0);
        }
        return $query;
    }

    public function getName(){
        //        pub-id::publisher-id
        return $this->hasMany(UserGroupSetting::class,'user_group_id','user_group_id')
            ->where('setting_name','name')->whereIn("locale",['es_ES','en_US'])->orderBy('locale','desc')->first();



    }
}
