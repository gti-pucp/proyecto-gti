<?php

namespace App\Modulos\OJS\v3;

use App\Modulos\OJS\v2\Journal as JournalBase;

class Journal extends JournalBase
{
    //
    protected $connection="mysql_ojs_v3";

    protected $tabla_metricas="_metricas_v3_";

    const VISITAS=1048585;
    const DESCARGAS=515;

    public function issues(){
        return $this->hasMany(Issue::class,'journal_id','journal_id');
    }

    public function scopeFindPath($query,$path){
        if($path!=""){
            $query->where('path',$path);
        }
        return $query;
    }
}
