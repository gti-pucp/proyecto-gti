<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $connection="mysql_ojs_v3";
    protected $primaryKey="user_id";

    public function scopeFromJournalAndGroup($query,$journal_id,$group_id,$grupo_unico=0){
        $query->selectRaw("users.*,count(distinct(uug1.user_group_id)) as total_grupos, group_concat(distinct(uug1.user_group_id)) as grupos");
        $query->join('user_user_groups as uug',function($join) use($group_id){
            $join->on( 'uug.user_id', '=', 'users.user_id');
            if($group_id>0){
                $join->where('uug.user_group_id',"=",$group_id);
            }
        });

        $query->join('user_groups as ug',function($join) use($journal_id){
            $join->on( 'ug.user_group_id','=','uug.user_group_id' );
        });


        $query->leftJoin('user_user_groups as uug1',function($join) use($group_id){
            $join->on( 'uug1.user_id', '=', 'users.user_id');
        });


        $query->join('user_groups as ug1',function($join) use($journal_id){
            $join->on( 'ug1.user_group_id','=','uug1.user_group_id' );
        });

//        $query->leftJoin('user_groups as ug1',function($join) use($journal_id){
//            $join->on( 'ug1.user_group_id','=','uug1.user_group_id' );
//            if($journal_id!=""){
//                $join->where('ug1.context_id',"=",$journal_id);
//            }
//        });

        if($journal_id!="") {
            $query->where('ug.context_id', $journal_id);
            $query->where('ug1.context_id', $journal_id);
        }



        if($grupo_unico==1){
            $query->havingRaw('count(uug.user_group_id)=1');
        }else  if($grupo_unico>1){
            $query->havingRaw('count(uug.user_group_id)>1');
        }


        $query->groupBy("users.user_id");
        $query->orderBy("last_name");

//        echo $query->toSql();

        return $query;
    }

    public function scopeFiltrar($query,$filtro){
        $query->where(function($subquery) use($filtro){
            $subquery->where('username','like','%'.$filtro.'%');
            $subquery->orWhere('email','like','%'.$filtro.'%');
        });

        return $query;
    }

    public function userGroups(){
        return $this->belongsToMany(UserGroup::class,'user_user_groups','user_id','user_group_id');
    }
}
