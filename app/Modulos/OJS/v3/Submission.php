<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    //
    protected $connection="mysql_ojs_v3";
    protected $primaryKey="submission_id";

    public function scopeFiltrarLicencias($query,$filtro="")
    {
        $query->addSelect("submissions.*");
        $query->leftJoin("submission_settings as ss",function($join){
                $join->on('ss.submission_id','=','submissions.submission_id')
                    ->where('ss.setting_name','licenseURL');
        });
        if($filtro==""){
            $query->whereNull('submissions.submission_id');
        }else{
            $query->where('ss.setting_value',$filtro);
        }
        return $query;
    }

    public function getPubId(){
        //        pub-id::publisher-id
        return $this->hasMany(SubmissionSetting::class,'submission_id','submission_id')
            ->where('setting_name','pub-id::doi')->first();
    }

    public function getDOISuffix(){
        //        pub-id::publisher-id
        return $this->hasMany(SubmissionSetting::class,'submission_id','submission_id')
            ->where('setting_name','doiSuffix')->first();
    }

    public function journal(){
        return $this->belongsTo(Journal::class,'context_id','journal_id');
    }
}
