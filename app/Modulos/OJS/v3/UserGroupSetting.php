<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class UserGroupSetting extends Model
{
    //
    protected $connection="mysql_ojs_v3";
    protected $primaryKey="user_group_id";

}
