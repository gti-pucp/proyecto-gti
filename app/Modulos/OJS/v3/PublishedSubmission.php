<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class PublishedSubmission extends Model
{
    //
    protected $connection="mysql_ojs_v3";

    public function submission(){
        return $this->hasOne(Submission::class,'submission_id','submission_id');
    }

    public function scopeWithTitle($query){
        $sub_sql="(SELECT submission_id, GROUP_CONCAT(ss.setting_value) AS titulo FROM submission_settings AS ss
	WHERE ss.setting_name='title' AND ss.setting_value!=''
	GROUP BY ss.submission_id) as ss1";


        $query->addSelect("ss1.titulo");
        $query->join(\DB::raw($sub_sql),"ss1.submission_id","=","published_submissions.submission_id");

        return $query;
    }

    public function scopeWithAbstract($query){
        $sub_sql="(SELECT submission_id, GROUP_CONCAT(ss.setting_value) AS resumen FROM submission_settings AS ss
	WHERE ss.setting_name='abstract' AND ss.setting_value!=''
	GROUP BY ss.submission_id) as ss2";


        $query->addSelect("ss2.resumen");
        $query->join(\DB::raw($sub_sql),"ss2.submission_id","=","published_submissions.submission_id");

        return $query;
    }

    public function scopeWithAuthors($query){
        $sub_sql="(select submission_id, concat(last_name,' ',first_name) as autor, group_concat(aus.setting_value) as afiliacion
	from authors as a
	left join author_settings as aus on aus.author_id=a.author_id and aus.setting_name='affiliation' and aus.setting_value!=''
	group by a.author_id) as a";
        $query->addSelect("a.autor");
        $query->addSelect("a.afiliacion");
        $query->join(\DB::raw($sub_sql),"a.submission_id","=","published_submissions.submission_id");

        return $query;
    }

    public function scopeWithKeywords($query){
        return $query;
    }

    public function scopeWithDoiSuffix($query){
        $query->addSelect("submission_settings.setting_value as doi_suffix");
        $query->join("submission_settings", function($join){
            $join->on("published_submissions.submission_id","=","submission_settings.submission_id");
            $join->where("submission_settings.setting_name","doiSuffix");

        });
        return $query;
    }

    public function scopeWithPubId($query){
        $query->addSelect("submission_settings.setting_value as pub_id_doi");
        $query->join("submission_settings", function($join){
            $join->on("published_submissions.submission_id","=","submission_settings.submission_id");
            $join->where("submission_settings.setting_name","pub-id::doi");
        });
        return $query;
    }
}
