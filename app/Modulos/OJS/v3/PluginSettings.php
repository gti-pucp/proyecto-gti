<?php

namespace App\Modulos\OJS\v3;

use Illuminate\Database\Eloquent\Model;

class PluginSettings extends Model
{
    //
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = null;

    protected $connection="mysql_ojs";
    protected $table="plugin_settings";
}
