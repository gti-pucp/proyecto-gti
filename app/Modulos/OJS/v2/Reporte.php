<?php

namespace App\Modulos\OJS\v2;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    //
    protected $connection="mysql";
    protected $table="ojs_reportes";
}
