<?php

namespace App\Modulos\OJS\v2;

use Illuminate\Database\Eloquent\Model;

class JournalSetting extends Model
{
    //
    protected $connection="mysql_ojs";
    protected $table="journal_settings";

    public function journal(){
        return $this->belongsTo(Journal::class,"journal_id","journal_id");
    }
}
