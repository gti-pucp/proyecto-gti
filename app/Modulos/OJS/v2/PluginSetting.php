<?php

namespace App\Modulos\OJS\v2;

use Illuminate\Database\Eloquent\Model;

class PluginSetting extends Model
{
    //
    protected $connection="mysql_ojs";
    protected $table="plugin_settings";

    public function journal(){
        return $this->belongsTo(Journal::class,'journal_id','context_id');
    }
}
