<?php

namespace App\Modulos\OJS\v2;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Journal extends Model
{
    //
    protected $connection="mysql_ojs_v2";
    protected $table="journals";
    protected $tabla_metricas="_metricas_v2_";


    const VISITAS=257;
    const DESCARGAS=260;

    public function journalSettings(){
        return $this->hasMany(JournalSetting::class,"journal_id","journal_id");
    }

    public function pluginSettings(){
        return $this->hasMany(PluginSetting::class,'context_id','journal_id');
    }

    public function pluginSetting($plugin){
        return $this->pluginSettings()->where('plugin_name','=',$plugin);
    }

    public function scopeGenerarTablaReporte($query,$anio,$assoc_type){
        $id_reporte= Reporte::create()->id;
        self::generarTablaMetricasResumida($this->connection,$this->tabla_metricas,$id_reporte,$assoc_type);

        return $query;
    }

    public static function generarTablaMetricasResumida($conexion,$tabla_metricas,$id_reporte,$assoc_type){
        $bd_destino=\DB::connection('mysql')->getDatabaseName();
        $bd_origen=\DB::connection($conexion)->getDatabaseName();

        $tabla_temporal=$tabla_metricas.$assoc_type;

        $create_sql="create table if not exists $bd_destino.$tabla_temporal SELECT $id_reporte as `id_reporte`,context_id, `month`,SUM(metric) AS 'metric' FROM $bd_origen.metrics m WHERE assoc_type='$assoc_type'  AND metric_type='ojs::counter' GROUP BY context_id, `month`;" ;
        DB::connection('mysql')->unprepared( DB::raw($create_sql) );

        $rs=DB::connection('mysql')->table($tabla_temporal)->where('id_reporte',$id_reporte)->addSelect(DB::raw('count(id_reporte) as conteo'))->first()->conteo;
        if($rs==0){
            $insert_sql="insert into $bd_destino.$tabla_temporal SELECT $id_reporte as `id_reporte`,context_id, `month`,SUM(metric) AS 'metric' FROM $bd_origen.metrics m WHERE assoc_type='$assoc_type'  AND metric_type='ojs::counter' GROUP BY context_id, `month`;" ;
            DB::connection('mysql')->unprepared( DB::raw($insert_sql) );
        }

        return $bd_destino.".".$tabla_temporal;
    }

    public function scopePrepararReporte($query,$anio, $id_reporte,$assoc_type){
        $tabla_temporal=self::generarTablaMetricasResumida($this->connection,$this->tabla_metricas,$id_reporte,$assoc_type);

        $query->select('journals.*');
        $query->cargarMetricasMensuales($anio,"01",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"02",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"03",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"04",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"05",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"06",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"07",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"08",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"09",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"10",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"11",$tabla_temporal,$id_reporte);
        $query->cargarMetricasMensuales($anio,"12",$tabla_temporal,$id_reporte);
        $query->cargarMetricasAnuales($anio,$tabla_temporal,$id_reporte);

//        $query->cargarMetricas($anio,$assoc_type);
        //$query->where('enabled',1);

        return $query;
    }

    public function scopeCargarMetricas($query,$year,$assoc_type){
        /*
         SELECT context_id,
	SUM(m_01) AS metric_01,SUM(m_02) AS metric_02,SUM(m_03) AS metric_03,
	SUM(m_04) AS metric_04,SUM(m_05) AS metric_05,SUM(m_06) AS metric_06,
	SUM(m_07) AS metric_07,SUM(m_08) AS metric_08,SUM(m_09) AS metric_09,
	SUM(m_10) AS metric_10,SUM(m_11) AS metric_11,SUM(m_12) AS metric_12,
	SUM(total) AS total
FROM (
	SELECT context_id,
		CASE WHEN MONTH='201901' THEN metric ELSE NULL END AS m_01,
		CASE WHEN MONTH='201902' THEN metric ELSE NULL END AS m_02,
		CASE WHEN MONTH='201903' THEN metric ELSE NULL END AS m_03,
		CASE WHEN MONTH='201904' THEN metric ELSE NULL END AS m_04,
		CASE WHEN MONTH='201905' THEN metric ELSE NULL END AS m_05,
		CASE WHEN MONTH='201906' THEN metric ELSE NULL END AS m_06,
		CASE WHEN MONTH='201907' THEN metric ELSE NULL END AS m_07,
		CASE WHEN MONTH='201908' THEN metric ELSE NULL END AS m_08,
		CASE WHEN MONTH='201909' THEN metric ELSE NULL END AS m_09,
		CASE WHEN MONTH='201910' THEN metric ELSE NULL END AS m_10,
		CASE WHEN MONTH='201911' THEN metric ELSE NULL END AS m_11,
		CASE WHEN MONTH='201912' THEN metric ELSE NULL END AS m_12,
		metric AS total
	FROM (
		SELECT context_id, MONTH, metric
		FROM metrics
		WHERE assoc_type=256 AND MONTH LIKE '2019%'
	) AS tmp_metrics
	GROUP BY context_id
) AS tmp_conteo
GROUP BY context_id
         * */
        $tabla="metric_count";
        $field_esquema=collect();
        $field_esquema->push('context_id');

        $field_conteo=collect();
        $field_conteo->push('context_id');

        $fecha=Carbon::createFromFormat("Y",$year)->startOfYear();
        while ($fecha->format("Y")==$year){
            $periodo=$fecha->format("Ym");
            $campo_esquema="m_".$fecha->format("m");
            $campo_conteo="metric_".$fecha->format("m");

            $field_esquema->push("CASE WHEN MONTH='".$periodo."' THEN metric ELSE NULL END AS ".$campo_esquema);
            $field_conteo->push("SUM(".$campo_esquema.") AS ".$campo_conteo);

            $query->addSelect($campo_conteo);
            $fecha->addMonth()->startOfMonth()  ;
        }

        $field_esquema->push('metric as m_total');
        $field_conteo->push('SUM(m_total) AS metric_total');
        $query->addSelect("metric_total");

        $fields_esquema=$field_esquema->implode(',');
        $fields_conteo=$field_conteo->implode(',');

        $tabla_metrics="
            SELECT context_id, month, metric FROM metrics
            WHERE month LIKE '".$year."%' and metric_type='ojs::counter' AND assoc_type=".$assoc_type." 
        ";


        $tabla_esquema="
            SELECT ".$fields_esquema."
            FROM (
                ".$tabla_metrics."
            ) AS tmp_metrics
        ";

        $tabla_conteo=DB::raw("
            (SELECT ".$fields_conteo."
            FROM (
                ".$tabla_esquema."
            ) AS tmp_conteo
            GROUP BY context_id) AS ".$tabla."
        ");

//        dd($tabla_metrics,$tabla_esquema,$tabla_conteo->getValue());

        $query->leftjoin($tabla_conteo, 'journals.journal_id', '=', $tabla.'.context_id');
        return $query;
    }

    public function scopeCargarMetricasMensuales($query,$anio,$mes,$tabla_temporal,$id_reporte){
        $bd_destino=\DB::connection('mysql')->getDatabaseName();

        $tabla="$bd_destino._m_$mes";

        $sql_metricas_mensaules="CREATE TEMPORARY TABLE ".$tabla." SELECT SUM(metric) AS 'metric', context_id FROM $tabla_temporal m WHERE `month`='".$anio.$mes."' and id_reporte='".$id_reporte."' group by context_id;";
        DB::connection($this->connection)->unprepared( DB::raw( $sql_metricas_mensaules ) );

        $query->addSelect(DB::raw($tabla.".metric as `metric_".$mes."`"));

        $query->leftJoin($tabla, function($join) use($tabla,$anio,$mes)
        {
            $join->on('journals.journal_id', '=', $tabla.'.context_id');
        });

        return $query;
    }

    public function scopeCargarMetricasAnuales($query,$anio,$tabla_temporal,$id_reporte){
        $bd_destino=\DB::connection('mysql')->getDatabaseName();
        $tabla="$bd_destino._m_total";

        $sql_metricas_totales="CREATE TEMPORARY TABLE ".$tabla." SELECT SUM(metric) AS 'metric', context_id FROM $tabla_temporal m WHERE `month` like '".$anio."%' and id_reporte='".$id_reporte."' group by context_id;";
        DB::connection($this->connection)->unprepared( DB::raw( $sql_metricas_totales ) );

        $query->addSelect(DB::raw($tabla.".metric as `metric_total`"));

        $query->leftjoin($tabla, function($join) use($tabla,$anio)
        {
            $join->on('journals.journal_id', '=', $tabla.'.context_id');
        });

        return $query;
    }
}
