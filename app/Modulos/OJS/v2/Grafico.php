<?php
/**
 * Created by PhpStorm.
 * User: agastelo
 * Date: 04/09/2018
 * Time: 09:12
 */

namespace App\Modulos\OJS\v2;


use Carbon\Carbon;

class Grafico
{
    private $journals;

    public function __construct($journals,$año)
    {
        $this->journals=$journals;
    }

    public function getJournals(){
        return $this->journals;
    }

    public function getPeriodos(){
        $c=Carbon::now()->startOfYear()->startOfMonth()->startOfDay()->startOfHour()->startOfMinute();
        $limite=$c->year+1;

        $periodos=collect();
        while($c->year<$limite){

            $periodos->push($c->copy());
            $c->addMonths(1);
        }

        return $periodos;
    }

    public function getLineas(){
        $data=collect();
        foreach ($this->journals as $journal) {
            $puntos = collect();

            $puntos->push([1, $journal->metrics_01]);
            $puntos->push([2, $journal->metrics_02]);
            $puntos->push([3, $journal->metrics_03]);
        }

        return $data;
    }


    public function getLabels(){
        return "'".$this->getPeriodos()->map(function($item){
            return $item->format('M');
        })->implode("','")."'";
    }

    public function getDatasets(){
        /*@foreach($grafico->getJournals() as $id=>$journal)
            @if($id>0)
                ,
            @endif
            {
                label: '{{ $journal->path }}',
                data:[
                    {{ $journal->metric_01 }},
                    {{ $journal->metric_02 }},
                    {{ $journal->metric_03 }},
                    {{ $journal->metric_04 }},
                    {{ $journal->metric_05 }},
                    {{ $journal->metric_06 }},
                    {{ $journal->metric_07 }},
                    {{ $journal->metric_08 }},
                    {{ $journal->metric_09 }},
                    {{ $journal->metric_10 }},
                    {{ $journal->metric_11 }},
                    {{ $journal->metric_12 }}
                ]
            }
        @endforeach*/

        $bloque=collect();

//        $color = sprintf("#%02x%02x%02x", 13, 0, 255);
        $maximo_lineas=10;
        $incremento=ceil(255/$maximo_lineas);
        foreach($this->journals as $id=>$journal){
            if($id>$maximo_lineas){
                continue;
            }


            $bloque->push([
                'label'=>$journal->path,
                'data'=>[
                    (int)$journal->metric_01,
                    (int)$journal->metric_02,
                    (int)$journal->metric_03,
                    (int)$journal->metric_04,
                    (int)$journal->metric_05,
                    (int)$journal->metric_06,
                    (int)$journal->metric_07,
                    (int)$journal->metric_08,
                    (int)$journal->metric_09,
                    (int)$journal->metric_10,
                    (int)$journal->metric_11,
                    (int)$journal->metric_12,
                ],
                'fill'=>false,
                'lineTension'=>0,

//                'borderColor'=>fillPattern,
//                'backgroundColor'=>fillPattern,
            ]);
        }
        return $bloque->toJSON();
    }
}
