<?php


namespace App\Modulos\Tesis;


use App\Modulos\DSPACE\v62\Item as ItemBase;

class Item extends ItemBase
{
    protected  $connection='pgsql_tesis';

    protected function getDaysMin(){
        return 5;
    }
}
