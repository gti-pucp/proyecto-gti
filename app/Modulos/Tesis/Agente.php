<?php

namespace App\Modulos\Tesis;

use Illuminate\Database\Eloquent\Model;

class Agente extends \App\Modulos\DSPACE\v62\Agente
{
    //
    protected $table="agentes_tesis";

    public function eperson(){
        return $this->hasOne(EPerson::class,'uuid','uuid')->addSelect('email')->conNombres()->conApellidos();
    }
}
