<?php

namespace App\Modulos\Monitoreo;

use App\Mail\NotificarServidorCaido;
use App\Mail\NotificarServidorLimiteEspacio;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\Environment\Console;

class Servidor extends Model
{
    //
    const OFFLINE=0;
    const ONLINE=1;

    const ENCONTRADO = 200;
    const NO_ENCONTRADO = 404;
    const REDIRECCION_PERMANENTE=302;
    const REDIRECCION_TEMPORAL=303;

    protected $table="servidores";
    protected $dates=['fecha_notificacion','visto_online'];

    public function bitacora(){
        return $this->hasMany(Bitacora::class,'id_servidor','id');
    }

    public function scopeFiltrar($query,$valor){
        if(isset($valor)){
            $query->where('url','like','%'.$valor.'%');
        }

        return $query;
    }

    public function scopeOrdenar($query,$orden){
        return $query;
    }

    private function comprobacionSocket(){
        $socket=new Socket();
    }

    private function comprobacionCurl(){
        $httpcode="";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);

        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_HEADER,true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);

        // SSL important
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output=curl_exec($ch);
        if($output){
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        }

        curl_close($ch);
        return $httpcode;
    }

    public function isOnline(){
        $httpcode=$this->comprobacionCurl();

        switch($httpcode){
            case self::ENCONTRADO:
                $status=self::ONLINE;
                break;
            case self::NO_ENCONTRADO:
            default:
                $status=self::OFFLINE;
        }
        return $status;
    }


    public function procesarBitacora($estado_servidor){
        //SE REGISTRA EL ESTADO DEL SERVIDOR
        $bitacora_actual=new Bitacora(['estado'=>$estado_servidor]);
        $this->bitacora()->save($bitacora_actual);

        $debeNotificar=false;
        if($bitacora_actual->estado==self::ONLINE){
            $this->fecha_online=$bitacora_actual->created_at;
            $this->fecha_notificacion=null;
            $this->save();
        }elseif($bitacora_actual->estado==self::OFFLINE){
            if($this->fecha_actual==null) {
                $this->fecha_notificacion = $bitacora_actual->created_at;
                $this->save();
                $debeNotificar = true;
            }elseif($bitacora_actual->created_at->diffInMinutes($this->fecha_notificacion)>1
                && $bitacora_actual->created_at->diffInMinutes($this->fecha_notificacion)<60){
                $debeNotificar=true;
            }elseif($bitacora_actual->created_at->diffInMinutes($this->fecha_notificacion)%60==0){
                $debeNotificar=true;
            }
        }

        $this->notificar_caida=$debeNotificar;
    }

    public function procesarEspacio($espacio_total,$espacio_ocupado){

        $this->espacio_total=$espacio_total;
        $this->espacio_ocupado=$espacio_ocupado;


        $this->porcentaje_usado=number_format($espacio_ocupado*100/$espacio_total,"2");

        $this->notificar_limite_espacio=$this->limite_espacio<$this->porcentaje_usado;
    }

    public function getEspacioOcupado(){
        return $this->convertirFormatoBytes($this->espacio_ocupado);
    }

    public function getEspacioTotal(){
        return $this->convertirFormatoBytes($this->espacio_total);
    }

    public function convertirFormatoBytes($valor){
        $kilos=1024;
        $megas=1024*$kilos;
        $gigas=1024*$megas;
        $teras=1024*$gigas;

        $mostrar_decimales=true;


        if(floor($valor/$teras)>0){
            $valorFormateado=ceil($valor/$teras);
            $mostrar_decimales=$valor%$teras>0?2:0;
            $sufijo="TB";
        }else if(floor($valor/$gigas)>0){
            $valorFormateado=$valor/$gigas;
            $mostrar_decimales=$valor%$gigas>0?2:0;
            $sufijo="GB";
        }else if(floor($valor/$megas)>0){
            $valorFormateado=$valor/$megas;
            $mostrar_decimales=$valor%$megas>0?2:0;
            $sufijo="MB";
        }elseif(floor($valor/$kilos)>0){
            $valorFormateado=$valor/$kilos;
            $mostrar_decimales=$valor%$kilos>0?2:0;
            $sufijo="KB";
        }else{
            $valorFormateado=$valor;
            $mostrar_decimales=false;
            $sufijo="B";
        }

        $valorFormateado=number_format($valorFormateado,$mostrar_decimales?2:0,",","")." ".$sufijo;
        return $valorFormateado;
    }

    public function getCorreosAdicionalesCC(){
        $correos_adicionales=explode(",",$this->correos_adicionales);

        $correos_adicionales_depurados=[];
        foreach($correos_adicionales as $email){
            $validator=Validator::make(['email'=>$email],['email','email']);

            if(!$validator->fails() && $email!=""){
                $correos_adicionales_depurados[]=$email;
            }
        }
        return $correos_adicionales_depurados;
    }
}
