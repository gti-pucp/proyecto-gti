<?php

namespace App\Modulos\Monitoreo;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    //
    protected $table='servidores_bitacora';
    protected $fillable=['estado'];

    public function servidor(){
        return $this->belongsTo(Servidor::class,'id_servidor','id');
    }
}
