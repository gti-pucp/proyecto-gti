<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class Bundle extends Model
{
    //
    protected $table='bundle';
    protected $primaryKey='uuid';
    public $incrementing = false;

    public function items(){
        return $this->belongsToMany(Item::class,'item2bundle','bundle_id','item_id');
    }

    public function bitstreams(){
        return $this->belongsToMany(Bitstream::class,'bundle2bitstream','bundle_id','bitstream_id');
    }

    public function handle(){
        return $this->hasOne(Handle::class,'resource_id','uuid');
    }

    public function getValores(){
        return $this->hasMany(MetadataValue::class,'dspace_object_id','uuid');
    }
}
