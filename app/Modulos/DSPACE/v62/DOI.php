<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class DOI extends Model
{
    //
    protected $table='handle';
    protected $primaryKey='uuid';
    public $incrementing = false;

    public function item(){
        return $this->belongsTo(Item::class,'uuid','resource_id');
    }
}
