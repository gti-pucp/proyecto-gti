<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class Bitstream extends Model
{
    //
    protected $table='bitstream';
    protected $primaryKey='uuid';
    public $incrementing = false;

    //
    private $prefix_bu2bi="bu2bi";
    private $prefix_it2bu="it2bu";
    private $prefix_c2i="c2i";

    public function bundles(){
        return $this->belongsToMany(Bundle::class,'bundle2bitstream','bitstream_id','bundle_id');
    }

    public function bitstreamFormatRegistry(){
        return $this->hasOne(BitstreamFormatRegistry::class,'bitstream_format_id','bitstream_format_id');
    }

    public function getCollection(){
        return $this->belongsTo(Collection::class,'collection_id','uuid')->first();
    }

    public function getItem(){
        return $this->belongsTo(Item::class,'item_id','uuid')->first();
    }

    public function getValores(){
        return $this->hasMany(MetadataValue::class,'dspace_object_id','uuid');
    }

    public function getTitle(){
        $valores=$this->getValores()->where('metadata_field_id',64)->limit(1);
        if($valores->count()>2){
            $titulo=$valores->implode("text_value",", ");
        }else{
            $titulo=$valores->first()->text_value;
        }
        return $titulo;
    }

    public function scopeJoinBundle2Bitstream($query){
        $query->leftJoin('bundle2bitstream as '.$this->prefix_bu2bi,'bu2bi.bitstream_id','=',$this->table.'.uuid');
        return $query;
    }


    public function scopeJoinItem2Bundle($query){
        $query->leftJoin('item2bundle as '.$this->prefix_it2bu,$this->prefix_it2bu.'.bundle_id','=',$this->prefix_bu2bi.'.bundle_id');
        $query->whereNotNull($this->prefix_it2bu.'.item_id');
        return $query;
    }

    public function scopeConTitulo($query){
        $prefix="mdv_ct";
        $query->addSelect($prefix.'.text_value as titulo');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.uuid');
            $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }

    public function scopeConHandleDeItem($query){
        $prefix='hi';
        $query->addSelect($prefix.'.handle as handle_item');
        $query->leftJoin('handle as '.$prefix, $prefix.'.resource_id','=',$this->prefix_it2bu.'.item_id');
        $query->whereNotNull($prefix.'.handle');
        return $query;
    }

    public function scopeConTituloDeItem($query){
        $prefix="mdv_cti";
        $query->addSelect($prefix.'.text_value as titulo_item');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->prefix_it2bu.'.item_id');
            $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }


    public function scopeConHandleDeColeccion($query){
        $prefix_handle="h_chc";

        $query->addSelect($prefix_handle.'.handle as handle_coleccion');
        $query->leftJoin('handle as '.$prefix_handle,$prefix_handle.'.resource_id','=',$this->prefix_c2i.'.collection_id');
        $query->whereNotNull($prefix_handle.'.handle');
        return $query;
    }

    public function scopeConTituloDeColeccion($query){
        $prefix="mdv_ctc";
        $query->addSelect($prefix.'.text_value as titulo_coleccion');

        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->prefix_c2i.'.collection_id');
            $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }

    public function scopeJoinCollection2Item($query){
        $query->leftJoin('collection2item as '.$this->prefix_c2i,$this->prefix_c2i.'.item_id','=',$this->prefix_it2bu.'.item_id');
        $query->whereNotNull($this->prefix_c2i.'.collection_id');
        return $query;
    }

    public function scopeObtenerPesados($query,$minimo,$principal){
        $query->addSelect($this->table.'.*');

        $query->joinBundle2Bitstream();
        $query->joinItem2Bundle();
        $query->joinCollection2Item();

        $query->conTitulo();
        $query->conHandleDeItem();
        $query->conTituloDeItem();

        $query->conHandleDeColeccion();
        $query->conTituloDeColeccion();

        if(isset($principal) && $principal==1){
            $query->where($this->table.'.sequence_id','=',1);
        }else{
            $query->where($this->table.'.sequence_id','>',1);
        }

        //FILTROS
        $query->where('size_bytes','>',$minimo*1024*1024);
        $query->where('deleted',false);
        $query->orderBy('size_bytes','desc');

        return $query;
    }

    public function scopeObtenerSinThumbnail($query){
        $query->addSelect($this->table.'.*');

        $query->joinBundle2Bitstream();
        $query->joinItem2Bundle();
        $query->joinCollection2Item();

        $query->conTitulo();
        $query->conHandleDeItem();
        $query->conTituloDeItem();

        $query->conHandleDeColeccion();
        $query->conTituloDeColeccion();
        $query->noGeneradosPorthumbnail();

        $query->where('deleted',false);

        dd($query->toSql(),$query->getBindings());
//        $query->groupBy('collection_id');
        return $query;
    }

    public function scopeNoGeneradosPorThumbnail($query){
        $prefix="mdv_c";

        $plugins=[
            "org.dspace.app.mediafilter.ImageMagickPdfThumbnailFilter",
            "org.dspace.app.mediafilter.JPEGFilter",
            "org.dspace.app.mediafilter.ImageMagickImageThumbnailFilter"
        ];


        $query->join('metadatavalue as '.$prefix,function($join) use($prefix,$plugins){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.uuid');
            $join->where(function($subquery) use($prefix,$plugins){
                foreach($plugins as $plugin){
                    $subquery->orWhere($prefix.".text_value","like",'%'.$plugin.'%');
                }
            });
        });

        return $query;
    }

}
