<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    //
    protected $connection="mysql";

    public function agentes(){
        return $this->belongsToMany(EPerson::class,'tesis_reporte_eperson','reporte_id','eperson_id');
    }
}
