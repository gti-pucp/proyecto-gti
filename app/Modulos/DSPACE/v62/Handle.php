<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class Handle extends Model
{
    //
    protected $table='handle';
    protected $primaryKey='uuid';
    public $incrementing = false;

    public function collection(){
        return $this->belongsTo(Collection::class,'uuid','resource_id');
    }
}
