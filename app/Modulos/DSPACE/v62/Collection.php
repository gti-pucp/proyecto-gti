<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    //
    protected $table='collection';
    protected $primaryKey='uuid';
    public $incrementing = false;

    public function getValores(){
        return $this->hasMany(MetadataValue::class,'dspace_object_id','uuid');
    }

    public function getTitle(){
        $valores=$this->getValores()->where('metadata_field_id',64);
        if($valores->count()>2){
            $titulo=$valores->implode("text_value",", ");
        }else{
            $titulo=$valores->first()->text_value;
        }
        return $titulo;
    }

    public function handle(){
        return $this->hasOne(Handle::class,'resource_id','uuid');
    }

    public function getHandle(){
        return $this->handle()->first()->handle;
    }
}
