<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class MetadataFieldRegistry extends Model
{
    //
    protected $table='metadatafieldregistry';
    protected $primaryKey='metadata_field_id';

    public function metadataSchemaRegistry(){
        return $this->belongsTo(MetadataSchemaRegistry::class,'metadata_schema_id','metadata_schema_id');
    }
}
