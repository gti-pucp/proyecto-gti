<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class MetadataValue extends Model
{
    //
    protected $table='metadatavalue';
    protected $primaryKey='metadata_value_id';

    public function metadataFieldRegistry(){
        return $this->belongsTo(MetadataFieldRegistry::class,'metadata_field_id','metadata_field_id');
    }

    public function scopeFiltrarEnvios($query){
        $field="";
        $table="";

//        $query->leftjoin('','','','');
        return $query;
    }

    public function scopeFiltrarURLDuplicadas($query){
        $query->select($this->table.".dspace_object_id");
        $query->filtrarUrlDuplicada();
//        $query->conHandleDeColeccion();
        $query->conHandleDeItem();
//        $query->conTituloDeColeccion();
        $query->conTituloDeItem();
        $query->orderBy($this->table.".text_value");

       // dd( $query->toSql());
        return $query;
    }

    public function scopeFiltrarUrlDuplicada($query){
        $query->whereRaw($this->table.'.text_value in (
         -----------------------------------select text_value from metadatavalue
        where metadata_field_id=25
        group by text_value having count(dspace_object_id)>1
        )');
        return $query;
    }

    public function scopeConTituloDeItem($query){
        $prefix="mdv_ch";
        $query->addSelect($prefix.'.text_value as titulo_item');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.dspace_object_id');
            $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }

    public function scopeConHandleDeItem($query){
        $prefix='hi';
        $query->addSelect($prefix.'.handle as handle_item');
        $query->leftJoin('handle as '.$prefix, $prefix.'.resource_id','=',$this->table.'.dspace_object_id');
        $query->whereNotNull($prefix.'.handle');
        return $query;
    }
}


