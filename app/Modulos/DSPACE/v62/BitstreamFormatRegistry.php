<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class BitstreamFormatRegistry extends Model
{
    //
    protected $table='bitstreamformatregistry';
    protected $primaryKey='bitstream_format_id';
    public $incrementing = false;

    public function fileExtension(){
        return $this->hasMany(FileExtension::class,'bitstream_format_id','bitstream_format_id');
    }
}
