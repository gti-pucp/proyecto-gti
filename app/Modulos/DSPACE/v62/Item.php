<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    //
    protected $table='item';
    protected $primaryKey='uuid';
    public $incrementing = false;

    //
    private $prefix_c2i="c2i";

    protected function getDaysMin(){
        return 5;
    }

    public function scopeConDOI($query){
        $prefix='di';
        $query->addSelect($prefix.'.doi as doi');
        $query->leftJoin('doi as '.$prefix, $prefix.'.resource_id','=',$this->table.'.uuid');
        $query->whereNotNull($prefix.'.doi');
        return $query;
    }

    public function scopeConHandle($query){
        $prefix='hi';
        $query->addSelect($prefix.'.handle as handle');
        $query->leftJoin('handle as '.$prefix, $prefix.'.resource_id','=',$this->table.'.uuid');
        $query->whereNotNull($prefix.'.handle');
        return $query;
    }

    public function scopeConTitulo($query){
        $prefix="mdv_ct";
        $query->addSelect($prefix.'.text_value as titulo');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
           $join->on($prefix.'.dspace_object_id','=',$this->table.'.uuid');
           $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }

    public function scopeConTituloDeColeccion($query){
        $prefix="mdv_ctc";
        $query->addSelect($prefix.'.text_value as titulo_coleccion');

        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=','c2i.collection_id');
            $join->where($prefix.'.metadata_field_id',64);
        });

        return $query;
    }

    public function scopeConHandleDeColeccion($query){
        $prefix_handle="h_chc";

        $query->addSelect($prefix_handle.'.handle as handle_coleccion');
        $query->leftJoin('handle as '.$prefix_handle,$prefix_handle.'.resource_id','=',$this->prefix_c2i.'.collection_id');
        $query->whereNotNull($prefix_handle.'.handle');
        return $query;
    }


    public function scopeJoinCollection2Item($query){
        $prefix_c2i=$this->prefix_c2i;
        $query->leftJoin('collection2item as '.$prefix_c2i,$prefix_c2i.'.item_id','=',$this->table.'.uuid');
        return $query;
    }

    public function scopePrepararParaThumbnailGenerator($query){
        $query->addSelect($this->table.'.*');

        $query->conHandle();
//        $query->conTitulo();
//
//        $query->joinCollection2Item();
//        $query->conHandleDeColeccion();
//        $query->conTituloDeColeccion();

        //filtros
        $query->noCosechados();
        $query->sinThumbnail();

        $query->conArchivosParaGenerarThumbnail();

        $query->where('in_archive',true);


        return $query;
    }

    public  function scopePrepararSinThumbnailParaGestion($query){
        $query->addSelect($this->table.'.*');



        //filtros
        $query->noCosechados();
        $query->sinThumbnail();

        $query->conArchivosParaGenerarThumbnail();

        $query->conHandle();
        $query->conTitulo();

        $query->joinCollection2Item();
        $query->conHandleDeColeccion();
        $query->conTituloDeColeccion();



        $query->where('in_archive',true);


        return $query;
    }



    public function scopeNoCosechados($query){
//        $query->addSelect('mdvsc.text_value as cosechado');
        $prefix="mdvsc";

        $select_oai=DB::raw("(select dspace_object_id, text_value from metadatavalue
            where text_value like 'Item created via OAI %') as ".$prefix);

        $query->leftJoin($select_oai,$prefix.'.dspace_object_id','=',$this->table.'.uuid');
        $query->whereNull($prefix.'.text_value');

        return $query;
    }

    public function scopeSinThumbnail($query){
        $prefix="_sth";
        $sql=DB::raw('(select i2b'.$prefix.'.item_id, count(i2b'.$prefix.'.item_id) as thumbnail from item2bundle as i2b'.$prefix.'
                    left join metadatavalue as mdv'.$prefix.' on mdv'.$prefix.'.dspace_object_id=i2b'.$prefix.'.bundle_id
                    where mdv'.$prefix.'.text_value like \'THUMBNAIL\'
                    group by i2b'.$prefix.'.item_id) as '.$prefix.'
                ');

        $query->leftJoin($sql,$prefix.'.item_id','=',$this->table.'.uuid');
        $query->whereNull($prefix.'.thumbnail');

        return $query;
    }

    public function scopeConArchivosParaGenerarThumbnail($query){
        $prefix="_pgth";

        $mimetypes=collect(['application/pdf','image/jpg','image/jpeg']);
        $filtro_mimetypes=$mimetypes->map(function($item){
            return "'".$item."'";
        })->implode(",");

        $sql=DB::raw('(
            select it2bu.item_id,count(bi.uuid) as archivos_originales
            from item2bundle as it2bu
            left join bundle as bu on bu.uuid=it2bu.bundle_id
	        left join bundle2bitstream as bu2bi on bu2bi.bundle_id=bu.uuid
            left join bitstream as bi on bu2bi.bitstream_id=bi.uuid and bi.deleted=false
            left join metadatavalue as mdv_bu on mdv_bu.dspace_object_id=it2bu.bundle_id  and mdv_bu.metadata_field_id=64 and mdv_bu.text_value=\'ORIGINAL\'
            left join bitstreamformatregistry as bfr on bfr.bitstream_format_id=bi.bitstream_format_id and bfr.mimetype in ('.$filtro_mimetypes.')
            group by it2bu.item_id
        ) as '.$prefix.'
                ');

        $query->leftJoin($sql,$prefix.'.item_id','=',$this->table.'.uuid');
        $query->where($prefix.'.archivos_originales','>','0');




        return $query;
    }

    public function scopeSinThumbnailBack($query){
        $prefix="_sth";
        $description_1="%org.dspace.app.mediafilter.ImageMagickPdfThumbnailFilter%";
        $description_2="%org.dspace.app.mediafilter.JPEGFilter%";
        $description_3="%org.dspace.app.mediafilter.ImageMagickImageThumbnailFilter%";

        //VERIFICA SI NO TIENE THUMBNAIL GENERADO
        $sql=DB::raw('(select i2b'.$prefix.'.item_id, count(i2b'.$prefix.'.item_id) as thumbnail from item2bundle as i2b'.$prefix.'
                    left join bundle as bu'.$prefix.' on bu'.$prefix.'.uuid=i2b'.$prefix.'.bundle_id
                    left join bundle2bitstream as bu2bi'.$prefix.' on bu2bi'.$prefix.'.bundle_id=bu'.$prefix.'.uuid
                    left join bitstream as bi'.$prefix.' on bi'.$prefix.'.uuid=bu2bi'.$prefix.'.bitstream_id
                    left join metadatavalue as mdv'.$prefix.' on mdv'.$prefix.'.dspace_object_id=bi'.$prefix.'.uuid
                    where mdv'.$prefix.'.text_value like \''.$description_1.'\'
                    or mdv'.$prefix.'.text_value like \''.$description_2.'\'
                    or mdv'.$prefix.'.text_value like \''.$description_3.'\'
                    group by i2b'.$prefix.'.item_id) as '.$prefix.'
                ');
        $sql=DB::raw('(select i2b'.$prefix.'.item_id, count(i2b'.$prefix.'.item_id) as thumbnail from item2bundle as i2b'.$prefix.'
                    left join bundle2bitstream as bu2bi'.$prefix.' on bu2bi'.$prefix.'.bundle_id=i2b'.$prefix.'.bundle_id
                    left join metadatavalue as mdv'.$prefix.' on mdv'.$prefix.'.dspace_object_id=bu2bi'.$prefix.'.bitstream_id
                    where mdv'.$prefix.'.text_value like \''.$description_1.'\'
                    or mdv'.$prefix.'.text_value like \''.$description_2.'\'
                    or mdv'.$prefix.'.text_value like \''.$description_3.'\'
                    group by i2b'.$prefix.'.item_id) as '.$prefix.'
                ');

//        dd($sql);
        $query->leftJoin($sql,$prefix.'.item_id','=','item.uuid');
        $query->whereNull($prefix.'.thumbnail');

        return $query;
    }

    public function scopeSinText($query){
        $prefix="_stx";
//        $metadata_field_id="26";
        $description="%org.dspace.app.mediafilter.PDFFilter%";

        $sql=DB::raw('(select i2b'.$prefix.'.item_id, count(i2b'.$prefix.'.item_id) as text from item2bundle as i2b'.$prefix.'
                    left join bundle as bu'.$prefix.' on bu'.$prefix.'.uuid=i2b'.$prefix.'.bundle_id
                    left join bundle2bitstream as bu2bi'.$prefix.' on bu2bi'.$prefix.'.bundle_id=bu'.$prefix.'.uuid
                    left join bitstream as bi'.$prefix.' on bi'.$prefix.'.uuid=bu2bi'.$prefix.'.bitstream_id
                    left join metadatavalue as mdv'.$prefix.' on mdv'.$prefix.'.dspace_object_id=bi'.$prefix.'.uuid
                    where  mdv'.$prefix.'.text_value like \''.$description.'\'
                    group by i2b'.$prefix.'.item_id) as '.$prefix.'
                ');

        $query->leftJoin($sql,$prefix.'.item_id','=',$this->table.'.uuid');
        $query->whereNull($prefix.'.text');
        return $query;
    }

   /* public function scopeConArchivoParaGenerarThumbnail($query){
        $sufix="_cth";
        //VERIFICA SI NO TIENE THUMBNAIL GENERADO

        $mime_image="image/JPEG";
        $mime_pdf="application/PDF";

        $sql=DB::raw('(select i2b'.$sufix.'.item_id, count(i2b'.$sufix.'.item_id) as imagen from item2bundle as i2b'.$sufix.'
                    left join bundle as bu'.$sufix.' on bu'.$sufix.'.uuid=i2b'.$sufix.'.bundle_id
                    left join bundle2bitstream as bu2bi'.$sufix.' on bu2bi'.$sufix.'.bundle_id=bu'.$sufix.'.uuid
                    left join bitstream as bi'.$sufix.' on bi'.$sufix.'.uuid=bu2bi'.$sufix.'.bitstream_id
                    left join bitstreamformatregistry as bfr'.$sufix.' on bfr'.$sufix.'.bitstream_format_id=bi'.$sufix.'.bitstream_format_id
                    left join metadatavalue as mdv'.$sufix.' on mdv'.$sufix.'.dspace_object_id=bi'.$sufix.'.uuid
                    where bfr'.$sufix.'.mimetype=\''.$mime_image.'\' or bfr'.$sufix.'.mimetype=\''.$mime_pdf.'\'
                    group by i2b'.$sufix.'.item_id) as '.$sufix.'
                ');

        $query->leftJoin($sql,$sufix.'.item_id','=','item.uuid');
        $query->where($sufix.'.imagen','>',0);
        return $query;
    }*/

    public function bundles(){
        return $this->belongsToMany(Bundle::class,'item2bundle','item_id','bundle_id');
    }

    public function scopeFiltrarTitulosDuplicados($query){
        $query->distinct("mdv_ct.text_value");
        $query->joinCollection2Item();

        $query->conHandle();
        $query->conTitulo();
        $query->conTituloDeColeccion();
        $query->conHandleDeColeccion();

        $query->whereRaw(\DB::raw(
            "mdv_ct.text_value in (
select titulo from (
	select mdv.text_value as titulo, count(i.uuid) as conteo from item i left join metadatavalue mdv
	on mdv.dspace_object_id=i.uuid and mdv.metadata_field_id=64
	where mdv.text_value is not null
	group by mdv.text_value order by  mdv.text_value
) as t where t.conteo>1
)"
        ));

//        echo $query->toSql();
//
//        dd($query->getBindings());

        return $query;
    }

    public function scopeFiltrarConPoliciesNulos($query){
        $query->conHandle();
        $query->conTitulo();
        $query->conResourcePolicy();
        return $query;
    }

    public function scopeConResourcePolicy($query){
        $prefix='rp';
        //$query->addSelect($prefix.'.handle as handle');
        $query->leftJoin('resourcepolicy as '.$prefix, $prefix.'.dspace_object','=',$this->table.'.uuid');
        $query->where($prefix.'.resource_type_id',2);
        $query->whereNull($prefix.'.epersongroup_id');

        return $query;
    }
}
