<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class MetadataSchemaRegistry extends Model
{
    //
    protected $table='metadataschemaregistry';

    public function metadataSchemaRegistry(){
        return $this->belongsTo(MetadataSchemaRegistry::class,'metadata_schema_id','metadata_field_id');
    }

}
