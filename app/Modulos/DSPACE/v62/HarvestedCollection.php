<?php

namespace App\Modulos\DSPACE\v62;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class HarvestedCollection extends Model
{
    //
    const STATUS_ERROR = 3;
    const STATUS_COSECHADO = 0;
    protected $table='harvested_collection';
    public $dates=['harvest_start_time'];

    protected $dateFormat = 'Y-m-d H:i:s.uO';

    public function scopeConHandleDeColeccion($query){
        $prefix_handle="h_chc";

        $query->addSelect($prefix_handle.'.handle as handle_coleccion');
        $query->leftJoin('handle as '.$prefix_handle,$prefix_handle.'.resource_id','=',$this->table.'.collection_id');
        $query->whereNotNull($prefix_handle.'.handle');
        return $query;
    }

    public function scopeConTitulo($query){
        $prefix="mdv_ch";
        $query->addSelect($prefix.'.text_value as titulo');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.collection_id');
            $join->where($prefix.'.metadata_field_id',64);
        });



        return $query;
    }


    public function scopePrepararCosechas($query,$fecha_limite){
        $query->addSelect($this->table.".*");

        $query->conHandleDeColeccion();



        $query->soloRepositoriosDesignados();

        $query->whereRaw('date(harvest_start_time) < date(?)',[$fecha_limite]);
        $query->orderBy('harvest_start_time','desc');


        return $query;
    }

    public function scopeSoloRepositoriosDesignados($query){
        $query->where(function($subquery){
            foreach($this->getFiltros() as $filtro){
                $subquery->orWhere($this->table.'.oai_source','like','%'.$filtro.'%');
            }
        });

        return $query;
    }

    private function getFiltros(){
        return ['tesis.pucp.edu.pe','datos.pucp.edu.pe'];
    }

    public function scopeFiltrarCosechas($query,$buscar){
        $query->addSelect($this->table.".*");
        $query->conHandleDeColeccion();
        $query->conTitulo();

        if(isset($buscar)){
            $query->whereRaw("LOWER(mdv_ch.text_value) like CONCAT('%',LOWER(?),'%')",$buscar);
        }

        $query->orderBy('harvest_status','desc');
        $query->orderBy('harvest_start_time','desc');
        $query->orderBy('mdv_ch.text_value','asc');

        return $query;
    }
}
