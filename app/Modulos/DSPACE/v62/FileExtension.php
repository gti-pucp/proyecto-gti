<?php

namespace App\Modulos\DSPACE\v62;

use Illuminate\Database\Eloquent\Model;

class FileExtension extends Model
{
    //
    protected $table='fileextension';
    protected $primaryKey='file_extension_id';

}
