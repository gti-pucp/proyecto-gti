<?php

namespace App\Modulos\DSPACE\v62;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EPerson extends Model
{
    //
    protected $table='eperson';
    protected $primaryKey='uuid';
    public $incrementing = false;



    public  function codigoApellidos()
    {
        return 134;
    }

    public  function codigoNombres()
    {
        return 133;
    }

    public function scopeConNombres($query){
        $prefix="mdv_cn";
        $query->addSelect($prefix.'.text_value as nombres');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.uuid');
            $join->where($prefix.'.metadata_field_id',$this->codigoNombres());
        });

        $query->orderBy($prefix.'.text_value');
        return $query;
    }

    public function scopeConApellidos($query){
        $prefix="mdv_ca";
        $query->addSelect($prefix.'.text_value as apellidos');
        $query->leftJoin('metadatavalue as '.$prefix,function($join) use($prefix){
            $join->on($prefix.'.dspace_object_id','=',$this->table.'.uuid');
            $join->where($prefix.'.metadata_field_id',$this->codigoApellidos());
        });

        $query->orderBy($prefix.'.text_value');

        return $query;
    }

    public function scopeFiltrarPorId($query,$items){
        if(count($items)==0){
            $query->whereNull($this->table.'.uuid');
        }

        foreach($items as $item){
            $query->orwhere(function($subquery) use($item){
                $subquery->orWhere($this->table.'.uuid',$item);
            });
        }
        return $query;
    }

    public function scopePrepararReporteAnual($query,$uuids,$year,$month=null){
        $query->addSelect($this->table.'.*');
        //$query->filtrarPorId($uuids);

        $query->conApellidos();
        $query->conNombres();

        $query->joinConteoDeEnvios($year,$month);


        return $query;
    }

    public static function getPeriodos($year,$month=null){
        $parametros_periodo[]=$year;

        $periodos=collect();
        if(isset($month)){
            $parametros_periodo[]=$month;
            $fecha_seteada=$year."-".$month;
            $numero_caracteres_fecha=9;

            $fecha=Carbon::createFromFormat("Y-m",$fecha_seteada)->startOfMonth()->startOfDay();
            while($fecha->format("Y-m")==$fecha_seteada){
                $filtro_fecha=$fecha->format("Y-m-d");
                $field_doi="doi_m_".$fecha->format('d');
                $field_metric="metric_".$fecha->format('d');

                $periodos->push(compact('filtro_fecha','field_doi','field_metric'));

                $fecha->addDay()->startOfDay();
            }
        }else{
            $fecha_seteada=$year;
            $fecha=Carbon::createFromFormat("Y",$year)->startOfYear()->startOfMonth();
            $numero_caracteres_fecha=7;

            while($fecha->format("Y")==$fecha_seteada){
                $filtro_fecha=$fecha->format("Y-m");
                $field_doi="doi_m_".$fecha->format('m');
                $field_metric="metric_".$fecha->format('m');

                $periodos->push([$filtro_fecha,$field_doi,$field_metric]);
                $fecha->addMonth()->startOfMonth();
            }
        }

        $periodo_global=implode("-",$parametros_periodo);
        return array($periodos,$numero_caracteres_fecha,$periodo_global);
    }

    public function scopeJoinConteoDeEnvios($query,$year,$month=null){

        list($periodos,$numero_caracteres_fecha,$periodo_global)=self::getPeriodos($year,$month);

        //GENERANDO CAMPOS DINAMICOS
        $field_objects=collect();
        $field_objects->push('username');

        $field_counts=collect();
        $field_counts->push('username');

        //AGREGANDO SELECTS PARA CONTEO DE METRICAS SEGUN PERIODO
        foreach ($periodos->toArray() as $periodo){
            list($filtro_fecha, $field_doi, $field_metric)=$periodo;

            $field_objects->push("CASE WHEN fecha='".$filtro_fecha."' THEN do_id ELSE NULL END AS ".$field_doi);
            $field_counts->push("count($field_doi) as ".$field_metric);
            $query->addSelect($field_metric);
        }

        //AGREGANDO TOTALES PARA CONTEO DE METRICAS SEGUN PERIODO
        $field_objects->push("do_id AS total");
        $field_counts->push('count(total) as total');

        $fields_objects=$field_objects->implode(",");
        $fields_counts=$field_counts->implode(",");

        //CAMPOS PARA EXTRACCION DE USUARIO Y FECHAS DE ENVIO
        $field_ex_username="SUBSTRING(text_value FROM strpos(text_value,'(')+1 FOR strpos(text_value,'@')-strpos(text_value,'(')-1)";
        $field_ex_fecha="SUBSTRING(text_value FROM strpos(text_value,' on ')+4 FOR ".$numero_caracteres_fecha.")";

        //CONSULTAR ESTRUCTURA DE USUARIO, FECHA Y OBJETO ENVIADO DE METADATAVALUE
        $sql_tabla_mdv=" SELECT ".$field_ex_username." AS username, ".$field_ex_fecha." AS fecha, dspace_object_id AS do_id
            FROM metadatavalue WHERE text_value LIKE '%Submitted by % ".$periodo_global."-%'
            ORDER BY $field_ex_fecha,$field_ex_username";

        //SEPARAR OBJETOS SEGUN PERIODO ENVIADO POR USUARIO
        $sql_tabla_conteo="SELECT ".$fields_objects." FROM (".$sql_tabla_mdv.")
            AS tabla_base";

        //DEFINIENDO ALIAS DE TABLA JOIN
        $temp_table="mdv_r";

        //CONTAR OBJETOS POR PERIODO DE TIEMPO
        $select_mdv=DB::raw("(SELECT ".$fields_counts." FROM ( $sql_tabla_conteo ) AS tmp GROUP BY username)
            AS ".$temp_table);

        //EXTRAYENDO USUARIO DE CAMPO CORREO
        $field_eperson="SUBSTRING(eperson.email from 0 for strpos(eperson.email,'@'))";

        //ENLAZANDO TABLA Y METRICAS SEGUN USUARIO
        $query->leftJoin($select_mdv, $temp_table.'.username','=',DB::raw($field_eperson));

//        dump($query->toSql());

        $query->whereRaw('total>0');
        $query->orderBy('total','desc');
        return $query;
    }
}
