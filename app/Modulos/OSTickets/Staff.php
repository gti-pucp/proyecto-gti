<?php

namespace App\Modulos\OSTickets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Staff extends Model
{
    //
    protected $connection="mysql_ostickets";
    protected $table="ost_staff";

    public function scopeReporteGeneral($query,$dateStart,$dateEnd){
        /**
        SELECT s.staff_id,username AS agente,
        COUNT(DISTINCT(t.ticket_id)) AS total_tickets,
        COUNT(DISTINCT(IF(t.staff_id=the.staff_id, t.ticket_id,NULL))) AS total_tickets_asignados,
        COUNT(DISTINCT(IF(t.staff_id!=the.staff_id, t.ticket_id,NULL))) AS total_tickets_ajenos,
        COUNT(DISTINCT(the.id)) AS total_mensajes,
        COUNT(DISTINCT(IF(the.type="M",the.id,NULL))) AS mensajes_inicial,
        COUNT(DISTINCT(IF(the.type="N",the.id,NULL))) AS notas,
        COUNT(DISTINCT(IF(the.type="R",the.id,NULL))) AS mensajes,
        COUNT(DISTINCT(IF(t.staff_id=the.staff_id,the.id,NULL))) AS total_mensajes_tickets_asignados,
        COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="M",the.id,NULL))) AS mensajes_inicial_tickets_asignados,
        COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="N",the.id,NULL))) AS notas_tickets_asignados,
        COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="R",the.id,NULL))) AS mensajes_tickets_asignados,
        COUNT(DISTINCT(IF(t.staff_id!=the.staff_id,the.id,NULL))) AS total_mensajes_tickets_ajenos,
        COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="M",the.id,NULL))) AS mensajes_inicial_tickets_ajenos,
        COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="N",the.id,NULL))) AS notas_tickets_ajenos,
        COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="R",the.id,NULL))) AS mensajes_tickets_ajenos,
        ROUND(COUNT(DISTINCT(the.id))/COUNT(DISTINCT(t.ticket_id)),4) AS promedio_mensajes_x_ticket ,
        ROUND(COUNT(DISTINCT(IF(t.staff_id!=the.staff_id, t.ticket_id,NULL)))/COUNT(DISTINCT(t.ticket_id)),2)*100 AS porcentaje_tickets_ajenos_entre_totales
        FROM ost_staff s
        LEFT JOIN ost_thread_entry the ON s.staff_id=the.staff_id
        LEFT JOIN ost_thread th ON th.id=the.thread_id
        LEFT JOIN ost_ticket t ON t.ticket_id=th.object_id
        WHERE DATE(the.created) BETWEEN @DESDE AND @HASTA
        GROUP BY s.staff_id ORDER BY (COUNT(DISTINCT(IF(the.type="M",the.id,NULL)))+COUNT(DISTINCT(IF(the.type="N",the.id,NULL)))+COUNT(DISTINCT(IF(the.type="R",the.id,NULL)))) DESC,username
         */

        $query->addSelect('*');
        $query->addSelect(DB::raw('count(DISTINCT(t.ticket_id)) as suma_tickets' ));
        $query->addSelect(DB::raw('COUNT(DISTINCT(t.ticket_id)) AS total_tickets'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id=the.staff_id, t.ticket_id,NULL))) AS total_tickets_asignados'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id!=the.staff_id, t.ticket_id,NULL))) AS total_tickets_ajenos'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(the.id)) AS total_mensajes'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(the.type="M",the.id,NULL))) AS mensajes_inicial'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(the.type="N",the.id,NULL))) AS notas'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(the.type="R",the.id,NULL))) AS mensajes'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id=the.staff_id,the.id,NULL))) AS total_mensajes_tickets_asignados'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="M",the.id,NULL))) AS mensajes_inicial_tickets_asignados'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="N",the.id,NULL))) AS notas_tickets_asignados'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id=the.staff_id AND the.type="R",the.id,NULL))) AS mensajes_tickets_asignados'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id!=the.staff_id,the.id,NULL))) AS total_mensajes_tickets_ajenos'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="M",the.id,NULL))) AS mensajes_inicial_tickets_ajenos'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="N",the.id,NULL))) AS notas_tickets_ajenos'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.staff_id!=the.staff_id AND the.type="R",the.id,NULL))) AS mensajes_tickets_ajenos'));
        $query->addSelect(DB::raw('ROUND(COUNT(DISTINCT(the.id))/COUNT(DISTINCT(t.ticket_id)),2) AS mensajes_x_ticket'));
        $query->addSelect(DB::raw('ROUND(COUNT(DISTINCT(IF(t.staff_id=the.staff_id, t.ticket_id,NULL)))/COUNT(DISTINCT(t.ticket_id)),4) AS tickets_asignados_entre_totales'));
        $query->addSelect(DB::raw('ROUND(COUNT(DISTINCT(IF(t.staff_id!=the.staff_id, t.ticket_id,NULL)))/COUNT(DISTINCT(t.ticket_id)),4) AS tickets_ajenos_entre_totales'));


        $query->leftJoin('ost_thread_entry as the','the.staff_id','=','ost_staff.staff_id');
        $query->leftJoin('ost_thread as th','th.id','=','the.thread_id');
        $query->leftJoin('ost_ticket as t','t.ticket_id','=','th.object_id');

        $query->whereRaw('DATE(the.created) BETWEEN ? AND ?',[$dateStart,$dateEnd]);

        $query->groupBy('ost_staff.staff_id');

        $query->orderBy(DB::raw('(COUNT(DISTINCT(IF(the.type="M",the.id,NULL)))+COUNT(DISTINCT(IF(the.type="N",the.id,NULL)))+COUNT(DISTINCT(IF(the.type="R",the.id,NULL))))'),'DESC');
        $query->orderBy('username','asc');

//        dd($query->toSql());

        return $query;
    }
}
