<?php

namespace App\Modulos\OSTickets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    //
    protected $connection="mysql_ostickets";
    protected $table="ost_user";

    public function scopeReporteGeneral($query, $dateStart,$dateEnd){
        /**
        SELECT u.id, NAME AS usuario,
        COUNT(DISTINCT(t.ticket_id)) AS total_tickets,
        COUNT(DISTINCT(IF(t.user_id=the.user_id, t.ticket_id,NULL))) AS total_tickets_propios,
        COUNT(DISTINCT(IF(t.user_id!=the.user_id, t.ticket_id,NULL))) AS total_tickets_ajenos,
        COUNT(DISTINCT(the.id)) AS total_mensajes,
        COUNT(DISTINCT(IF(t.user_id=the.user_id, the.id,NULL))) AS total_mensajes_tickets_propios,
        COUNT(DISTINCT(IF(t.user_id!=the.user_id, the.id,NULL))) AS total_mensajes_tickets_ajenos
        FROM ost_user u
        LEFT JOIN ost_thread_entry the ON u.id=the.user_id
        LEFT JOIN ost_thread th ON th.id=the.thread_id
        LEFT JOIN ost_ticket t ON (t.ticket_id=th.object_id)
        WHERE DATE(the.created) BETWEEN @DESDE AND @HASTA
        GROUP BY u.id ORDER BY  COUNT(t.ticket_id) DESC,NAME
         */

        $query->addSelect('*');
        $query->addSelect(DB::raw('COUNT(DISTINCT(t.ticket_id)) AS total_tickets'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.user_id=the.user_id, t.ticket_id,NULL))) AS total_tickets_propios'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.user_id!=the.user_id, t.ticket_id,NULL))) AS total_tickets_ajenos'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(the.id)) AS total_mensajes'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.user_id=the.user_id, the.id,NULL))) AS total_mensajes_tickets_propios'));
        $query->addSelect(DB::raw('COUNT(DISTINCT(IF(t.user_id!=the.user_id, the.id,NULL))) AS total_mensajes_tickets_ajenos'));

        $query->leftJoin('ost_thread_entry as the','the.user_id','=','ost_user.id');
        $query->leftJoin('ost_thread as th','th.id','=','the.thread_id');
        $query->leftJoin('ost_ticket as t','t.ticket_id','=','th.object_id');

        $query->whereRaw('DATE(the.created) BETWEEN ? AND ?',[$dateStart,$dateEnd]);

        $query->groupBy('ost_user.id');

        $query->orderBy(DB::raw('COUNT(DISTINCT(t.ticket_id))'),'desc');
        $query->orderBy('name','asc');

        return $query;
    }
}
