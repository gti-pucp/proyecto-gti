<?php

namespace App\Modulos\OSTickets;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    protected $connection="mysql_ostickets";
    protected $table="ost_ticket";

    private static function generarSQLtiempo($fecha_inicio,$fecha_final,$tipo,$departamento_id,$equipo_id,$mostrar){
        switch ($tipo){
            case "tipo":
                $select="tickets.`topic_id` AS `topic_id`";
                $join="";
                $filtro="and tickets.`topic_id` > 0";
                $groupBy="tickets.`topic_id`";
                break;
            case "agente":
                $select="tickets.`staff_id` AS `staff_id`";
                $join="";
                $filtro="AND tickets.`staff_id` > 0";
                $groupBy="tickets.`staff_id`";
                break;
            case "equipos_de_departamento":
                $select="tickets.`team_id` AS `team_id`";
                $join="left join ost_team team on (team.team_id=tickets.team_id)";
                $filtro="AND tickets.`dept_id`=$departamento_id";
                $groupBy="tickets.`team_id`";
                break;
            case "agentes_de_equipo":
                $select="tickets.`staff_id` AS `staff_id`";
                $join="";
                $filtro="AND tickets.`dept_id`=$departamento_id AND tickets.`team_id`=$equipo_id";
                $groupBy="tickets.`staff_id`";
                break;
            case "departamento":
            default:
                $select="department.`id` AS `dept__id`";
                $join="JOIN `ost_department` department ON (tickets.`dept_id` = department.`id`)";
                $filtro="";
                $groupBy="department.`id`";
                break;
        }

//        dd($mostrar);
        if(isset($mostrar)){

            $lista_excluidos="SELECT t.ticket_id FROM ost_ticket t
LEFT JOIN `ost_thread` th ON (t.`ticket_id` = th.`object_id`)
LEFT JOIN `ost_thread_event` the ON (th.`id` = the.`thread_id` AND (the.`annulled` = 0 ))
WHERE the.data LIKE '{\"status\":7}'";
            if($mostrar=="mostrar_no_escalados"){
                $filtro_excluir_escalados="and tickets.ticket_id not in ($lista_excluidos)";
            }elseif($mostrar=="mostrar_escalados"){
                $filtro_excluir_escalados="and tickets.ticket_id in ($lista_excluidos)";
            }else{
                $filtro_excluir_escalados="";
            }
        }else{
            $filtro_excluir_escalados="";
        }

        $sql_tiempo="
            SELECT $select,
                AVG(DATEDIFF(tickets.`closed`, tickets.`created`)) AS `ServiceTime`,
                AVG(DATEDIFF(thread_entry.`created`, thread_parent.`created`)) AS `ResponseTime`
            FROM `ost_ticket` tickets
                LEFT JOIN `ost_thread` thread ON (thread.`object_type` = 'T' AND tickets.`ticket_id` = thread.`object_id`)
                LEFT JOIN `ost_thread_entry` thread_entry ON (thread.`id` = thread_entry.`thread_id` AND thread_entry.`type` = 'R')
                LEFT JOIN `ost_thread` thread_parent ON (thread_entry.`thread_id` = thread_parent.`id`)
                $join
            WHERE thread.`created` BETWEEN FROM_UNIXTIME($fecha_inicio->timestamp) AND FROM_UNIXTIME($fecha_final->timestamp)
            $filtro
            $filtro_excluir_escalados
            GROUP BY $groupBy
        ";


        return $sql_tiempo;

    }

    public static function generarSQLConteos($fecha_inicio,$fecha_final,$tipo, $departamento_id,$equipo_id,$mostrar){

        switch ($tipo){
            case "tipo":
                $select="tickets.`topic_id` AS `topic_id`";
                $join="LEFT JOIN `ost_help_topic` help_topic ON (tickets.`topic_id` = help_topic.`topic_id`)";
                $filtro="and tickets.`topic_id` > 0";
                $groupBy="tickets.`topic_id`";
                break;
            case "agente":
                $select="tickets.`staff_id` AS `staff_id`";
                $join="LEFT JOIN `ost_staff` staff ON (tickets.`staff_id` = staff.`staff_id`)";
                $filtro="AND tickets.`staff_id` > 0";
                $groupBy="tickets.`staff_id`";
                break;
            case "equipos_de_departamento":
                $select="tickets.`team_id` AS `team_id`";
                $join="left join ost_team team on (team.team_id=tickets.team_id)";
                $filtro="AND tickets.`dept_id`=$departamento_id";
                $groupBy="tickets.`team_id`";
                break;
            case "agentes_de_equipo":
                $select="tickets.`staff_id` AS `staff_id`";
                $join="";
                $filtro="AND tickets.`dept_id`=$departamento_id AND tickets.`team_id`=$equipo_id";
                $groupBy="tickets.`staff_id`";
                break;

            case "departamento":
            default:
                $select="department.`id` AS `dept__id`";
                $join="JOIN `ost_department` department ON (tickets.`dept_id` = department.`id`)";
                $filtro="";
                $groupBy="department.`id`";
                break;
        }

        if(isset($mostrar)){
            $lista_excluidos="SELECT t.ticket_id FROM ost_ticket t
LEFT JOIN `ost_thread` th ON (t.`ticket_id` = th.`object_id`)
LEFT JOIN `ost_thread_event` the ON (th.`id` = the.`thread_id` AND (the.`annulled` = 0 ))
WHERE the.data LIKE '{\"status\":7}'";
            if($mostrar=="mostrar_no_escalados"){
                $filtro_excluir_escalados="and tickets.`ticket_id` not in ($lista_excluidos)";
            }else if($mostrar=="mostrar_escalados"){
                $filtro_excluir_escalados="and tickets.`ticket_id` in ($lista_excluidos)";
            }else{
                $filtro_excluir_escalados="";
            }
        }else{
            $filtro_excluir_escalados="";
        }

        $sql_conteo="
            SELECT $select,
                   COUNT(CASE WHEN thread_event.`state` = 'created' THEN 1 END) AS `Opened`,
                   COUNT(CASE WHEN thread_event.`state` = 'assigned' THEN 1 END) AS `Assigned`,
                   COUNT(CASE WHEN thread_event.`state` = 'overdue' THEN 1 END) AS `Overdue`,
                   COUNT(CASE WHEN thread_event.`state` = 'closed' THEN 1 END) AS `Closed`,
                   COUNT(CASE WHEN thread_event.`state` = 'reopened' THEN 1 END) AS `Reopened`
            FROM `ost_ticket` tickets
                LEFT JOIN `ost_thread` thread ON (thread.`object_type` = 'T' AND tickets.`ticket_id` = thread.`object_id`)
                LEFT JOIN `ost_thread_event` thread_event ON (thread.`id` = thread_event.`thread_id` AND
                	(thread_event.`annulled` = 0 AND thread_event.`timestamp`
				BETWEEN FROM_UNIXTIME($fecha_inicio->timestamp) AND FROM_UNIXTIME($fecha_final->timestamp)))
                $join
            WHERE thread.`created` BETWEEN FROM_UNIXTIME($fecha_inicio->timestamp) AND FROM_UNIXTIME($fecha_final->timestamp)
            $filtro
            $filtro_excluir_escalados
            GROUP BY $groupBy
            ";

        return $sql_conteo;
    }


    public function scopeReporteDepartamento($query, $fecha_inicio, $fecha_final, $tipo, $mostrar){

        //TIEMPO
        $sql_tiempo=self::generarSQLTiempo($fecha_inicio,$fecha_final,$tipo,null,null,$mostrar);

        $query->addSelect(\DB::raw("ost_ticket.dept_id, D.name as departamento"));
        $query->addSelect(\DB::raw("temp_tiempo.ServiceTime as tiempo_servicio"))
            ->addSelect(\DB::raw("temp_tiempo.ResponseTime as tiempo_respuesta"));

        $sql_tiempo=\DB::raw("(".$sql_tiempo.") as temp_tiempo");

        $query->join($sql_tiempo,"temp_tiempo.dept__id","=","ost_ticket.dept_id");

        //CONTEO
        $sql_conteo=self::generarSQLConteos($fecha_inicio,$fecha_final,$tipo, null, null,$mostrar);

        $query->addSelect(\DB::raw("temp_conteo.Opened as opened"))
            ->addSelect(\DB::raw("temp_conteo.Assigned as assigned"))
            ->addSelect(\DB::raw("temp_conteo.Overdue as overdue"))
            ->addSelect(\DB::raw("temp_conteo.Closed as closed"))
            ->addSelect(\DB::raw("temp_conteo.Reopened as reopened"));

        $sql_conteo=\DB::raw("(".$sql_conteo.") as temp_conteo");

        $query->leftJoin($sql_conteo,"temp_conteo.dept__id","=","ost_ticket.dept_id");


        $query->leftJoin("ost_department as D",function($join){
            $join->on("D.id","=","ost_ticket.dept_id");
        });

        $query->groupBy('ost_ticket.dept_id');

        return $query;
    }

    public function scopeReporteTipo($query, $fecha_inicio, $fecha_final, $tipo, $mostrar){

        $query->addSelect(\DB::raw("ost_ticket.topic_id, ht.topic as tipo"));

        $sql_tiempo=self::generarSQLtiempo($fecha_inicio,$fecha_final,$tipo,null,null,$mostrar);

        $query->addSelect(\DB::raw("temp_tiempo.ServiceTime as tiempo_servicio"))
            ->addSelect(\DB::raw("temp_tiempo.ResponseTime as tiempo_respuesta"));

        $sql_tiempo=\DB::raw("(".$sql_tiempo.") as temp_tiempo");

        $query->join($sql_tiempo,"temp_tiempo.topic_id","=","ost_ticket.topic_id");

        $sql_conteo=self::generarSQLConteos($fecha_inicio,$fecha_final,$tipo,null,null,$mostrar);

        $query->addSelect(\DB::raw("temp_conteo.Opened as opened"))
            ->addSelect(\DB::raw("temp_conteo.Assigned as assigned"))
            ->addSelect(\DB::raw("temp_conteo.Overdue as overdue"))
            ->addSelect(\DB::raw("temp_conteo.Closed as closed"))
            ->addSelect(\DB::raw("temp_conteo.Reopened as reopened"));

        $sql_conteo=\DB::raw("(".$sql_conteo.") as temp_conteo");

        $query->leftJoin($sql_conteo,"temp_conteo.topic_id","=","ost_ticket.topic_id");

        //join por tipo
        $query->leftJoin("ost_help_topic as ht",function($join){
            $join->on("ht.topic_id","=","ost_ticket.topic_id");
        });

        $query->where('ost_ticket.topic_id','>',"0");

        $query->groupBy('ost_ticket.topic_id');
        return $query;
    }

    public function scopeReporteAgente($query, $fecha_inicio, $fecha_final, $tipo, $mostrar){
        $query->addSelect(\DB::raw("ost_ticket.staff_id, concat(S.firstname,' ',S.lastname) as agente"));

        $sql_tiempo=self::generarSQLtiempo($fecha_inicio, $fecha_final, $tipo,null, null,$mostrar);

        $query->addSelect(\DB::raw("temp_tiempo.ServiceTime as tiempo_servicio"))
            ->addSelect(\DB::raw("temp_tiempo.ResponseTime as tiempo_respuesta"));

        $sql_tiempo=\DB::raw("(".$sql_tiempo.") as temp_tiempo");

        $query->join($sql_tiempo,"temp_tiempo.staff_id","=","ost_ticket.staff_id");

        $sql_conteo=self::generarSQLConteos($fecha_inicio,$fecha_final,$tipo,null,null,$mostrar);

        $query->addSelect(\DB::raw("temp_conteo.Opened as opened"))
            ->addSelect(\DB::raw("temp_conteo.Assigned as assigned"))
            ->addSelect(\DB::raw("temp_conteo.Overdue as overdue"))
            ->addSelect(\DB::raw("temp_conteo.Closed as closed"))
            ->addSelect(\DB::raw("temp_conteo.Reopened as reopened"));

        $sql_conteo=\DB::raw("(".$sql_conteo.") as temp_conteo");

        $query->leftJoin($sql_conteo,"temp_conteo.staff_id","=","ost_ticket.staff_id");

        $query->leftJoin("ost_staff as S",function($join){
            $join->on("S.staff_id","=","ost_ticket.staff_id");
        });

        $query->where('ost_ticket.staff_id',">","0");
        $query->groupBy('ost_ticket.staff_id');
        return $query;
    }

    public function scopeReporteEquiposDepartamento($query, $fecha_inicio, $fecha_final, $tipo, $departamento_id, $mostrar){
        $query->addSelect(\DB::raw("ost_ticket.team_id, concat(S.name,' ') as equipo"));

        $sql_tiempo=self::generarSQLtiempo($fecha_inicio, $fecha_final, $tipo,$departamento_id, null,$mostrar);

        $query->addSelect(\DB::raw("temp_tiempo.ServiceTime as tiempo_servicio"))
            ->addSelect(\DB::raw("temp_tiempo.ResponseTime as tiempo_respuesta"));

        $sql_tiempo=\DB::raw("(".$sql_tiempo.") as temp_tiempo");

        $query->join($sql_tiempo,"temp_tiempo.team_id","=","ost_ticket.team_id");

        $sql_conteo=self::generarSQLConteos($fecha_inicio,$fecha_final,$tipo,$departamento_id,null,$mostrar);

        $query->addSelect(\DB::raw("temp_conteo.Opened as opened"))
            ->addSelect(\DB::raw("temp_conteo.Assigned as assigned"))
            ->addSelect(\DB::raw("temp_conteo.Overdue as overdue"))
            ->addSelect(\DB::raw("temp_conteo.Closed as closed"))
            ->addSelect(\DB::raw("temp_conteo.Reopened as reopened"));

        $sql_conteo=\DB::raw("(".$sql_conteo.") as temp_conteo");

        $query->leftJoin($sql_conteo,"temp_conteo.team_id","=","ost_ticket.team_id");

        $query->leftJoin("ost_team as S",function($join){
            $join->on("S.team_id","=","ost_ticket.team_id");
        });

        //$query->where('ost_ticket.team_id',">","0");
        $query->groupBy('ost_ticket.team_id');

        return $query;
    }

    public function scopeReporteAgentesEquipo($query, $fecha_inicio, $fecha_final, $tipo, $departamento_id, $equipo_id, $mostrar){
        $query->addSelect(\DB::raw("ost_ticket.staff_id, concat(S.firstname,' ',S.lastname) as agente"));

        $sql_tiempo=self::generarSQLtiempo($fecha_inicio, $fecha_final, $tipo,$departamento_id, $equipo_id,$mostrar);

        $query->addSelect(\DB::raw("temp_tiempo.ServiceTime as tiempo_servicio"))
            ->addSelect(\DB::raw("temp_tiempo.ResponseTime as tiempo_respuesta"));

        $sql_tiempo=\DB::raw("(".$sql_tiempo.") as temp_tiempo");

        $query->join($sql_tiempo,"temp_tiempo.staff_id","=","ost_ticket.staff_id");

        $sql_conteo=self::generarSQLConteos($fecha_inicio,$fecha_final,$tipo,$departamento_id,$equipo_id,$mostrar);

        $query->addSelect(\DB::raw("temp_conteo.Opened as opened"))
            ->addSelect(\DB::raw("temp_conteo.Assigned as assigned"))
            ->addSelect(\DB::raw("temp_conteo.Overdue as overdue"))
            ->addSelect(\DB::raw("temp_conteo.Closed as closed"))
            ->addSelect(\DB::raw("temp_conteo.Reopened as reopened"));

        $sql_conteo=\DB::raw("(".$sql_conteo.") as temp_conteo");

        $query->leftJoin($sql_conteo,"temp_conteo.staff_id","=","ost_ticket.staff_id");

        $query->leftJoin("ost_staff as S",function($join){
            $join->on("S.staff_id","=","ost_ticket.staff_id");
        });

        $query->where('ost_ticket.staff_id',">","0");
        $query->groupBy('ost_ticket.staff_id');
        return $query;
    }


}
