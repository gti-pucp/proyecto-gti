<?php

namespace App\Modulos\OSTickets;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $connection="mysql_ostickets";
    protected $table="ost_staff";
}
