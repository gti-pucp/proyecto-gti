<?php

namespace App\Modulos\Dataverse;

use Illuminate\Database\Eloquent\Model;

class Identificador extends Model
{
    //
    protected $table="identificadores";

    protected $fillable=['id_archivo','identificador','es_texto'];


}
