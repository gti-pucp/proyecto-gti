<?php

namespace App\Modulos\Dataverse;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    //
    protected $table="archivos";
    protected $fillable=['nombre'];

    public function registrarIdentificadores($contenido,$codigo_idioma){
        $this->limpiarIdentificadores();


        $listado=collect();
        $lineas=explode("\n",$contenido);

        foreach($lineas as $posicion=>$linea){
            $partes=explode("=",$linea);
            $conteo=count($partes);

            if($conteo==0){
                //es linea vacia
                $identificador=Identificador::create([
                    'id_archivo'=>$this->id,
                    'identificador'=>'',
                    'es_texto'=>1,
                ]);
            }elseif($conteo==1){
                $comentario=$partes[0];
                $identificador=Identificador::create([
                    'id_archivo'=>$this->id,
                    'identificador'=>$comentario,
                    'es_texto'=>0,
                ]);
            }else{
                list($id,$valor)=$partes;
                $identificador=Identificador::create([
                    'id_archivo'=>$this->id,
                    'identificador'=>$id,
                    'es_texto'=>0,
                ]);

                if(isset($identificador)){
                    $traduccion=Traduccion::create([
                        'id_identificador'=>$identificador->id,
                        'id_idioma'=>$codigo_idioma,
                        'valor'=>$valor
                    ]);
                }
            }

        }
    }

    public function getNombreArchivo(Idioma $oIdioma){
        if($oIdioma->codigo=='en'){
            return $this->nombre.".properties";
        }else{
            return $this->nombre."_".$oIdioma->codigo.".properties";
        }
    }

    private function limpiarIdentificadores(){
        Identificador::where('id_archivo',$this->id)->delete();
    }

    public function identificadores(){
        return $this->hasMany(Identificador::class,'id_identificador','id');
    }
}
