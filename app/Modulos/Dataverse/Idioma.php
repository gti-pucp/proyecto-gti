<?php

namespace App\Modulos\Dataverse;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    //
    protected $table="idiomas";
    protected $fillable=['codigo'];
}
