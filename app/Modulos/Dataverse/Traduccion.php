<?php

namespace App\Modulos\Dataverse;

use Illuminate\Database\Eloquent\Model;

class Traduccion extends Model
{
    //
    protected $table="traducciones";

    protected  $fillable=['id_identificador','id_idioma','valor'];
}
