<?php

namespace App\Modulos\Usuarios;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    //
    protected $table="permisos";
    protected $fillable=['codigo','nombre'];

    public function __toString()
    {
        return $this->nombre;
    }
}
