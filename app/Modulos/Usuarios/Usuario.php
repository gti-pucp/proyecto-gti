<?php

namespace App\Modulos\Usuarios;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //
    protected $table="users";

    public function scopeFiltrar($query,$valor){
        $query->where('name','like','%'.$valor.'%')
            ->orWhere('email','like','%'.$valor.'%');

        return $query;
    }

    public function scopeOrdenar($query,$orden){
        return $query;
    }


//    public function getUrlImagen(){
//        return route('perfil.imagen');
//    }
//

    //
    public function permisos(){
        return $this->belongsToMany(Permiso::class,'permisos_usuarios','id_usuario','id_permiso');
    }

    public function tienePermiso($codigo){
        $numero_permisos_asignados=$this->permisos()->where('permisos.codigo',$codigo)->where('estado',true)->count();
        return $numero_permisos_asignados>0; // || $this->isSuperAdmin();
    }

    public function isSuperAdmin(){
        $administradores=[1,2];
        return in_array($this->id,$administradores);
    }
}
