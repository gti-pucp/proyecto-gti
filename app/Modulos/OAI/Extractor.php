<?php

namespace App\Modulos\OAI;

use Carbon\Carbon;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Util\Xml;

class Extractor
{
    private $url;
    private $petition_number=0;

    private $data;
    private $resumption_token;

    public function __construct($url)
    {
        $this->url=$url;
    }

    public function getParams(){
        $params=['verb'=>'ListRecords'];

        if($this->resumption_token!=""){
            $params['resumptionToken']=$this->resumption_token;
        }else{
            $params['metadataPrefix']='oai_dc';
        }

        return $params;
    }

    public function obtenerDatosRemotos(){
        $params=$this->getParams();
        $this->petition_number++;

        $this->data=$this->getRemoteData($this->url,$params);

        $xml=new XmlDepurador($this->data);
        $this->resumption_token=$xml->getResumptionToken();
    }

    public function tieneMasRegistros(){
        return isset($this->resumption_token);
    }

    public function saveXml($directorio){
        $ruta_archivo_xml=$directorio."/".$this->petition_number.'.xml';
        $xml=new XmlDepurador($this->data);
        return $xml->saveXML($ruta_archivo_xml);
    }

    private function formatURL($url,$params){
        return $url."?".http_build_query($params);
    }

    private function getRemoteData($url,$params){

        $url=$this->formatURL($url,$params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);

        $output=curl_exec($ch);

        return $output;
    }


}
