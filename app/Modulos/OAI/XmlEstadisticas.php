<?php


namespace App\Modulos\OAI;

class XmlEstadisticas{
    public static $CONTEO_ACCESO_RESTRINGIDO=0;
    public static $CONTEO_ACCESO_ABIERTO=0;
    public static $CONTEO_ACCESO_EMBARGADO=0;
    public static $CONTEO_ACCESO_OTRO=0;

    public static $COLECCIONES=[];
    public static $CONTEO_REGISTROS=0;
    public static $REGISTROS_XML=[];

    public static $REGISTROS_COLECCION_MULTIPLE=[];

    public static $REGISTROS_HABILITADOS;
    public static $REGISTROS_ELIMINADOS;

    public static $ARCHIVO_ORIGEN="";
    /**
     * @var array
     */
    public static $REGISTROS_FORMATO_MULTIPLE=[];

    public static function analizar($archivo,$xml){
        self::$ARCHIVO_ORIGEN=$archivo;
        foreach($xml->getRecord() as $record) {
            XmlEstadisticas::estadisticasPorEstado($record);
            XmlEstadisticas::estadisticasPorXML($record);
            XmlEstadisticas::estadisticasPorColeccion($record);

            $lista_campos_dc=$xml->getCamposDC($record);
            if(isset($lista_campos_dc)){
                XmlEstadisticas::estadisticasPorMetadata($record,$lista_campos_dc);
            }
        }
    }

    public static function estadisticasPorMetadata($record,$lista_campos_dc){
        $rights_encontrado=false;

        $formatos=[];
        foreach($lista_campos_dc as $element){
            if($element instanceof \SimpleXMLElement){
                $name=$element->getName();
                $value=$element;
            }else{
                $name=$element->name;
                $value=$element->value;
            }

            if($name=="format"){
                $formatos[]=$value;
            }

            if($name=="rights" && strpos($value,"/semantics/")!=false
                && $rights_encontrado==false){
                $rights_encontrado=true;

                $aa="info:eu-repo/semantics/openAccess";
                $ae="info:eu-repo/semantics/embargoedAccess";
                $ar="info:eu-repo/semantics/restrictedAccess";

                if(in_array($value,[$aa,XmlDepurador::getArrayValores()[$aa]])){
                    self::$CONTEO_ACCESO_ABIERTO++;
                }elseif(in_array($value,[$ae,XmlDepurador::getArrayValores()[$ae]])){
                    self::$CONTEO_ACCESO_EMBARGADO++;
                }elseif(in_array($value,[$ar,XmlDepurador::getArrayValores()[$ar]])){
                    self::$CONTEO_ACCESO_RESTRINGIDO++;
                }else{
                    self::$CONTEO_ACCESO_OTRO++;
                }
            }
        }

        if(count($formatos)>1){
            $identifier=(string)$record->header->identifier;
            $parts_identifier=explode(":",$identifier);
            $handle_identifier=array_pop($parts_identifier);
            self::$REGISTROS_FORMATO_MULTIPLE[]=(object)['identifier'=>$identifier,'handle'=>$handle_identifier,'archivo'=>self::$ARCHIVO_ORIGEN,'formatos'=>$formatos];
        }
    }

    public static function estadisticasPorXML(\SimpleXMLElement $record){
        self::$CONTEO_REGISTROS++;
        if(isset(self::$REGISTROS_XML[self::$ARCHIVO_ORIGEN])){
            self::$REGISTROS_XML[self::$ARCHIVO_ORIGEN]->total_registros++;
        }else{
            self::$REGISTROS_XML[self::$ARCHIVO_ORIGEN]=(object)['ruta'=>basename(self::$ARCHIVO_ORIGEN),'total_registros'=>1];
        }
    }
    public static function estadisticasPorColeccion(\SimpleXMLElement $record)
    {
        $colecciones_multiples=[];
        foreach($record->header->setSpec as $elemento){
            $parts=explode("_",$elemento);
            $prefix=array_shift($parts);
            $handle_coleccion=implode("/",$parts);

            if($prefix=="col"){
                $colecciones_multiples[]=$handle_coleccion;
                //extraer coleccion de origen y contabilizar registros
                if(isset(self::$COLECCIONES[$handle_coleccion])){
                    self::$COLECCIONES[$handle_coleccion]->total_registros++;
                }else{
                    self::$COLECCIONES[$handle_coleccion]=(object)['handle'=>$handle_coleccion,'total_registros'=>1];
                }
            }
        }

        if(count($colecciones_multiples)>1){
            $parts_identifier=explode(":",(string)$record->header->identifier);
            $handle_identifier=array_pop($parts_identifier);

            self::$REGISTROS_COLECCION_MULTIPLE[]=(object)[
                'identifier'=>(string) $record->header->identifier,
                'handle'=>(string)$handle_identifier,
                'archivo'=>self::$ARCHIVO_ORIGEN,
                'colecciones'=>$colecciones_multiples
            ];
        }
    }

    public static function estadisticasPorEstado(\SimpleXMLElement $record)
    {
        $identifier=(string)$record->header->identifier;
        $parts_identifier=explode(":",$identifier);
        $handle_identifier=array_pop($parts_identifier);

        if(!isset($record->metadata)){
            self::$REGISTROS_ELIMINADOS[]=(object)['identifier'=>$identifier,'handle'=>$handle_identifier,'archivo'=>basename(self::$ARCHIVO_ORIGEN)];
        }else{
            self::$REGISTROS_HABILITADOS[]=(object)['identifier'=>$identifier,'handle'=>$handle_identifier,'archivo'=>basename(self::$ARCHIVO_ORIGEN)];
        }
    }
}
