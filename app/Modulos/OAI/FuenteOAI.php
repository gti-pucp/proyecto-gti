<?php

namespace App\Modulos\OAI;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class FuenteOAI extends Model
{
    //
    const FUENTE_DSPACE_56="DS6";
    const FUENTE_OJS_3="OJS";

    protected $table="fuentes_oai";

    private $ruta_comprimido;
    private $temp_dir;

    private $lista_archivos_xml;
    private $depurar;

    private function createDirectoryTemp(){
        $this->temp_dir=Carbon::now()->getPreciseTimestamp(4);
        Storage::disk('local')->makeDirectory($this->temp_dir);

    }

    public function getUrlIdentifier(){
        return $this->url."?verb=Identify";
    }

    public function obtenerArchivosRemotos(){
        if(!isset($this->lista_archivos_xml)){
            $this->lista_archivos_xml=collect();
        }
        $this->createDirectoryTemp();

        $extractor=new Extractor($this->url);
        $extractor->obtenerDatosRemotos();
        $this->lista_archivos_xml->push($extractor->saveXml($this->temp_dir));

        while($extractor->tieneMasRegistros()){
            $extractor->obtenerDatosRemotos();
            $this->lista_archivos_xml->push($extractor->saveXml($this->temp_dir));
        }
        return $this->lista_archivos_xml;
    }
    public function generarEstadisticas()
    {
        foreach ($this->lista_archivos_xml as $archivo) {
            $data = Storage::disk('local')->get($archivo);
            $xml=new XmlDepurador($data);
            XmlEstadisticas::analizar($archivo,$xml);
        }
    }

    public function depurarData(){
        $this->depurar=true;
        $nueva_coleccion=collect();
        foreach($this->lista_archivos_xml as $archivo){
            $data=Storage::disk('local')->get($archivo);
            $xml=new XmlDepurador($data);
            $xml->depurarContenido();
            $ruta_archivo=$xml->saveXML($archivo);

            $nueva_coleccion->push($ruta_archivo);
        }

        $this->lista_archivos_xml=$nueva_coleccion;
    }

    public function empaquetar($borrar_archivos_origen=true){

        $comprimido=new Comprimido($this->temp_dir,$this->lista_archivos_xml);
        $comprimido->eliminarOrigenArchivos($borrar_archivos_origen);
        return $comprimido->empaquetar();
    }

    public function getNombreComprimido(){
        $fecha=Carbon::now();
        $sufijo=collect([
            parse_url($this->url, PHP_URL_HOST),
            $fecha->format("Ymd"),
            $fecha->format("hisa"),
            $this->depurar==1?'depurado':'original',

        ]);

        return $sufijo->implode("_").'.zip';
    }

    public function getRutaComprimido(){
        return $this->ruta_comprimido;
    }


}
