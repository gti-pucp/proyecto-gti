<?php


namespace App\Modulos\OAI;


use Illuminate\Support\Facades\Storage;

class XmlDepurador
{
    /**
     * @var array
     */
    /**
     * @var array
     */



    /**
     * @var array
     */

    private $data_xml;

    public function __construct($data)
    {
        $this->data_xml=simplexml_load_string($data);
    }

    public function getResumptionToken(){
        if(isset($this->data_xml->ListRecords)
            && isset($this->data_xml->ListRecords->resumptionToken)
            && $this->data_xml->ListRecords->resumptionToken->__toString()!=""
        ){
            return $this->data_xml->ListRecords->resumptionToken->__toString();
        }
        return null;
    }

    public function saveXML($ruta_archivo){
        Storage::disk('local')->put($ruta_archivo,$this->data_xml->asXML());
        return $ruta_archivo;
    }

    public function depurarContenido()
    {
        set_time_limit(0);
        foreach($this->getRecord() as $record){
            //RESETEA HEADER
            $record->header="";
            //obtiene la lista de metadatos corregida para resetear y añadirlos nuevamente.
            $lista_campos_dc=$this->getCamposDC($record);
            if(isset($lista_campos_dc)){
                $this->depurarMetadata($record,$lista_campos_dc);
            }
        }
        $data=self::reemplazarCaracteres($this->data_xml->asXML());
        $this->data_xml=simplexml_load_string($data);
    }

    public function getCamposDC($record){
        $ns=$record->metadata->getNamespaces(true);
        if(isset($record->metadata)){
            return $record->metadata->children($ns['oai_dc'])->children($ns['dc']);
        }
        return null;
    }

    public function getOAI_DC($record){
        $ns=$record->metadata->getNamespaces(true);
        return $record->metadata->children($ns['oai_dc']);
    }

    public function getRecord(){
        return $this->data_xml->ListRecords->record;
    }

    public static function reemplazarCaracteres($contenido){
        $array_limpieza=array_merge(
            self::getArrayValores(),
            self::getArrayCaracteres(),
            self::getArrayEtiquetas()
        );

        return str_replace(array_keys($array_limpieza),array_values($array_limpieza),$contenido);
    }

    public static function getArrayValores(){
        return [
            #tipos
            "info:eu-repo/semantics/doctoralThesis"=>"Tesis de doctorado",
            "info:eu-repo/semantics/masterThesis"=>"Tesis de maestría",
            "info:eu-repo/semantics/bachelorThesis"=>"Tesis de grado",
            #derechos
            "info:eu-repo/semantics/openAccess"=>"Acceso abierto",
            "info:eu-repo/semantics/embargoedAccess"=>"Acceso embargado",
            "info:eu-repo/semantics/restrictedAccess"=>"Acceso restringido",
            #formato
            "application/pdf"=>"PDF",
        ];
    }

    public static function getArrayEtiquetas(){
        return [
            #ETIQUETAS AUTOR
            "</dc:creator>"=>".</dc:creator>",
            "..</dc:creator>"=>".</dc:creator>",

            #ETIQUETAS TEMA
            "</dc:title>"=>".</dc:title>",
            "..</dc:title>"=>".</dc:title>",

            #ETIQUETAS CONTRIBUTOR
            "</dc:contributor>"=>".</dc:contributor>",
            "..</dc:contributor>"=>".</dc:contributor>",
        ];
    }

    private static function getArrayCaracteres(){
        return [
            "\r"=>" ",
            "\n"=>" ",
            "\n\r"=>" ",
            "\r\n"=>" ",
            "&#xd;"=>" ",
            "&#13;"=>" ",
        ];
    }

    private static function getEtiquetasProhibidas(){
        return [
            'date.issued',
            'format'
        ];
    }

    private function depurarMetadata($record,$lista_elements)
    {
        $lista_depurada=collect();

        $date_encontrado=false;
        foreach($lista_elements as $element){

            if($element->getName()=='description.abstract'){
                //REEMPLAZA CARACTERES Y CAMPO DESCRIPTION;
                $array_limpieza=self::getArrayCaracteres();
                $descripcion=str_replace(array_keys($array_limpieza),array_values($array_limpieza),$element);
                $lista_depurada->push((object)['name'=>'description',
                    'value'=>$descripcion]);
            }elseif($element->getName()=='date'){
                //VERIFICA SI EL DATE TIENE 4 caracteres y no ha sido asignado, lo agrega a la lista
                if(strlen($element->__toString())==4 && $date_encontrado==false){
                    $date_encontrado=true;
                    $lista_depurada->push((object)['name'=>$element->getName(),
                        'value'=>clone $element]);
                }
            }elseif(in_array($element->getName(),self::getEtiquetasProhibidas())){
                //si es el campo lo remueve de la lista
            }else{
                //cualquier otro caso, lo agrega normalmente.
                $lista_depurada->push((object)['name'=>$element->getName(),
                    'value'=>clone $element]);
            }
        }

        //setea nuevos valores en registro
        $this->getOAI_DC($record)->dc="";
        foreach($lista_depurada as $item){
            $this->getOAI_DC($record)->addChild($item->name,htmlspecialchars($item->value),'http://purl.org/dc/elements/1.1/');
        }
    }
}
