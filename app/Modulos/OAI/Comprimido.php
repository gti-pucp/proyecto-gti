<?php

namespace App\Modulos\OAI;

use Chumper\Zipper\Facades\Zipper;
use Illuminate\Support\Facades\Storage;

class Comprimido extends Zipper
{
    private $directory;
    private $subdirectory;
    private $lista_archivos;

    private $eliminar_archivos_origen;
    public function __construct($subdirectory,$lista_archivos)
    {
        $this->directory=Storage::disk('local')->getAdapter()->getPathPrefix().$subdirectory;
        $this->subdirectory=$subdirectory;
        $this->lista_archivos=$lista_archivos;
    }

    public function eliminarOrigenArchivos($eliminar=true){
        return $this->eliminar_archivos_origen=$eliminar;
    }

    public  function empaquetar($archivo_comprimido=""){
        $ruta_archivo_comprimido=$this->directory.$archivo_comprimido.".zip";

        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make($ruta_archivo_comprimido);
        $zipper->add($this->directory);
        $zipper->close();

        if($this->eliminar_archivos_origen){
            Storage::disk('local')->deleteDirectory($this->subdirectory);
        }

        return $ruta_archivo_comprimido;
    }
}
