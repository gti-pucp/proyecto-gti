<?php

namespace App\Modulos\SB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evento extends Model
{
    //
    use SoftDeletes;
    const SORTEO = 1;
    const COLECTA = 2;
    protected $connection="mysql";
    protected $table='eventos';

    protected $dates=['fecha_hora'];

    public function scopeSorteos($query){
        $query->where('tipo',self::SORTEO);
        return $query;
    }

    public function estaInscrito($usuario){
        return random_int(0,1);
    }

    public function sorteados(){
        return $this->hasMany(Posicion::class,'id_evento','id');
    }

    public function invitados(){
        return $this->hasMany(Invitado::class,'id_evento','id');
    }

    public function participantes(){
        return $this->hasMany(Invitado::class,'id_evento','id')->where('');
    }
}
