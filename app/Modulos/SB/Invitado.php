<?php

namespace App\Modulos\SB;

use Illuminate\Database\Eloquent\Model;

class Invitado extends Model
{
    //
    protected $connection="mysql";
    protected $table='invitados';

    const PARTICIPANTE=2;
    const NO_PARTICIPA=1;
    const INVITADO=0;
}
