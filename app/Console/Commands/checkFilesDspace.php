<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CheckFilesDspace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dspace:checkfiles '
                            .'{repo : tesis/repositorio} '
                            .'{type : assetstore/status} '
                            .'{--status= : activos/eliminados} '
                            .'{--store_number= : numero de store en bd} '
                            .'{--exportar}'
                            .'{--test} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dspace:checkfiles [tesis/repositorio] [assetstore/status]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */



    private $repo;
    private $type;
    private $status;
    private $store_number;
    private $pruebas;

    private $deleted;

    public function handle()
    {
        //
        $this->repo=$this->argument('repo');
        $this->type=$this->argument('type');
        $this->status=$this->option('status');
        $this->store_number=$this->option('store_number');
        $this->pruebas=$this->option('test');


        if($this->type=="assetstore"){
            $this->extraerDataByStore();
        }elseif($this->type=="status"){
            $this->extraerDataByStatus();
        }else {
            $this->info("no se especifico accion");
        }
    }

    private function extraerDataByStore(){
        $resumen=$this->getResumenByStore();
        foreach($resumen as $fila){
            if(!isset($this->store_number) || $this->store_number==$fila->store_number){
                $this->info($this->store_number. "Analizar ".$fila->total." archivos store [".$fila->store_number."]    con ".round($fila->peso,2)." GB");
            }
        }

        if(isset($this->store_number)){
            $this->info(" CARGANDO REGISTROS ");

            // procesar solo filtrado
            foreach($resumen as $fila){
                if($this->store_number==$fila->store_number){
                        $this->analizar($fila->total,"store".$this->store_number);
                        $this->info("\n Procesados ".$fila->total." archivos en store[".$this->store_number."] con ".round($fila->peso,2)." GB");
                }
            }
        }
    }

    private function extraerDataByStatus(){
        // prepara el tipo de accion
        if($this->status=="activos"){
            $this->deleted=false;
        }elseif($this->status=="eliminados"){
            $this->deleted=true;
        }


        $resumen=$this->getResumenByStatus();

        foreach($resumen as $fila){
            if(!isset($this->status) || $this->deleted==$fila->deleted){
                $this->info("Analizar ".$fila->total." archivos ".($fila->deleted?'eliminados':'activos')." con ".round($fila->peso,2)." GB");
            }
        }

        if(isset($this->status)){
            $this->info(" CARGANDO REGISTROS ");

            // procesar solo filtrado
            foreach($resumen as $fila){
                if($this->deleted==$fila->deleted){
                    if(!$fila->deleted){
                        $this->analizar($fila->total,"activos");
                        $this->info("\n Procesados ".$fila->total." archivos activos    con ".round($fila->peso,2)." GB");
                    }else{
                        $this->analizar($fila->total,"eliminados");
                        $this->info("\n Procesados ".$fila->total." archivos eliminados con ".round($fila->peso,2)." GB");
                    }
                }
            }
        }
    }

    private function getResumenByStore(){
        if($this->repo=='tesis'){
            return \App\Modulos\Tesis\Bitstream::selectRaw('store_number,count(uuid) as total, sum(size_bytes)/1024/1024/1024 as peso')->groupBy('store_number')->get();
        }elseif($this->repo=='repositorio'){
            return \App\Modulos\RI\Bitstream::selectRaw('store_number,count(uuid) as total, sum(size_bytes)/1024/1024/1024 as peso')->groupBy('store_number')->get();
        }else{
            return collect();
        }

    }

    private function getResumenByStatus(){
        if($this->repo=='tesis'){
            return \App\Modulos\Tesis\Bitstream::selectRaw('deleted,count(uuid) as total, sum(size_bytes)/1024/1024/1024 as peso')->groupBy('deleted')->get();
        }elseif($this->repo=='repositorio'){
            return \App\Modulos\RI\Bitstream::selectRaw('deleted,count(uuid) as total, sum(size_bytes)/1024/1024/1024 as peso')->groupBy('deleted')->get();
        }

    }

    private function getBitstreamsByStatus($limit,$offset){
        if($this->repo=='tesis'){
            return \App\Modulos\Tesis\Bitstream::where('deleted',$this->deleted)->limit($limit)->offset($offset)->get();
        }elseif($this->repo=='repositorio'){
            return \App\Modulos\RI\Bitstream::where('deleted',$this->deleted)->limit($limit)->offset($offset)->get();
        }

    }

    private function getBitstreamsByStore($limit,$offset){
        if($this->repo=='tesis'){
            return \App\Modulos\Tesis\Bitstream::where('store_number',$this->store_number)->limit($limit)->offset($offset)->get();
        }elseif($this->repo=='repositorio'){
            return \App\Modulos\RI\Bitstream::where('store_number',$this->store_number)->limit($limit)->offset($offset)->get();
        }
    }

    private function getBitstreams($limit,$offset){
        if($this->type=="assetstore"){
            return $this->getBitstreamsByStore($limit,$offset);
        }elseif($this->type=="status"){
            return $this->getBitstreamsByStatus($limit,$offset);
        }
    }

    private function setearDirectorioBase(){

        if($this->repo=='tesis'){
            return "/data/tesis/assetstore/";
        }elseif($this->repo=='repositorio'){
            return "/data/institucional/assetstore/";
        }
    }

    private $log_file="ri.txt";

    private function setLogFile($tipo){
        $this->log_file=$this->repo."_".Carbon::now()->format("Ymd_His")."_".$tipo.".txt";
        $this->export_file=$this->repo."_".Carbon::now()->format("Ymd_His")."_".$tipo."-check.txt";
    }

    private function analizar($total,$tipo){
        $this->setLogFile($tipo);

        $limit=1000;
        $bucle_maximo=ceil($total/$limit);
        $bucle_actual=0;

        if($this->pruebas){
            $bucle_maximo=2;    // PRUEBAS
            $limit=10;          // PRUEBAS
        }

        $this->progresbar = $this->output->createProgressBar($total);

        while($bucle_actual<$bucle_maximo){
            //


//            $this->info('Recorriendo '.$bucle_actual." de ".$bucle_maximo." mostrando hasta ".$limit*($bucle_actual+1));
//            $this->info(' limit '.$limit." offset ".$limit*$bucle_actual);

            $offset=$bucle_actual*$limit;

            $this->procesarBitstreams($limit,$offset);
            $bucle_actual++;

        }
        $this->progresbar->finish();
    }



    private function procesarBitstreams($limit,$offset){
        $this->baseDataDir=$this->setearDirectorioBase();

        $bitstreams=$this->getBitstreams($limit,$offset);

        $messageLote="";
        $messageExport="";
        foreach ($bitstreams as $pos=>$bitstream){
            $this->progresbar->advance();
            $this->setBitstreamFilePath($bitstream);

            $posicion=$pos+1+$offset;

            $message_info="(".$posicion.")".
                "uuid: ".$bitstream->uuid." \t " .
                "internal_id: ".$bitstream->internal_id."\t " .
                "file: ".$bitstream->file." \t ".
                "size: ".$bitstream->size_bytes;

            $messageLote.=($pos>0?"\n":"").$message_info;

            $messageExport.=($pos>0?"\n":"").$bitstream->file;
        }

        $this->generarLogs($messageLote,$messageExport);
    }

    private function generarLogs($data_log,$data_exportacion){
        //$this->info($data_log);
        Storage::append($this->log_file, $data_log);
        Storage::append($this->export_file, $data_exportacion);
    }

    private $baseDataDir;

    private function setBitstreamFilePath($bitstream){

        $internal_id=$bitstream->internal_id;
        $ruta=[
            substr($internal_id,0,2),
            substr($internal_id,2,2),
            substr($internal_id,4,2),
            $internal_id
        ];

        $bitstream->file=$this->baseDataDir.implode("/",$ruta);
    }
}
