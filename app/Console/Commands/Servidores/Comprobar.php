<?php

namespace App\Console\Commands\Servidores;

use App\Mail\NotificarServidorCaido;
use App\Modulos\Monitoreo\Servidor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class Comprobar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'servidores:comprobar {opcion?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comprobar estado de servidor y notificar\n test ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        try {
            $servidores = Servidor::where("comprobar", true)->get();

            foreach ($servidores as $servidor) {
                $isOnline=$servidor->isOnline();
                $servidor->procesarBitacora($isOnline);

                if($servidor->notificar_caida){
                    Mail::send(new NotificarServidorCaido($servidor));
                }

                //LOG CONSOLA
                $status = $isOnline?"online":"offline";
                $this->line($servidor->url . " : " . $status);
            }
        }catch(\Exception $e){
            $this->line($e->getMessage());
        }
    }
}
