<?php

namespace App\Providers;


use App\Modulos\EZPROXY\ReporteEZProxy;
use App\Modulos\Monitoreo\Servidor;
use App\Modulos\OAI\Comprimido;
use App\Modulos\OJS\v2\Reporte as ReporteOJS;
use App\Modulos\OSTickets\Reporte as ReporteOSTickets;
use App\Modulos\Tesis\Reporte as ReporteTesis;
use App\Modulos\RI\Reporte as ReporteRI;
use App\Modulos\Usuarios\Permiso;
use App\Modulos\Usuarios\Usuario;
use App\Policies\PermisosPolicy;
use App\Policies\ReporteDspaceRIPolicy;
use App\Policies\ReporteDspaceTesisPolicy;
use App\Policies\ReporteEZProxyPolicy;
use App\Policies\ReporteOJSPolicy;
use App\Policies\ReporteOSTicketsPolicy;
use App\Policies\UsuariosPolicy;

use App\Policies\OAIPolicy;
use App\Policies\ServidorPolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Servidor::class => ServidorPolicy::class,
        Comprimido::class => OAIPolicy::class,
        Usuario::class=>UsuariosPolicy::class,

        ReporteOJS::class => ReporteOJSPolicy::class,
        ReporteEZProxy::class => ReporteEZProxyPolicy::class,
        Permiso::class=>PermisosPolicy::class,

        ReporteOSTickets::class=>ReporteOSTicketsPolicy::class,
        ReporteTesis::class=>ReporteDspaceTesisPolicy::class,
        ReporteRI::class=>ReporteDspaceRIPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
