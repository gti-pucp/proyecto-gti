<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class CorreosMultiples implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $correos_error=[];
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rules = [
            'email' => 'email',
        ];
        $listado=explode(',',$value);

        foreach ($listado as $email) {
            $data = [
                'email' => $email
            ];
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $this->correos_error[]=$email;
            }
        }


        return count($this->correos_error)==0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El campo :attribute tiene uno o mas valores no permitidos: "'.implode(',',$this->correos_error).'"';
    }
}
