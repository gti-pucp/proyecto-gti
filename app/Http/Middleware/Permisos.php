<?php

namespace App\Http\Middleware;

use App\Modulos\Usuarios\Permiso;
use App\Modulos\Usuarios\Usuario;
use Closure;
use Illuminate\Support\Facades\Auth;

class Permisos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$codigo_permiso= null)
    {
        if (!Auth::user()->tienePermiso($codigo_permiso)) {
            $permiso=Permiso::where('codigo',$codigo_permiso)->first();
            return abort(403,"No tiene los permisos suficientes para acceder a ".$permiso->titulo);
        }

        return $next($request);
    }
}
