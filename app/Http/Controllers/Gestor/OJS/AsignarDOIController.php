<?php

namespace App\Http\Controllers\Gestor\OJS;

use App\Modulos\OJS\v3\Issue;
use App\Modulos\OJS\v3\Journal;
use App\Modulos\OJS\v3\PublishedSubmission;
use App\Modulos\OJS\v3\Submission;
use App\Modulos\OJS\v3\SubmissionSetting;
use App\Policies\ReporteOJSPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AsignarDOIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteOJSPolicy::class);
    }

    public function form(){
        return view('gestor.ojs.doi-form');
    }

    public function register(Request  $request){
        $doi_multiple=$request->input('doi_multiple');
        $orden_parametros=$request->input('orden_parametros');

        //Remueve saltos de linea
        $doi_multiple=explode(PHP_EOL,trim($doi_multiple));


        //remueve lineas en vacias
        $doi_multiple=array_filter($doi_multiple);

        //formatea lineas como array indice/doi => valor/url
        $doi_depurados=[];
        foreach($doi_multiple as $doi_line){
            // recorre cada linea
           if(strpos($doi_line,"\t")>-1){
                //Si tiene tabs de separacion
                $item=explode("\t",$doi_line);

            }elseif(strpos($doi_line,"=>")>-1){
                //si tiene sintaxis de array
                $doi_line=str_replace(["'",","],["",""],$doi_line);
                $item=explode('=>',$doi_line);
            }else if(strpos(trim($doi_line)," ")>-1){
                   //Si tiene tabs de separacion
                   $item=explode(" ",trim($doi_line));

           }

            //SI LOS VALORES SON VACIOS NO AGREGA
            if($item[0]!="" || $item[1]!=""){
                $doi_depurados[trim($item[0])]=trim($item[1]);
            }
        }



        if($orden_parametros!="doi_url"){
            $doi_depurados=array_flip($doi_depurados);
        }


//        dd($doi_depurados);

        $listado_resultados=$this->registrarDOIS($doi_depurados);

        $data=compact('listado_resultados');

        return view('gestor.ojs.doi-form-result',$data);
    }

    private function registrarDOIS($listado){
        set_time_limit(0);

//        $listado=[
////            'https://doi.org/10.18800/sordaysonora.201901.001'=>'http://revistas.pucp.edu.pe/index.php/sordaysonora/article/view/21815',
////            'https://doi.org/10.18800/sordaysonora.201901.002'=>'http://revistas.pucp.edu.pe/index.php/sordaysonora/article/view/21811',
//        ];

        $lista_resultados=collect();
        foreach ($listado as $doi=>$url){
            $submission_id=basename($url);
            $doi_suffix=basename($doi);
            $pub_id_doi="10.18800/".$doi_suffix;

            $item_list=(object)compact(
                'url',
                'doi',
                'submission_id'
                ,'doi_suffix',
                'pub_id_doi'
            );

            \DB::beginTransaction();
            $lista_resultados->push($item_list);
            try {

                if(strpos($url,"revistas.pucp.edu.pe/index.php/")===false){
                    throw new \Exception("La url no corresponde a revistas ".$url);
                }

                if(strpos($doi,"https://doi.org/")===false){
                    throw new \Exception("La url no corresponde a doi ".$doi );
                }

                $this->registrarDoiSuffix($item_list);
                $this->registrarPubIdDoi($item_list);
                \DB::commit();
                $item_list->status=true;
            }catch (\Exception $e){
                $item_list->status=false;
                $item_list->error=$e->getMessage();
                \DB::rollback();
            }



        }

        return $lista_resultados;
    }

    private function registrarDoiSuffix($item){
        $submission_setting=SubmissionSetting::prepararQuery($item->submission_id,'doiSuffix')->first();
        if($submission_setting instanceof SubmissionSetting){
            if($submission_setting->setting_value!=$item->doi_suffix){
                $item->doi_suffix_status="ACTUALIZADO";
                $item->doi_suffix_anterior=$submission_setting->setting_value;
                $submission_setting=SubmissionSetting::prepararQuery($item->submission_id,'doiSuffix')->update([
                    "setting_value"=>$item->doi_suffix
                ]);
            }else{
                $item->doi_suffix_status="EXISTENTE";
                $item->doi_suffix_anterior=null;
            }
        }else{
            $item->doi_suffix_status='REGISTRADO';
            $item->doi_suffix_anterior=null;
            $submission_setting= SubmissionSetting::create([
                'submission_id'=>$item->submission_id,
                'locale'=>'',
                'setting_name'=>'doiSuffix',
                'setting_value'=>$item->doi_suffix,
                'setting_type'=>'string'
            ]);
        }

        $item->doi_suffix_actual=$submission_setting->setting_value;

        $path=$submission_setting->submission->journal->path;
        $item->url_revistas="https://revistas.pucp.edu.pe/index.php/".$path."/article/view/".$item->submission_id;
    }

    private function registrarPubIdDoi($item)
    {
        $submission_setting=SubmissionSetting::prepararQuery($item->submission_id,'pub-id::doi')->first();
        if($submission_setting instanceof SubmissionSetting){

            if($submission_setting->setting_value!=$item->pub_id_doi) {
                $item->pub_id_doi_status = "ACTUALIZADO";
                $item->pub_id_doi_anterior = $submission_setting->setting_value;
                            $submission_setting=SubmissionSetting::prepararQuery($item->submission_id,'pub-id::doi')->update([
                                "setting_value"=>$item->pub_id_doi
                            ]);
            }else{
                $item->pub_id_doi_status="EXISTENTE";
                $item->pub_id_doi_anterior=null;
            }


        }else{
            $item->pub_id_doi_status='REGISTRADO';
            $item->pub_id_doi_anterior=null;
            $submission_setting=SubmissionSetting::create([
                'submission_id'=>$item->submission_id,
                'locale'=>'',
                'setting_name'=>'pub-id::doi',
                'setting_value'=>$item->pub_id_doi,
                'setting_type'=>'string'
            ]);
        }

        $item->pub_id_doi=$submission_setting->setting_value;

        $item->url_doi="https://doi.org/".$item->pub_id_doi;
    }

}


