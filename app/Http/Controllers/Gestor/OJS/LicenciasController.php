<?php

namespace App\Http\Controllers\Gestor\OJS;

use App\Modulos\OJS\v3\Submission;
use App\Modulos\OJS\v3\SubmissionSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LicenciasController extends Controller
{
    //
    public function index(Request $request){
        $filtros=SubmissionSetting::filtrarLicenciasRegistradas()
            ->paginate(20);

        $url_licencias_permitidas=[
            'http://creativecommons.org/licenses/by-nc-nd/4.0',
            'http://creativecommons.org/licenses/by-nc/4.0',
            'http://creativecommons.org/licenses/by-nc-sa/4.0',
            'http://creativecommons.org/licenses/by-nd/4.0',
            'http://creativecommons.org/licenses/by/4.0',
            'http://creativecommons.org/licenses/by-sa/4.0'
        ];

        foreach($filtros as $item){
            $item->permitido=in_array($item->setting_value,$url_licencias_permitidas);
            $item->url_filtrado=route('revistas3.licencias',['licencia'=>$item->setting_value]);
        }

        $filtro=$request->get('licencia');


        $listado=Submission::filtrarLicencias($filtro)->paginate(20);




        foreach($listado as $submission){
            $submission->url_revista="https://revistas.pucp.edu.pe/index.php/";
            $submission->url_revista.=$submission->journal->path;
            $submission->url_revista.="/article/view/".$submission->submission_id;

            $submission->url_gestor="https://revistas.pucp.edu.pe/index.php/";
            $submission->url_gestor.=$submission->journal->path;
            $submission->url_gestor.="/workflow/index/".$submission->submission_id."/5";
        }


        $data=compact('listado','filtro','filtros','url_licencias_permitidas');
        return view('gestor.ojs.licencias',$data);
    }
}
