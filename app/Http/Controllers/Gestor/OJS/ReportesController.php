<?php

namespace App\Http\Controllers\Gestor\OJS;

use App\Modulos\OJS\v2\Grafico;
use App\Modulos\OJS\v2\Journal;
use App\Modulos\OJS\v2\Reporte;
use App\Modulos\OJS\v3\Journal as JournalV3;
use App\Policies\ReporteOJSPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteOJSPolicy::class);
    }

    //
    //
    public function visitasAnualOJS2(Request $request){
        set_time_limit(1800);

        $filtro_year=$this->cargarFiltroAnual();
        $ultimo_anio=$filtro_year->last();

        $titulo="visitas v2";
        $assoc_type=Journal::VISITAS;
        $anio=$request->get('year',$ultimo_anio);

        $reporte=Reporte::orderBy('id','desc')->first();

        if(($request->ajax() && $request->get('procesar') )
            || !($reporte instanceof Reporte)
            ){
            Journal::generarTablaReporte($anio,$assoc_type);//->orderBy('metric_total','desc');
            return response()->json(['status'=>true]);
        }elseif($reporte instanceof  Reporte){
            $query=Journal::prepararReporte($anio, $reporte->id,$assoc_type);
        }

        $journals=$query->orderBy('metric_total','desc')->paginate(50)->appends($request->only('year','month'));

        $grafico=new Grafico($query->orderBy('path','asc')->paginate(50),$request->get('year'));



        $breadcrumbs=$this->getBreadcrumbs('',$titulo);
        $data=compact('titulo','breadcrumbs','journals','filtro_year','grafico','reporte','ultimo_anio');
        return view("gestor.ojs.reporte",$data);
    }

    public function descargasAnualOJS2(Request $request){
        set_time_limit(1800);
        $filtro_year=$this->cargarFiltroAnual();
        $ultimo_anio=$filtro_year->last();

        $titulo="descargas V2";
        $assoc_type=Journal::DESCARGAS;
        $anio=$request->get('year',$ultimo_anio);


        $reporte=Reporte::orderBy('id','desc')->first();

        if(($request->ajax() && $request->get('procesar') )
            || !($reporte instanceof Reporte)
            ){
            Journal::generarTablaReporte($anio,$assoc_type);//->orderBy('metric_total','desc');
            return response()->json(['status'=>true]);
        }elseif($reporte instanceof  Reporte){
            $query=Journal::prepararReporte($anio, $reporte->id,$assoc_type);
        }

        $journals=$query->orderBy('metric_total','desc')->paginate(50)->appends($request->only('year','month'));

        $grafico=new Grafico($query->orderBy('path','asc')->paginate(50),$request->get('year'));


        $breadcrumbs=$this->getBreadcrumbs('',$titulo);

        $data=compact('titulo','breadcrumbs','journals','filtro_year','grafico','reporte','ultimo_anio');
        return view("gestor.ojs.reporte",$data);
    }

    public function visitasAnualOJS3(Request $request){
        set_time_limit(3600);

        $filtro_year=$this->cargarFiltroAnual();
        $ultimo_anio=$filtro_year->last();

        $assoc_type=JournalV3::VISITAS;
        $titulo="visitas v3";
        $anio=$request->get('year',$ultimo_anio);

        $reporte=Reporte::orderBy('id','desc')->first();

        if(($request->ajax() && $request->get('procesar') )
            || !($reporte instanceof Reporte)
            ){
            JournalV3::generarTablaReporte($anio,$assoc_type);//->orderBy('metric_total','desc');
            return response()->json(['status'=>true]);
        }elseif($reporte instanceof  Reporte){
            $query=JournalV3::prepararReporte($anio, $reporte->id,$assoc_type);
        }

        $journals=$query->orderBy('metric_total','desc')->paginate(50)->appends($request->only('year','month'));

        $grafico=new Grafico($query->orderBy('path','asc')->paginate(50),$request->get('year'));


        $breadcrumbs=$this->getBreadcrumbs('',$titulo);

        $data=compact('titulo','breadcrumbs','journals','filtro_year','grafico','reporte','ultimo_anio');
        return view("gestor.ojs.reporte",$data);
    }

    public function descargasAnualOJS3(Request $request){
        set_time_limit(3600);
        $filtro_year=$this->cargarFiltroAnual();
        $ultimo_anio=$filtro_year->last();

        $titulo="descargas v3";
        $assoc_type=JournalV3::DESCARGAS;
        $anio=$request->get('year',$ultimo_anio);

        $reporte=Reporte::orderBy('id','desc')->first();

        if(($request->ajax() && $request->get('procesar') )
            || !($reporte instanceof Reporte)
            ){
            JournalV3::generarTablaReporte($anio,$assoc_type);//->orderBy('metric_total','desc');
            return response()->json(['status'=>true]);
        }elseif($reporte instanceof  Reporte){
            $query=JournalV3::prepararReporte($anio, $reporte->id,$assoc_type);
        }

        $journals=$query->orderBy('metric_total','desc')->paginate(50)->appends($request->only('year','month'));

        $grafico=new Grafico($query->orderBy('path','asc')->paginate(50),$request->get('year'));


        $breadcrumbs=$this->getBreadcrumbs('',$titulo);

        $data=compact('titulo','breadcrumbs','journals','filtro_year','grafico','reporte','ultimo_anio');
        return view("gestor.ojs.reporte",$data);
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo Revistas'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }

    private function cargarFiltroAnual()
    {
        $lista=collect();
        $date=Carbon::createFromFormat("Y","2016");
        $date_limit=Carbon::now();
        while($date->format("Y")<$date_limit->format("Y")){
            $lista->add($date->format("Y"));
            $date->addYear();
        }

        return $lista;
    }
}
