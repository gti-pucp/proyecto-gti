<?php

namespace App\Http\Controllers\Gestor\Comunicacion;

use App\Modulos\Chat\Canal;
use App\Modulos\Chat\Mensaje;
use App\Events\Chat\EnviandoMensajePrivado;
use App\Policies\EventosPolicy;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CanalesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id_canal)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /*
    //
    public function formulario($id_canal=0){

        $usuario=Usuario::all()->random(1);

        $canal=Canal::findOrFail($id_canal);

        $mensajes=$canal->mensajes;

        $data=compact('canal','mensajes','usuario');
        return view('chat.formulario',$data);
    }

    public function store(Request $request,$id_canal=0){
        $canal=Canal::findOrFail($id_canal);

        $usuario=Auth::user();
        $mensaje=new Mensaje();
        $mensaje->id_canal=$canal->id;
        $mensaje->id_usuario=$usuario->id;
        $mensaje->mensaje=$request->input('mensaje');

        $mensaje->save();

        event(new EnviandoMensajePrivado($usuario,$mensaje));

        $data=compact('mensaje');
        return response()->json(["status"=>true,"mensaje"=>view('chat.mensaje',$data)->render()]);
    }*/
}
