<?php

namespace App\Http\Controllers\gestor\EZPROXY;

use App\Modulos\EZPROXY\ReporteEZProxy;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReportesMensualesController extends Controller
{
    //
    public function index(Request $request){
//        $directories=Storage::disk('s3-proyecto-gti')->allDirectories();
        $archivos=Storage::disk('s3-proyecto-gti')->allFiles("reportes-ezproxy");

        $files=collect();

        foreach($archivos as $ruta){

            $archivo=basename($ruta);
            $ext = pathinfo($archivo, PATHINFO_EXTENSION);



            $file = basename($archivo,".".$ext);
            $fecha=str_replace(["comprimido","-","parcial",".tar","fuente"],["","","","",""],$file);
            $fecha=Carbon::createFromFormat("Ym",$fecha)->format("Y-m");


            $item=(object)[
                "nombre"=>$archivo,
                "fecha"=>$fecha,
                "url_descarga"=>route("gestor.ezproxy.descarga",$archivo)
            ];
            $files->push($item);
        }

        $files=$files->sortByDesc("nombre");

        $data=compact('files');
        return view ('gestor.ezproxy.reportes',$data);
    }



    public function descarga($file){
//        $reporte=ReporteEZProxy::findOrFail($id);
//        $ruta_sistema=$reporte->getComprimido();
//
//

        return Storage::disk('s3-proyecto-gti')->download("reportes-ezproxy/".$file);
//        return response()->download($ruta_sistema)->deleteFileAfterSend(false);
    }
}
