<?php

namespace App\Http\Controllers\Gestor\CrossRef;

use App\Modulos\CrossRef\XmlGenerator;
use App\Modulos\RI\Item;
use Carbon\Carbon;
use DOMDocument;
use DOMElement;
use DOMNode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GeneradorXmlController extends Controller
{
    //
    public function formulario(){
        return view('gestor.crossref.formulario');
    }

    public function generar(Request $request){
        $this->validate($request,[
            'url'=>'url',
            'type'=>'required',
            'doi'=>'required'
        ]);

        $url=$request->input('url');
        $type=$request->input('type');
        $doi=$request->input('doi');

        $path=parse_url($url,PHP_URL_PATH);

        $handle=basename(dirname($path))."/".basename($path);

        $item=Item::findHandle($handle)->first();

        $xml=new XmlGenerator();
        $xml->cargarItemFromDSPACE($item,$type,$doi);
        $xml_data=$xml->generarXML();

        $nombre=time().".xml";
        Storage::disk()->put($nombre,$xml_data->saveXML());


        $headers=[
            'Content-disposition'=>"attachment",
            'Content-type'=>"text/xml",
            'charset'=>"utf8"
        ];
//        $data=compact('autores');
        return response()->download(storage_path("app/".$nombre),$nombre,$headers)->deleteFileAfterSend();
    }


}

