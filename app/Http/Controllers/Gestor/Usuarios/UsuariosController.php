<?php

namespace App\Http\Controllers\Gestor\Usuarios;

use App\Http\Controllers\Controller;
use App\Mail\NotificarCambioClave;
use App\Mail\NotificarCuentaCreada;
use App\Modulos\Usuarios\Usuario;
use App\Policies\UsuariosPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UsuariosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.UsuariosPolicy::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $usuarios=Usuario::filtrar($request->get('buscar'))->orderBy('name');

        $listado_usuarios=$usuarios->paginate(10);

        foreach($listado_usuarios as $usuario){
            $usuario->url_reset=route('gestor.usuarios.reset',[$usuario->id]);
            $usuario->url_permisos=route('gestor.permisos.listado',[$usuario->id]);
            $usuario->url_editar=route('gestor.usuarios.editar',[$usuario->id]);
            $usuario->url_eliminar=route('gestor.usuarios.eliminar',[$usuario->id]);
        }

        $titulo="Listado de usuarios";
        $breadcrumbs=$this->getBreadCrumbs('','Listado');

        $data=compact('titulo','breadcrumbs','listado_usuarios');
        return view('gestor.usuarios.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $usuario=new Usuario();

        $titulo="Crear usuario";
        $breadcrumbs=$this->getBreadCrumbs('','Crear usuario');


        $data=compact('titulo','breadcrumbs','usuario');
        return view('gestor.usuarios.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre'=>'required',
            'correo'=>'required|unique:mysql.users,email'
        ]);
        //
        $clave=Str::random(16);

        $usuario=new Usuario();
        $usuario->name=$request->get('nombre');
        $usuario->email=$request->get('correo');
        $usuario->password=bcrypt($clave);
        $usuario->save();

        Mail::send(new NotificarCuentaCreada($usuario,$clave));

        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Guardado con exito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $usuario=Usuario::findOrFail($id);

        $data=compact('usuario');
        return view('gestor.usuarios.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'nombre'=>'required'
        ]);
        //
        $usuario=Usuario::findOrFail($id);
        $usuario->name=$request->get('nombre');
//        $usuario->email=$request->get('email');
        $usuario->save();
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Actualizado con exito']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usuario=Usuario::findOrFail($id);

        $usuario->delete();
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Eliminado con exito']);
    }


    public function generarClave(Request $request,$id){
        $clave=Str::random(16);

        $usuario=Usuario::findOrFail($id);
        $usuario->password=bcrypt($clave);
        $usuario->save();

        Mail::send(new NotificarCambioClave($usuario,$clave));

        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Nueva clave generada con exito']);
    }

    public function importar(){

        $listado_importaciones=ImportacionUsuarios::paginate(10);
        $data=compact('listado_importaciones');
        return view('gestor.usuario.importar',$data);
    }
//
//    public function procesarImportacion(Request $request){
//
//        $importacion=new ImportacionUsuarios();
//        $importacion->archivo=time()."_".$request->file('file')->getClientOriginalName();
//        $importacion->nombre=$request->file('file')->getBasename();
//        $importacion->save();
//
//        if($request->ajax()){
//            return response()->json(['error'=>"error",'code'=>200]);
//        }
//        return redirect()->back();
//    }

    public function download($id)
    {
        $importacion=ImportacionUsuarios::findOrFail($id);

        return response()->download($importacion->archivo);
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import(Request $request)
    {
        $importacion=new ImportacionUsuarios();
        $importacion->archivo=time()."_".$request->file('file')->getClientOriginalName();
        $importacion->nombre=$request->file('file')->getBasename();
        $importacion->save();


        Excel::import(new UsersImport,$request->file('file') );
        return redirect()->back()->with('success', 'All good!');
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo usuarios'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }
}
