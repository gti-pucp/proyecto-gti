<?php

namespace App\Http\Controllers\Gestor\Usuarios;

use App\Modulos\Usuarios\Permiso;
use App\Modulos\Usuarios\Usuario;
use App\Policies\PermisosPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermisosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.PermisosPolicy::class);
    }

    public function index(Request $request, $id_usuario)
    {
        //
        $usuario=Usuario::findOrFail($id_usuario);
        $permisos=$usuario->permisos();

        $combo_permisos=Permiso::whereNotIn('id',$permisos->pluck('permisos.id'))->get();

        $listado_permisos=$permisos->paginate(20);

        foreach($listado_permisos as $permiso){
            $permiso->url_eliminar=route('gestor.permisos.listado',[$usuario->id,$permiso->id]);
        }

        $breadcrumbs=$this->getBreadCrumbs('',"Permisos");

        $data=compact('breadcrumbs','listado_permisos','usuario','combo_permisos');
        return view('gestor.usuarios.permisos',$data);
    }

    public function store(Request $request, $id_usuario)
    {
        //
        $usuario=Usuario::findOrFail($id_usuario);
        $id_permiso=$request->input('permiso');

        $usuario->permisos()->attach($id_permiso,['estado'=>true]);
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Agregado con exito']);
    }

    public function destroy(Request $request, $id_usuario){
        $usuario=Usuario::findOrFail($id_usuario);
        $id_permiso=$request->input('id_permiso');
        $usuario->permisos()->detach($id_permiso);
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Agregado con exito']);
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo usuarios'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }
}
