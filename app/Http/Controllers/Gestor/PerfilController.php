<?php

namespace App\Http\Controllers\Gestor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PerfilController extends Controller
{
    //

    const DIRECTORIO_FOTO_PERFIL = "perfil/";

    public function show(){
        $usuario=Auth::user();
        $data=compact('usuario');
        return view('gestor.perfil.info',$data);
    }

    public function clave(){
        $usuario=Auth::user();
        $data=compact('usuario');
        return view('gestor.perfil.clave',$data);
    }

    public function store(Request $request){
        $usuario=Auth::user();
        $usuario->password=bcrypt($request->input('password'));
        $usuario->save();
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Contraseña actualizada con éxito']);;
    }

    public function editar(){
        $usuario=Auth::user();
        $data=compact('usuario');
        return view('gestor.perfil.edit',$data);
    }

    public function update(Request $request){
        $usuario=Auth::user();
        $usuario->name=$request->input('name');
        $usuario->save();

        return redirect()->route("perfil.show")->with(['tipo'=>'success','notificacion'=>'Actualizado con éxito']);
    }

    public function imagen(){
        $usuario=Auth::user();

        $data=compact('usuario');
        return view('gestor.perfil.imagen',$data);
    }

    public function almacenarImagen(Request $request) {

        $this->validate($request,[
            'imagen'=>'required|dimensions:max_width=500,max_height=500'
        ]);

        $usuario=Auth::user();

        $imagen=$request->file('imagen')->getClientOriginalName();
        $request->file('imagen')->storeAs(self::DIRECTORIO_FOTO_PERFIL."/".$usuario->id,$imagen);

        $usuario->imagen=$imagen;
        $usuario->save();

        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Eliminado con exito']);
    }

    public function eliminarImagen(Request $request) {


        $usuario=Auth::user();

        Storage::disk('local')->delete(self::DIRECTORIO_FOTO_PERFIL.$usuario->id."/".$usuario->imagen);

        $usuario->imagen=null;
        $usuario->save();

        return redirect()->back();
    }

    public function cargarImagen(){
        $usuario=Auth::user();
        $imagen=Storage::disk('local')->path(self::DIRECTORIO_FOTO_PERFIL.$usuario->id."/".$usuario->imagen);
        return response()->file($imagen);
    }
}
