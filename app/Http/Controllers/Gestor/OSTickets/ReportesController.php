<?php

namespace App\Http\Controllers\Gestor\OSTickets;

use App\Modulos\OSTickets\Reporte;
use App\Modulos\OSTickets\User;
use App\Modulos\OSTickets\Staff;
use App\Policies\ReporteOSTicketsPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteOSTicketsPolicy::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function participacionAgentes(Request $request)
    {
        //
        $titulo="Reporte de Agentes";

        $reporte=new Reporte();

        $desde= $request->get('desde',Carbon::now()->addMonths(-1)->startOfMonth()->format("Y-m-d"));
        $hasta= $request->get('hasta',Carbon::now()->addMonths(-1)->endOfMonth()->format("Y-m-d"));

        $dateStart=Carbon::createFromFormat('Y-m-d',$desde);
        $dateEnd=Carbon::createFromFormat('Y-m-d', $hasta);

        $agentes=Staff::reporteGeneral($dateStart->format('Y-m-d'),$dateEnd->format('Y-m-d'))->paginate(200);
        $filtro_year=['2016','2017','2018','2019'];

        $breadcrumbs=$this->getBreadCrumbs('','Reportes');

        //$data=compact('titulo','breadcrumbs','agentes','meses','filtro_year','grafico','reporte','dateStart','dateEnd');
        $data=compact('titulo','breadcrumbs','agentes','filtro_year','reporte','dateStart','dateEnd');
        return view("gestor.ostickets.reporte-agentes",$data);
    }

    public function participacionUsuarios(Request $request){
        //
        $titulo="Reporte de Usuarios";

        $reporte=new Reporte();

        $desde= $request->get('desde',Carbon::now()->addMonths(-1)->startOfMonth()->format("Y-m-d"));
        $hasta= $request->get('hasta',Carbon::now()->addMonths(-1)->endOfMonth()->format("Y-m-d"));

        $dateStart=Carbon::createFromFormat('Y-m-d',$desde);
        $dateEnd=Carbon::createFromFormat('Y-m-d', $hasta);

        $usuarios=User::reporteGeneral($dateStart->format('Y-m-d'),$dateEnd->format('Y-m-d'))->paginate(50);
        $filtro_year=['2016','2017','2018','2019'];

        $breadcrumbs=$this->getBreadCrumbs('','Reportes');

//        $data=compact('titulo','breadcrumbs','usuarios','meses','filtro_year','grafico','reporte','dateStart','dateEnd');
        $data=compact('titulo','breadcrumbs','usuarios','filtro_year','reporte','dateStart','dateEnd');
        return view("gestor.ostickets.reporte-usuarios",$data);
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo mesa de ayuda'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }
}
