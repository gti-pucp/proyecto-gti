<?php

namespace App\Http\Controllers\Gestor\OSTickets;

use App\Modulos\OSTickets\Ticket;
use App\Policies\ReporteOSTicketsPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportesOSTicketsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteOSTicketsPolicy::class);
    }

    public function index(Request $request){


        $titulo="Reporte Mesa de ayuda";

        $formato_fecha="Y-m-d";

        $fecha_inicio_default=Carbon::now()->addMonths(-1)->format($formato_fecha);
        $fecha_final_default=Carbon::now()->format($formato_fecha);

        $fecha_inicio=Carbon::createFromFormat("Y-m-d",$request->get("fecha_inicio",$fecha_inicio_default))->startOfDay();//"2022-04-24 21:17:50";
        $fecha_final=Carbon::createFromFormat("Y-m-d",$request->get("fecha_final",$fecha_final_default))->endOfDay();//"2022-05-24 21:17:50";

        $departamento_id=$request->get('departamento_id');
        $equipo_id=$request->get('equipo_id');


        $temporalidad_seleccionado=$request->get("temporalidad","rango");
        $selectTemporalidades=$this->generarCombo($temporalidad_seleccionado,collect([
            (object)['value'=>"ultimo_mes",'label'=>'Ultimo mes hasta el momento'],
            (object)['value'=>"ultimo_dia",'label'=>'Ultimo dia hasta el momento'],
            (object)['value'=>"dia_anterior",'label'=>'Día anterior'],
            (object)['value'=>"mes_anterior",'label'=>'Mes anterior'],
        ]));
        foreach($selectTemporalidades as $item){
            $this->generarUrlFiltroTemporal($item,$formato_fecha,$request);
        }


        $tipo_seleccionado=$request->get("tipo","departamento");

        $selectTipos=$this->generarCombo($tipo_seleccionado,collect([
            (object)['value'=>"departamento",'label'=>'departamento'],
            (object)['value'=>"tipo",'label'=>'tipo'],
            (object)['value'=>"agente",'label'=>'agente'],
        ]));

//        $fecha_inicio=Carbon::createFromTimestamp(1653690983);
//        $fecha_final=Carbon::createFromTimestamp(1656369383);

//        echo $fecha_inicio->timestamp." _ ".$fecha_final;
//        echo "<br>";
//        echo $fecha_final->timestamp."-".$fecha_final;
        $mostrar=$request->get('mostrar','solo_no_escalados');
        $selectMostrar=$this->generarCombo($mostrar,collect([
            (object)['value'=>"todos",'label'=>'Todos los tickets'],
            (object)['value'=>"mostrar_no_escalados",'label'=>'Excluir tickets escalados'],
            (object)['value'=>"mostrar_escalados",'label'=>'Solo tickets escalados'],
        ]));

        $filtro=['fecha_inicio'=>$fecha_inicio->format($formato_fecha),
            'fecha_final'=>$fecha_final->format($formato_fecha),
            'tipo'=>$tipo_seleccionado,
            'departamento_id'=>$departamento_id,
            'equipo_id'=>$equipo_id,
            'mostrar'=>$mostrar,
        ];




        if($tipo_seleccionado=="departamento"){
            $listado=Ticket::reporteDepartamento($fecha_inicio,$fecha_final,$tipo_seleccionado,$mostrar)->paginate(20)->appends($filtro);
        }else if($tipo_seleccionado=="tipo"){
            $listado=Ticket::reporteTipo($fecha_inicio,$fecha_final,$tipo_seleccionado,$mostrar)->paginate(20)->appends($filtro);
        }else if($tipo_seleccionado=="agente"){
            $listado=Ticket::reporteAgente($fecha_inicio,$fecha_final,$tipo_seleccionado,$mostrar)->paginate(20)->appends($filtro);
        }else if($tipo_seleccionado=="equipos_de_departamento"){
            //al seleccionar departamento
            $listado=Ticket::reporteEquiposDepartamento($fecha_inicio,$fecha_final,$tipo_seleccionado,$departamento_id,$mostrar)->paginate(20)->appends($filtro);
        }else if($tipo_seleccionado=="agentes_de_equipo"){
            //al seleccionar equipo
            $listado=Ticket::reporteAgentesEquipo($fecha_inicio,$fecha_final,$tipo_seleccionado,$departamento_id,$equipo_id,$mostrar)->paginate(20)->appends($filtro);
        }

        foreach($listado as $item){
            $item->url_equipos_departamento=route("ostickets.reporte.equipos_departamento",[
                'fecha_inicio'=>$fecha_inicio->format($formato_fecha),
                'fecha_final'=>$fecha_final->format($formato_fecha),
                'tipo'=>'equipos_de_departamento',
                'departamento_id'=>$item->dept_id,
                'mostrar'=>$mostrar,
            ]);

            $item->url_agentes_equipo=route("ostickets.reporte.agentes_equipo",[
                'fecha_inicio'=>$fecha_inicio->format($formato_fecha),
                'fecha_final'=>$fecha_final->format($formato_fecha),
                'tipo'=>'agentes_de_equipo',
                'departamento_id'=>$departamento_id,
                'equipo_id'=>$item->team_id,
                'mostrar'=>$mostrar,
            ]);
            $item->tiempo_servicio=number_format($item->tiempo_servicio,1);
            $item->tiempo_respuesta=number_format($item->tiempo_respuesta,1);
        }

        $data=compact("listado","titulo","fecha_inicio","fecha_final","selectTipos","selectTemporalidades","temporalidad_seleccionado","selectMostrar");
        return view('gestor.ostickets.reporte-general',$data);
    }

    private function generarCombo($item_seleccionado,$opciones){

        foreach($opciones as $item){
            $item->selected=$item->value==$item_seleccionado;
        }

        return $opciones;
    }

    private function generarUrlFiltroTemporal($item,$formato_fecha,$request){
        if($item->value=="ultimo_dia"){
            $fecha_inicio=Carbon::now()->addDays(-1);
            $fecha_final=Carbon::now();
        }else  if($item->value=="ultimo_mes"){
            $fecha_inicio=Carbon::now()->addMonths(-1);
            $fecha_final=Carbon::now();
        }else if($item->value=="mes_anterior"){
            $fecha_inicio=Carbon::now()->addDays(-1);
            $fecha_final=Carbon::now();

            $fecha_inicio->startOfMonth()->addMonths(-1);
            $fecha_final->endOfMonth()->addMonths(-1);
        }else if($item->value=="dia_anterior"){
            $fecha_inicio=Carbon::now()->addDays(-1);
            $fecha_final=Carbon::now();

            $fecha_inicio->addDays(-1);
            $fecha_final->addDays(-1);
        }else{
            $fecha_inicio=Carbon::now()->addDays(-1);
            $fecha_final=Carbon::now();
        }

        $item->url=route("ostickets.reporte.general",['fecha_inicio'=>$fecha_inicio->format($formato_fecha),'fecha_final'=>$fecha_final->format($formato_fecha)]);
    }
}
