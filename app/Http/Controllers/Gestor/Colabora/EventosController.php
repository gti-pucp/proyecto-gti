<?php

namespace App\Http\Controllers\Admin\Colabora;

use App\Modulos\SB\Evento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventosController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $buscar=$request->get('buscar');

        $eventos=Evento::orderBy('created_at');

        $listado_eventos=$eventos->paginate(10);
        foreach ($listado_eventos as $item){
            $item->url_invitados=route('invitados.index',$item->id);

            $item->url_editar=route('eventos.edit',[$item->id]);
            $item->url_eliminar=route('eventos.destroy',[$item->id]);

        }
        $data=compact('listado_eventos');
        return view('admin.colabora.eventos.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $evento=new Evento();

        $data=compact('evento');
        return view('admin.colabora.eventos.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $evento=new Evento;
        $evento->titulo=$request->input('titulo');
        $evento->fecha_hora=$request->input('fecha_hora');

        $evento->save();

        return redirect()->back()->with([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $evento=Evento::findOrFail($id);

        $data=compact('evento');
        return view('admin.colabora.eventos.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $evento=Evento::findOrFail($id);
        $evento->delete();

        return redirect()->back()->with([]);
    }
}
