<?php

namespace App\Http\Controllers\Gestor\OAI;

use App\Modulos\OAI\Comprimido;
use App\Modulos\OAI\FuenteOAI;
use App\Modulos\OAI\XmlDepurador;
use App\Modulos\OAI\XmlEstadisticas;
use App\Policies\OAIPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ExtractorController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.OAIPolicy::class);
    }

    public function index(Request $request){
        $titulo="Recursos OAI";
        $fuentes=FuenteOAI::paginate(10);
        $data=compact('fuentes','titulo');
        return view('gestor.oai.listado',$data);
    }

    public function create(){
        $titulo="Extractor XML OAI";
        $data=compact('titulo');
        return view('gestor.oai.formulario',$data);
    }

    public function store(Request $request){
        $this->validate($request,[
            'url'=>'required|url',
        ]);

        $fuente=new FuenteOAI();
        $fuente->tipo_fuente=0;
        $fuente->url=$request->input('url');
        $fuente->save();
        return redirect()->route('gestor.oai.listado');
    }

    public function delete(Request $request){
        $fuente=FuenteOAI::findOrFail($request->input('id_fuente'));
        return redirect()->route('gestor.oai.listado');
    }

    public function procesar(Request $request,$id_fuente){
        $depurado=$request->get('depurado',0);

        $fuente=FuenteOAI::findOrFail($id_fuente);
        $fuente->obtenerArchivosRemotos();
        $fuente->generarEstadisticas();
        if($depurado==1){
            $fuente->depurarData();
        }
        session()->put('archivo_temporal',$fuente->empaquetar());
        session()->put('nombre_descarga',$fuente->getNombreComprimido());

        //ESTADISTICAS,
        $base_url_web="http://tesis.pucp.edu.pe/repositorio/handle/";
        $base_url_oai="http://tesis.pucp.edu.pe/oai/request?verb=GetRecord&metadataPrefix=oai_dc&identifier=";

        $titulo="Reporte de extraccion XML OAI";
        $data=compact('titulo','archivos','colecciones','registros_por_xml','base_url_web','base_url_oai','fuente');
        return view('gestor.oai.reporte-extraccion',$data);

//
    }

    public function descarga(){
        $archivo_temporal=session()->get('archivo_temporal');
        $nombre_descarga=session()->get('nombre_descarga');
        return response()->download($archivo_temporal,$nombre_descarga);
    }

    public function testXMLDepurador(){
        $archivo="15735880586555/2.xml";
        $data=Storage::disk('local')->get($archivo);
        $xml=new XmlDepurador($data,basename($archivo));
        $xml->procesarContenido(false);
        $xml->saveXML("15735880586555","2-depurado.xml");
    }

    public function testXMLConteo(){
//        $fuente=FuenteOAI::findOrFail(1);
//        //$fuente->depurar();
//        $fuente->prepararExtractor(false);


        $directorio="15737498567344";
        $archivos=collect(Storage::disk('local')->files($directorio));
        $archivos=$archivos->sort(function ($aa, $bb) {
            $a=str_replace(".xml","",basename($aa));
            $b=str_replace(".xml","",basename($bb));

            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        foreach($archivos as $archivo){
            $data=Storage::disk('local')->get($archivo);
            $xml=new XmlDepurador($data);
            XmlEstadisticas::analizar($archivo,$xml);
        }


        $base_url_web="http://tesis.pucp.edu.pe/repositorio/handle/";
        $base_url_oai="http://tesis.pucp.edu.pe/oai/";

        $titulo="Reporte de extraccion XML OAI";
        $data=compact('titulo','archivos','colecciones','registros_por_xml','base_url_web','base_url_oai');
        return view('gestor.oai.reporte-extraccion',$data);
    }
}
