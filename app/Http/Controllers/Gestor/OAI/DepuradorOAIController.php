<?php

namespace App\Http\Controllers\Gestor\OAI;

use App\Modulos\OAI\Comprimido;
use App\Policies\OAIPolicy;
use Carbon\Carbon;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DepuradorOAIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.OAIPolicy::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('gestor.oai.depurador');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
           'paquete'=>'required|file|mimetypes:application/zip'
        ]);

        //DESCOMPRIMIR EMPAQUETADO EN CARPETA
        $paquete=$request->file('paquete');
        $timestamp=Carbon::now()->getPreciseTimestamp(4);

        Storage::disk('temp')->makeDirectory($timestamp);
        $ruta=Storage::disk('temp')->getAdapter()->getPathPrefix().$timestamp;

        Zipper::make($paquete->getRealPath())->extractTo($ruta);

        //LERR ARCHIVO Y APLICAR PROCESAMIENTO DE TEXTO
        $archivos=collect(Storage::disk('temp')->allFiles($timestamp));
        $collection = $archivos->sort(function ($aa, $bb) {
            $a=str_replace(".xml","",basename($aa));
            $b=str_replace(".xml","",basename($bb));

            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        $prefijo="procesado_";
        foreach($collection as $archivo_original){
            $contenido_original=Storage::disk('temp')->get($archivo_original);

            $contenido=Comprimido::limpiarContenido($contenido_original);
            Storage::disk('temp')->put($prefijo.$archivo_original,$contenido);
        }
        //EMPAQUETAR
        $ruta_archivos_procesados=Storage::disk('temp')->getAdapter()->getPathPrefix().$prefijo.$timestamp;
        $ruta_comprimido=Storage::disk('temp')->getAdapter()->getPathPrefix().$paquete->getClientOriginalName();

        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make($ruta_comprimido);
        $zipper->add($ruta_archivos_procesados);
        $zipper->close();

        Storage::disk('temp')->deleteDirectory($timestamp);
        Storage::disk('temp')->deleteDirectory($prefijo.$timestamp);
        //MOSTRAR LINK DE DESCARGA EMPAQUETADO PROCESADO

        return response()->download($ruta_comprimido,"procesado-".$paquete->getClientOriginalName())->deleteFileAfterSend();

    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
