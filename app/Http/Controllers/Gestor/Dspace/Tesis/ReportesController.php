<?php

namespace App\Http\Controllers\Gestor\Dspace\Tesis;

use App\Exports\Tesis\ReporteExport;
use App\Modulos\Tesis\Agente;
use App\Modulos\Tesis\Bitstream;
use App\Modulos\Tesis\EPerson;
use App\Modulos\Tesis\Item;
use App\Modulos\Tesis\MetadataValue;
use App\Policies\ReporteDspaceTesisPolicy;
use App\Policies\ReporteOSTicketsPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteDspaceTesisPolicy::class);
    }



    public function index(Request $request){
        $titulo="Reporte de tesis";

//        $correos=[
//            'dalvarado@pucp.edu.pe',
//            'vbranes@pucp.pe',
//            'miriam.cayllahua@pucp.pe',
//            'mgaldos@pucp.pe',
//            'cgiraldoa@pucp.pe',
//            'shuaraz@pucp.pe',
//            'leon.jd@pucp.pe',
//            'jsorianof@pucp.edu.pe',
//            'ana.tarazona@pucp.pe',
//            'rvaleriom@pucp.pe',
//            'lmanchego@pucp.pe',
//            'nbasilio@pucp.edu.pe'
//        ];

        $uuids=Agente::all()->pluck('uuid')->toArray();

        $year=$request->get('year',Carbon::now()->format("Y"));
        $month=$request->get('month',null);


        $agentes=EPerson::prepararReporteAnual($uuids,$year,$month)->paginate(25);

        $total=$this->agregarTotales($agentes);

        $breadcrumbs=$this->getBreadCrumbs('','Reporte de publicaciones de usuario');

        $data=compact('titulo','breadcrumbs','agentes','meses','grafico','reporte','total','year');
        return view("gestor.dspace.tesis.reporte",$data);
    }

    public function archivosPesados(Request $request){
        $titulo="Archivos pesados segun tipo y peso mínimo";
        $url_base=$this->getUrlBaseAcceso();

        $peso=$request->get('peso',250);
        $tipo=$request->get('tipo',1);

        $combo_tipos=collect([
            (object)['id'=>'1','valor'=>'Primer archivo'],
            (object)['id'=>'0','valor'=>'No es primer archivo']
        ]);



        $bitstreams=Bitstream::obtenerPesados($peso,$tipo)->paginate(10)->appends([
            'tipo'=>$tipo,
            'peso'=>$peso,
        ]);;
        $breadcrumbs=$this->getBreadCrumbs('','Reporte de archivos pesados');

        $data=compact('titulo','breadcrumbs','peso','tipo','combo_tipos','bitstreams','url_base');
        return view("gestor.dspace.tesis.archivos-pesados",$data);
    }

    public function itemsPendientes(Request $request){
        $titulo="Archivos pendientes de generacion de thumbnail";
        $url_base=$this->getUrlBaseAcceso();

        $items=Item::prepararParaThumbnailGenerator()->paginate(10);
//        $items=Bitstream::obtenerSinThumbnail()->paginate(10);

        $breadcrumbs=$this->getBreadCrumbs('','Reporte thumbnail pendientes');

        $data=compact('titulo','breadcrumbs','items','url_base');
        return view("gestor.dspace.tesis.items-pendientes",$data);
    }

    public function getUrlBaseAcceso(){
        return "http://tesis.pucp.edu.pe/repositorio/handle/";
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo Tesis'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }

    public function exportar(Request $request)
    {
        $uuids=Agente::all()->pluck('uuid')->toArray();

        $year=$request->get('year',Carbon::now()->format("Y"));
        $month=$request->get('month',null);


        $agentes=EPerson::prepararReporteAnual($uuids,$year,$month)->paginate(25);
        $totales=$this->agregarTotales($agentes);

        return Excel::download(new ReporteExport($agentes,$totales), 'reporte.xlsx');
    }

    private function agregarTotales($agentes)
    {
        $total=[
            'metric_01'=>0,
            'metric_02'=>0,
            'metric_03'=>0,
            'metric_04'=>0,
            'metric_05'=>0,
            'metric_06'=>0,
            'metric_07'=>0,
            'metric_08'=>0,
            'metric_09'=>0,
            'metric_10'=>0,
            'metric_11'=>0,
            'metric_12'=>0,
            'metric_total'=>0
        ];
        $agentes->each(function($agente) use(&$total){
            $agente->metric_total=$agente->metric_01+$agente->metric_02+$agente->metric_03+
                $agente->metric_04+$agente->metric_05+$agente->metric_06+
                $agente->metric_07+$agente->metric_08+$agente->metric_09+
                $agente->metric_10+$agente->metric_11+$agente->metric_12;

            $total['metric_01']+=$agente->metric_01;
            $total['metric_02']+=$agente->metric_02;
            $total['metric_03']+=$agente->metric_03;
            $total['metric_04']+=$agente->metric_04;
            $total['metric_05']+=$agente->metric_05;
            $total['metric_06']+=$agente->metric_06;
            $total['metric_07']+=$agente->metric_07;
            $total['metric_08']+=$agente->metric_08;
            $total['metric_09']+=$agente->metric_09;
            $total['metric_10']+=$agente->metric_10;
            $total['metric_11']+=$agente->metric_11;
            $total['metric_12']+=$agente->metric_12;
            $total['metric_total']+=$agente->metric_total;
        });
        return $total;
    }
}
