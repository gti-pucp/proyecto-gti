<?php

namespace App\Http\Controllers\Gestor\Dspace\Tesis;

use App\Modulos\Tesis\EPerson;
use App\Modulos\Tesis\Agente;
use App\Policies\ReporteDspaceTesisPolicy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgentesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteDspaceTesisPolicy::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $agentes=Agente::paginate(20);

        foreach($agentes as $agente){
            $agente->url_eliminar=route('dspace.tesis.agentes.delete',[$agente->uuid]);
        }

        $data=compact('agentes');
        return view('gestor.dspace.tesis.agentes',$data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'email'=>'required',
        ]);
        //

        $email=$request->input('email');
        $eperson=EPerson::where('email',$email)->get()->first();
        if($eperson instanceof EPerson){
            $agente=Agente::where('uuid',$eperson->uuid)->get()->first();
            if($agente instanceof Agente){
                return redirect()->back()->with(['tipo'=>'warning','notificacion'=>'Ya se ha asignado el usuario']);
            }else{
                $agente=new Agente();
                $agente->uuid=$eperson->uuid;
                $agente->save();
                return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Asignado con exito']);
            }


        }else{
            return redirect()->back()->with(['tipo'=>'danger','notificacion'=>'No se encontro el usuario']);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $agente=Agente::where('uuid',$id)->first();
        $agente->delete();
        return redirect()->back()->with(['tipo'=>'success','notificacion'=>'Removido con exito']);
    }
}
