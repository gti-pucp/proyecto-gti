<?php

namespace App\Http\Controllers\Gestor\Dspace\RI;

use App\Modulos\RI\Agente;
use App\Modulos\RI\Bitstream;
use App\Modulos\RI\EPerson;
use App\Modulos\RI\HarvestedCollection;
use App\Modulos\RI\Item;
use App\Modulos\RI\MetadataValue;
use App\Policies\ReporteDspaceRIPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ReporteDspaceRIPolicy::class);
    }

    public function index(Request $request){

        $titulo="Reporte de tesis";

//        $correos=[
//            'dalvarado@pucp.edu.pe',
//            'vbranes@pucp.pe',
//            'miriam.cayllahua@pucp.pe',
//            'mgaldos@pucp.pe',
//            'cgiraldoa@pucp.pe',
//            'shuaraz@pucp.pe',
//            'leon.jd@pucp.pe',
//            'jsorianof@pucp.edu.pe',
//            'ana.tarazona@pucp.pe',
//            'rvaleriom@pucp.pe',
//            'lmanchego@pucp.pe'
//        ];

        $uuids=Agente::all()->pluck('uuid')->toArray();

        $year=$request->get('year',Carbon::now()->format("Y"));
        $month=$request->get('month',null);


        $agentes=EPerson::prepararReporteAnual($uuids,$year,$month)->paginate(25);


        $total=[
            'metric_01'=>0,
            'metric_02'=>0,
            'metric_03'=>0,
            'metric_04'=>0,
            'metric_05'=>0,
            'metric_06'=>0,
            'metric_07'=>0,
            'metric_08'=>0,
            'metric_09'=>0,
            'metric_10'=>0,
            'metric_11'=>0,
            'metric_12'=>0,
            'metric_total'=>0
        ];
        $agentes->each(function($agente) use(&$total){
            $agente->metric_total=$agente->metric_01+$agente->metric_02+$agente->metric_03+
                $agente->metric_04+$agente->metric_05+$agente->metric_06+
                $agente->metric_07+$agente->metric_08+$agente->metric_09+
                $agente->metric_10+$agente->metric_11+$agente->metric_12;

            $total['metric_01']+=$agente->metric_01;
            $total['metric_02']+=$agente->metric_02;
            $total['metric_03']+=$agente->metric_03;
            $total['metric_04']+=$agente->metric_04;
            $total['metric_05']+=$agente->metric_05;
            $total['metric_06']+=$agente->metric_06;
            $total['metric_07']+=$agente->metric_07;
            $total['metric_08']+=$agente->metric_08;
            $total['metric_09']+=$agente->metric_09;
            $total['metric_10']+=$agente->metric_10;
            $total['metric_11']+=$agente->metric_11;
            $total['metric_12']+=$agente->metric_12;
            $total['metric_total']+=$agente->metric_total;

        });

        $breadcrumbs=$this->getBreadCrumbs('','Reporte de usuarios');
        $data=compact('titulo','breadcrumbs','agentes','meses','grafico','reporte','total','year');
        return view("gestor.dspace.tesis.reporte",$data);
    }

    public function archivosPesados(Request $request){

//        return "En desarrollo";
        $titulo="Archivos pesados segun tipo y peso mínimo";
        $url_base=$this->getUrlBaseAcceso();

        $peso=$request->get('peso',250);
        $tipo=$request->get('tipo',1);

        $combo_tipos=collect([
            (object)['id'=>'1','valor'=>'Primer archivo'],
            (object)['id'=>'0','valor'=>'No es primer archivo']
        ]);

        $bitstreams=Bitstream::obtenerPesados($peso,$tipo)->paginate(10);

        $breadcrumbs=$this->getBreadCrumbs('','Reporte archivos pendientes');

        $data=compact('titulo','breadcrumbs','peso','tipo','combo_tipos','bitstreams','url_base');
        return view("gestor.dspace.tesis.archivos-pesados",$data);
    }

    public function itemsPendientes(Request $request){
//        return "En desarrollo";
        $titulo="Archivos pendientes de generacion de thumbnail";
        $url_base=$this->getUrlBaseAcceso();
        $items=Item::prepararSinThumbnailParaGestion()->paginate(10);
        $breadcrumbs=$this->getBreadCrumbs('','Reporte thumbnail pendientes');

        $data=compact('titulo','breadcrumbs','items','url_base');
        return view("gestor.dspace.tesis.items-pendientes",$data);
    }

    public function getUrlBaseAcceso(){
        return "http://repositorio.pucp.edu.pe/index/handle/";
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo Repositorio Institucional'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }

    public function cosechas(Request $request){
        $buscar=$request->get('buscar');
        $titulo="Reporte de colecciones cosechadas";
        $url_base=$this->getUrlBaseAcceso();

        $colecciones_cosechadas=HarvestedCollection::filtrarCosechas($buscar)->paginate(20);
        $breadcrumbs=$this->getBreadCrumbs('','Reporte de colecciones cosechada');

        $data=compact('titulo','breadcrumbs','colecciones_cosechadas','url_base','buscar');
        return view("gestor.dspace.tesis.colecciones-cosechadas",$data);
    }

    public function duplicados(Request $request){
        $titulo="Reporte de Items duplicados";
        $url_base=$this->getUrlBaseAcceso();

        //$items=MetadataValue::filtrarURLDuplicadas()->paginate(20);
        $items=Item::filtrarTitulosDuplicados()->orderBy('titulo')->paginate(20);

        $data=compact('items','url_base','titulo');
        return view('gestor.dspace.tesis.cosechas-duplicadas',$data);
    }

    public function itemsIndexacionNullPointer(Request $request){
        $titulo="Reporte de Items con NULL al indexar OAI";
        $url_base=$this->getUrlBaseAcceso();

        //$items=MetadataValue::filtrarURLDuplicadas()->paginate(20);
        $items=Item::FiltrarConPoliciesNulos()->paginate(20);

        $data=compact('items','url_base','titulo');
        return view('gestor.dspace.tesis.items-nulos-indexacion',$data);
    }
}
