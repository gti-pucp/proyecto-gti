<?php

namespace App\Http\Controllers\Gestor\Monitoreo;

use App\Modulos\Monitoreo\Servidor;
use App\Policies\ServidorPolicy;
use App\Rules\CorreosMultiples;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServidoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permisos:'.ServidorPolicy::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $servidores=Servidor::filtrar($request->get('buscar'))->orderBy('url');

        $listado_servidores=$servidores->paginate(10);

        foreach($listado_servidores as $servidor){
            $servidor->url_editar=route('gestor.servidores.editar',[$servidor->id]);
            $servidor->url_eliminar=route('gestor.servidores.eliminar',[$servidor->id]);
        }

        $titulo="Listado";
        $breadcrumbs=$this->getBreadcrumbs('','Listado de servidores');

        $data=compact('titulo','breadcrumbs','listado_servidores');
        return view('gestor.servidores.listado',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $servidor=new Servidor();

        $titulo="Registrar servidor";
        $breadcrumbs=$this->getBreadcrumbs('','Registrar servidor');

        $data=compact('titulo','breadcrumbs','servidor');
        return view('gestor.servidores.formulario',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'url'=>'required',
            'correo_responsable'=>'required|email',
            'correos_adicionales'=>[new CorreosMultiples],
//            'comprobar'=>'required',
            'limite_espacio'=>'required',
        ]);
        $servidor=new Servidor();

        $servidor->url=$request->input('url');
        $servidor->correo_responsable=$request->input('correo_responsable');
        $servidor->correos_adicionales=$request->input('correos_adicionales');
        $servidor->comprobar=$request->input('comprobar',false)?true:false;
        $servidor->limite_espacio=$request->input('limite_espacio');
        $servidor->save();

        return redirect()->route('gestor.servidores.listado')->with('status','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $titulo="Editar servidor";
        $servidor=Servidor::findOrFail($id);
        $data=compact('servidor','titulo');
        return view('gestor.servidores.formulario',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'url'=>'required',
            'correo_responsable'=>'required|email',
            'correos_adicionales'=>[new CorreosMultiples],
//            'comprobar'=>'required',
            'limite_espacio'=>'required',
        ]);
        $servidor=Servidor::findOrFail($id);

        $servidor->url=$request->input('url');
        $servidor->correo_responsable=$request->input('correo_responsable');
        $servidor->correos_adicionales=$request->input('correos_adicionales');
        $servidor->comprobar=$request->input('comprobar',false)?true:false;
        $servidor->limite_espacio=$request->input('limite_espacio');
        $servidor->fecha_notificacion=null;

        $servidor->save();

        return redirect()->route('gestor.servidores.listado')->with('status','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $servidor=Servidor::findOrFail($id);

        $servidor->delete();

        return redirect()->route('gestor.servidores.listado')->with('status','success');
    }

    public function getBreadCrumbs($valor,$label){
        $breadcrumbs=collect([
            (object)['valor'=>'','active'=>false,'label'=>'Inicio'],
            (object)['valor'=>'','active'=>false,'label'=>'Modulo servidores'],
        ]);

        $breadcrumbs->push((object)['valor'=>$valor,'active'=>false,'label'=>$label]);
        return $breadcrumbs;
    }
}
