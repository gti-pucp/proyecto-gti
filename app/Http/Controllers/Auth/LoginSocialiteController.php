<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modulos\Usuarios\Usuario;
use Illuminate\Support\Facades\Auth;
use Socialite;

class LoginSocialiteController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        //$user = Socialite::driver('google')->stateLess()->user();

        try {
            $user_social = Socialite::driver('google')->stateLess()->user();   //watch out with changes due to stateless user
        } catch (Exception $e) {
            echo "ERROR:";
        }

        //SI EXISTE lo carga
        $usuario=Usuario::where("email",$user_social->getEmail())->first();

        if( !($usuario instanceof Usuario) ){
            session(['token_google'=>$user_social->token]);
            return redirect()->route('registrar');
        }

        Auth::login($usuario);


        return redirect("/home");
    }
}
