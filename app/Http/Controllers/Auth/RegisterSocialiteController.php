<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modulos\Usuarios\Usuario;
use Illuminate\Http\Request;
use Socialite;

class RegisterSocialiteController extends Controller
{
    //
    public function registrar(Request $request){

        if(env('AUTOREGISTRO',false)==true){
            abort(404,"No se permite el registro de usuarios");
        }
        $token=$request->session()->get("token_google");

        $user_social =  Socialite::driver('google')->userFromToken($token);

        $usuario=new Usuario([
                'id'=> $user_social->getId(),
                'name'=> $user_social->getName(),
                'email'=> $user_social->getEmail(),
            ]);

        $data=compact('usuario');
        return view('auth.register-social',$data);
    }
}
