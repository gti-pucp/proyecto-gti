<?php

namespace App\Http\Controllers\Api;

use App\Modulos\RI\HarvestedCollection;
use App\Modulos\RI\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RIController extends Controller
{
    public function cosechasPendientes(Request $request){
        $fecha_get=$request->get('fecha_limite');
        if(isset($fecha_get)){
            $fecha_limite=Carbon::createFromFormat("Y-m-d",$fecha_get)->startOfDay();
        }else{
            $fecha_limite=Carbon::now()->startOfDay();

            if(!$fecha_limite->isSunday()){
                $fecha_limite->startOfWeek()->addDays(-1);
            }
        }

        if($fecha_limite instanceof Carbon){
            $items=HarvestedCollection::prepararCosechas($fecha_limite)->get();
            foreach($items as $harvested){
                echo $harvested->handle_coleccion."\n";
            }
        }else{
            echo "error";
        }
    }

    public function thumbnailsPendientes(){
        $items=Item::prepararParaThumbnailGenerator()->get();
        foreach($items as $item){
            echo $item->handle."\n";
        }
    }
}
