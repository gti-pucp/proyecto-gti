<?php

namespace App\Http\Controllers\Api;

use App\Modulos\Tesis\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TesisController extends Controller
{
    public function thumbnailsPendientes(){
        $items=Item::prepararParaThumbnailGenerator()->get();

        foreach($items as $item) {
            echo $item->handle."\n";
        }
    }
}
