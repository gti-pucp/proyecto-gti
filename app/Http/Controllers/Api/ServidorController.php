<?php

namespace App\Http\Controllers\Api;

use App\Mail\NotificarServidorLimiteEspacio;
use App\Modulos\Monitoreo\Servidor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ServidorController extends Controller
{
    public function espacioDisco(Request  $request){
        $url_servidor=$request->input('servidor');
        $espacio_total=$request->input('espacio_total');
        $espacio_usado=$request->input('espacio_usado');

        $servidor=Servidor::where('url',$url_servidor)->firstOrFail();

        if($servidor->comprobar){
            $servidor->procesarEspacio($espacio_total,$espacio_usado);

            //campo temporal al procesar espacio
            if($servidor->notificar_limite_espacio){
                Mail::send(new NotificarServidorLimiteEspacio($servidor));
            }
        }

        echo $servidor->url. " ocupado al ".$servidor->porcentaje_usado;
    }
}
