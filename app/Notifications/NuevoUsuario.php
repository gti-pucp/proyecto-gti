<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevoUsuario extends Notification implements ShouldQueue
{
    use Queueable;

    private $usuario;
    private $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario,$password)
    {
        //
        $this->usuario=$usuario;
        $this->password=$password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Bienvenido a '.env('app_name').'.')
                    ->line('Su usuario de acceso es: ')
                    ->line('# '.$this->usuario->email)
                    ->line('Su clave de acceso es: ')
                    ->line('# '.$this->password)
                    ->action('Iniciar sesión', url('/'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
