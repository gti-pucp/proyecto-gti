<?php

namespace App\Policies;

use App\Modulos\SB\Evento;

use App\Modulos\Usuarios\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventosPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //

    }

    /**
     * Determine whether the Usuario can view any servidors.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function viewAny(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }

    /**
     * Determine whether the Usuario can view the model.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Usuario  $model
     * @return mixed
     */
    public function view(Usuario $usuario)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can create models.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function create(Usuario $usuario)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can update the model.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Usuario  $model
     * @return mixed
     */
    public function update(Usuario $usuario, Evento $evento)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can delete the model.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Usuario  $model
     * @return mixed
     */
    public function delete(Usuario $usuario, Evento $evento)
    {
        //
//        dd();
//        $usuario=$permiso->pivot->id_usuario;
//        echo $permiso->pivot->id_usuario."!=".$usuario->id."<br>";

        return true;
    }

    /**
     * Determine whether the Usuario can restore the model.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Usuario  $model
     * @return mixed
     */
    public function restore(Usuario $usuario, Evento $evento)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can permanently delete the model.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Usuario  $model
     * @return mixed
     */
    public function forceDelete(Usuario $usuario, Evento $evento)
    {
        //
        return true;
    }
}
