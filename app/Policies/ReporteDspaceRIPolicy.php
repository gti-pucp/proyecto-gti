<?php

namespace App\Policies;

use App\Modulos\Usuarios\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReporteDspaceRIPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }
}
