<?php

namespace App\Policies;

use App\Modulos\Usuarios\Usuario;
use App\Modulos\Usuarios\Usuario as Editable;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsuariosPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the Usuario can view any servidors.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @return mixed
     */
    public function viewAny(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }

    public function changePassword(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }

    /**
     * Determine whether the Usuario can view the servidor.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @param  Editable  $usuario_editable
     * @return mixed
     */
    public function view(Usuario $usuario, Editable $usuario_editable)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can create servidors.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @return mixed
     */
    public function create(Usuario $usuario)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can update the servidor.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @param  Editable  $usuario_editable
     * @return mixed
     */
    public function update(Usuario $usuario, Editable $usuario_editable)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can delete the servidor.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @param  Editable  $usuario_editable
     * @return mixed
     */
    public function delete(Usuario $usuario, Editable $usuario_editable)
    {
        //
        return $usuario->id!==$usuario_editable->id;
    }

    /**
     * Determine whether the Usuario can restore the servidor.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @param  Editable  $usuario_editable
     * @return mixed
     */
    public function restore(Usuario $usuario, Editable  $usuario_editable)
    {
        //
        return $usuario->id!==$usuario_editable->id;
    }

    /**
     * Determine whether the Usuario can permanently delete the servidor.
     *
     * @param  \App\Modulos\Usuarios\Usuario  $usuario
     * @param  Editable  $usuario_editable
     * @return mixed
     */
    public function forceDelete(Usuario $usuario, Editable $usuario_editable)
    {
        //
        return $usuario->id!==$usuario_editable->id;
    }
}
