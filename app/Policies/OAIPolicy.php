<?php

namespace App\Policies;

use App\Modulos\OAI\Comprimido;
use App\Modulos\Usuarios\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class OAIPolicy
{
    use HandlesAuthorization;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the Usuario can view any servidors.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function viewAny(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }

    /**
     * Determine whether the Usuario can view the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  Comprimido  $comprimido
     * @return mixed
     */
    public function view(Usuario $usuario, Comprimido $comprimido)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can create servidors.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function create(Usuario $usuario)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can update the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  Comprimido  $comprimido
     * @return mixed
     */
    public function update(Usuario $usuario, Comprimido $comprimido)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can delete the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  Comprimido $comprimido
     * @return mixed
     */
    public function delete(Usuario $usuario, Comprimido $comprimido)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can restore the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  Comprimido  $servidor
     * @return mixed
     */
    public function restore(Usuario $usuario, Comprimido $comprimido)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can permanently delete the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  Comprimido  $comprimido
     * @return mixed
     */
    public function forceDelete(Usuario $usuario, Comprimido $comprimido)
    {
        //
        return true;
    }


}
