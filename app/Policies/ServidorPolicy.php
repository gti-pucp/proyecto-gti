<?php

namespace App\Policies;

use App\Modulos\Monitoreo\Servidor;
use App\Modulos\Usuarios\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServidorPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the Usuario can view any servidors.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function viewAny(Usuario $usuario)
    {
        //
        return $usuario->tienePermiso(get_class($this));
    }

    /**
     * Determine whether the Usuario can view the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Servidor  $servidor
     * @return mixed
     */
    public function view(Usuario $usuario, Servidor $servidor)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can create servidors.
     *
     * @param  \App\Usuario  $usuario
     * @return mixed
     */
    public function create(Usuario $usuario)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can update the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Servidor  $servidor
     * @return mixed
     */
    public function update(Usuario $usuario, Servidor $servidor)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can delete the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Servidor  $servidor
     * @return mixed
     */
    public function delete(Usuario $usuario, Servidor $servidor)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can restore the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Servidor  $servidor
     * @return mixed
     */
    public function restore(Usuario $usuario, Servidor $servidor)
    {
        //
        return true;
    }

    /**
     * Determine whether the Usuario can permanently delete the servidor.
     *
     * @param  \App\Usuario  $usuario
     * @param  \App\Servidor  $servidor
     * @return mixed
     */
    public function forceDelete(Usuario $usuario, Servidor $servidor)
    {
        //
        return true;
    }
}
