<?php

namespace App\Exports\Tesis;

use App\Modulos\Tesis\Agente;
use App\Modulos\Tesis\EPerson;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReporteExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $agentes;
    private $totales;
    public function __construct($agentes,$totales)
    {
        $this->agentes=$agentes;
        $this->totales=$totales;
    }


    public function view(): View
    {
        $agentes=$this->agentes;
        $total=$this->totales;
        $data=compact('titulo','breadcrumbs','agentes','meses','grafico','reporte','total','year');
        return view("gestor.dspace.tesis.reporte-export",$data);
    }

}
